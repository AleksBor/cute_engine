#include <iostream>

#include "Shader.hpp"
#include "Camera.hpp"

#include "Village.hpp"
#include "PostProcessing.hpp"
#include "SSAO.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "SceneWindow.hpp"

#include "Assertion.h"

int screenWidth = 640;
int screenHeight = 480;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

bool grabCursor = false;

#if __APPLE__
// GL 3.2 + GLSL 150
const char* glsl_version = "#version 150";
#else
// GL 3.0 + GLSL 130
const char* glsl_version = "#version 130";
#endif

bool initializeWindow() {

    SceneWindow::Init(screenWidth, screenHeight);

    //SceneWindow::GrabCursor();

    std::cout << glGetString(GL_VERSION) << std::endl;

    return true;
}

int main(void)
{
    if (!initializeWindow()) { return -1; }

    STATIC_ASSERT(sizeof(int) == 4);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(SceneWindow::Instance->getWindow(), true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);

    Scene* currCanv = new Village();

    while (!glfwWindowShouldClose(SceneWindow::Instance->getWindow()))
    {
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;

        if (deltaTime < 0.05)
        {
            continue;
        }

        lastFrame = currentFrame;

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            ImGui::Begin("Examples");                          // Create a window called "Hello, world!" and append into it.

            ImGui::Text("Examples:");

            static int radioBtn = 0;
            static int lastCanv = radioBtn;
            ImGui::RadioButton("Village", &radioBtn, 0);
            ImGui::RadioButton("Post processing", &radioBtn, 1);
            ImGui::RadioButton("SSAO", &radioBtn, 2);

            if (radioBtn != lastCanv) {
                lastCanv = radioBtn;
                delete currCanv;
                switch (radioBtn) {
                case 0:
                    currCanv = new Village();
                    break;
                case 1:
                    currCanv = new PostProcessing();
                    break;
                case 2:
                    currCanv = new SSAO();
                    break;
                }
            }

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::End();
        }

        // Rendering
        ImGui::Render();

        // Render here
        glViewport(0, 0, screenWidth, screenHeight);

        currCanv->Update();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(SceneWindow::Instance->getWindow());
        glfwPollEvents();
    }
    delete currCanv;

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    SceneWindow::Reset();
    glfwTerminate();
    return 0;
}
