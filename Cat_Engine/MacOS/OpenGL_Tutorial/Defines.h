//
//  Defines.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 30/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#define ROOT_PATH std::string("/Users/aleks/Documents/Engine/Cat_Engine")
#define TEXTURE_ROOT_PATH ROOT_PATH + "/Examples/res/texture/"
#define SHADERS_ROOT_PATH ROOT_PATH + "/Examples/res/shaders/"
#define TERRAIN_ROOT_PATH ROOT_PATH + "/Examples/res/terrain/"
