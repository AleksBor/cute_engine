//
//  data_manip.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

namespace Data {

template < typename T >
    void WriteData(void** dest, T data)
    {
        T* result = *((T**)dest);
        *result++ = data;
        *dest = result;
    }
}
