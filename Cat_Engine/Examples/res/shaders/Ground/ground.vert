#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

//Camera

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

uniform mat4 model;

uniform mat4 lightSpaceMatrix;

//Normal transform
uniform mat4 normalTransform;

//Outputs

out vec4 FragPos;
out vec3 Normal;
out vec2 TexCoords;
out vec4 FragPosLightSpace;

void main()
{
    TexCoords = aTexCoords;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    
    FragPos = model * vec4(aPos.xyz, 1.0);
    
    Normal = mat3(normalTransform) * aNormal;
    
    FragPosLightSpace = lightSpaceMatrix * FragPos;
}
