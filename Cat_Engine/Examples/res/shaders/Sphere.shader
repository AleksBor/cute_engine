#shader vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec3 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 normalTransform;

void main()
{
    gl_Position = projection * view * model * vec4(position.xyz, 1.0);
    FragPos = vec3(model * vec4(position.xyz, 1.0));
    
    Normal = mat3(normalTransform) * normal;
}

#shader fragment
#version 330 core

struct Light {
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Light light;

uniform vec3 viewPos;

in vec3 FragPos;
in vec3 Normal;

out vec4 resultColor;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform Material material;

void main()
{
    //ambient
    vec3 ambient = light.ambient * material.ambient;

    //diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diffKoef = max(dot(norm, lightDir), 0.0f);
    vec3 diffuse = (diffKoef * material.diffuse) * light.diffuse;
    
    //specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = material.specular * spec * light.specular;
    
    vec3 resLight = (ambient + diffuse + specular);
    resultColor = vec4(resLight, 1.0f);
}
