#version 330 core

#include "DirLight.shader"

//Outputs
out vec4 FragColor;

//Inputs
in vec2 TexCoords;
in vec4 FragPos;
in vec3 Normal;
in vec4 FragPosLightSpace;

in mat3 TBN;

uniform bool enableNormals;

//Material

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

//Light
uniform DirLight dirLight;

uniform vec3 viewPos;

uniform sampler2D texture_depth1;
uniform sampler2D texture_normal_map_1;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(texture_depth1, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    float bias = max(0.0015 * (1.0 - dot(normal, lightDir)), 0.0015);
    
    float shadow = 0.0;
    
    vec2 texelSize = 1.0 / textureSize(texture_depth1, 0);
    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            float pcfDepth = texture(texture_depth1, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    
    shadow /= 9.0;
    
    if (currentDepth > 1.0) {
        shadow = 0.0;
    }
    
    return shadow;
}

void main()
{
    vec4 diffuseColor = texture(material.texture_diffuse1, TexCoords);
    
    if (diffuseColor.a < 0.1)
    {
        discard;
    }
    
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = normalize(Normal);
    if (enableNormals) {
        norm = vec3(texture(texture_normal_map_1, TexCoords));
        norm = normalize(norm * 2.0 - 1.0);
        norm = normalize(TBN * norm);
        //norm = normalize(normTransform * norm);
    }
    
    vec3 viewDir = normalize(viewPos - FragPos.xyz);
    
    float shadow = ShadowCalculation(FragPosLightSpace, norm, dirLight.position);
    outputColor += CalcDirLight(dirLight, norm, viewDir, vec3(diffuseColor), shadow);
    
    FragColor = vec4(outputColor, 1.f);
}
