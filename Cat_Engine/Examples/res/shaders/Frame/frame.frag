#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightnessColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_diffuse2;
};

uniform Material material;

void main()
{
    float exposure = 1.0;

    const float gamma = 2.2;
    vec3 hdrColor = texture(material.texture_diffuse1, TexCoords).rgb;
    vec3 bloomColor = texture(material.texture_diffuse2, TexCoords).rgb;
    hdrColor += bloomColor;
    
    //vec3 mapped = hdrColor / (hdrColor + vec3(1.0));
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
    mapped = pow(mapped, vec3(1.0f / gamma));
    
    //FragColor = vec4(mapped, 1.0f);
    FragColor = vec4(hdrColor, 1.0f);
}
