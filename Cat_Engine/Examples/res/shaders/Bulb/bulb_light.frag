#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightnessColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

void main()
{
    vec3 res = 5 * vec3(0.93f, 0.86f, 0.1f);
    FragColor = vec4(res, 1.0f);
    
    float brightness = dot(res, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1.0) {
        BrightnessColor = vec4(res, 1.0);
    } else {
        BrightnessColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
    BrightnessColor = FragColor;
}
