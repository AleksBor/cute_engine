#shader vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 normal;

out vec2 TexCoords;

out vec3 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 normalTransform;

void main()
{
    gl_Position = projection * view * model * vec4(position.xyz, 1.0);
    FragPos = vec3(model * vec4(position.xyz, 1.0));
    
    Normal = mat3(normalTransform) * normal;
    
    TexCoords = texCoord;
}

#shader fragment
#version 330 core

struct DirLight {
    vec3 direction;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
uniform DirLight dirLight;

struct PointLight {
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};
uniform PointLight pointLight;

struct SpotLight {
    //projector
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};
uniform SpotLight spotLight;

uniform vec3 viewPos;

in vec3 FragPos;
in vec3 Normal;

out vec4 resultColor;

struct Material {
    vec3 ambient;
    sampler2D diffuse1;
    sampler2D specular1;
    float shininess;
};

in vec2 TexCoords;

uniform Material material;

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);
    
    outputColor += CalcDirLight(dirLight, norm, viewDir);
    outputColor += CalcPointLight(pointLight, norm, FragPos, viewDir);
    outputColor += CalcSpotLight(spotLight, norm, FragPos, viewDir);
    
    resultColor = vec4(0.04, 0.28, 0.26, 1.0);
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction);
    
    float diffKoef = max(dot(normal, lightDir), 0.0);
    
    vec3 reflectDir = reflect(-lightDir, normal);
    float specKoef = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    vec3 ambient = light.ambient * vec3(texture(material.diffuse1, TexCoords));
    vec3 diffuse = light.diffuse * diffKoef * vec3(texture(material.diffuse1, TexCoords));
    vec3 specular = light.specular * specKoef * vec3(texture(material.specular1, TexCoords));
    
    return (ambient + diffuse + specular);
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    
    float diffKoef = max(dot(normal, lightDir) , 0.0);
    
    vec3 reflectDir = reflect(-lightDir, normal);
    float specKoef = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
    
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    
    vec3 ambient = light.ambient * vec3(texture(material.diffuse1, TexCoords));
    vec3 diffuse = light.diffuse * diffKoef * vec3(texture(material.diffuse1, TexCoords));
    vec3 specular = light.specular * specKoef * vec3(texture(material.specular1, TexCoords));
    
    return (attenuation * (ambient + diffuse + specular));
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    if (theta > light.outerCutOff) {
        float lightDist = length(light.position - fragPos);
        float attenuation = 1.0 / (light.constant + light.linear * lightDist + light.quadratic * (lightDist * lightDist));
        
        vec3 texColor = vec3(texture(material.diffuse1, TexCoords));
        //ambient
        vec3 ambient = light.ambient * texColor;

        //diffuse
        vec3 norm = normalize(Normal);
        
        float diffKoef = max(dot(norm, lightDir), 0.0f);
        vec3 diffuse = diffKoef * light.diffuse * texColor;
        
        //specular
        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        //vec3 specular = vec3(texture(material.specular1, TexCoords)) * spec * light.specular;
        vec3 specular = vec3(texture(material.specular1, TexCoords)) * spec * light.specular;
        
        diffuse *= intensity;
        specular *= intensity;
        
        return (attenuation * (ambient + diffuse + specular));
    } else {
        return light.ambient * vec3(texture(material.diffuse1, TexCoords));
    }
}
