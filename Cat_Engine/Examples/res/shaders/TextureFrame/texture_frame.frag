#version 330 core
layout (location = 0) out vec4 FragColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
};
uniform Material material;

uniform bool specular;

void main()
{
    if (specular) {
        FragColor = vec4(vec3(texture(material.texture_diffuse1, TexCoords).a), 1.0f);
    } else {
        vec3 color = texture(material.texture_diffuse1, TexCoords).rgb;
        
        FragColor = vec4(color, 1.0f);
    }
}
