#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
layout (location = 5) in vec2 aOffset;

out vec2 TexCoords;
out vec3 FragPos;
out vec3 Normal;

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

uniform mat4 model;

uniform mat4 normalTransform;

void main()
{
    Normal = mat3(normalTransform) * aNormal;
    TexCoords = aTexCoords;
    vec3 pos = aPos;
    pos.x += aOffset.x;
    pos.z += aOffset.y;
    FragPos = vec3(model * vec4(pos.xyz, 1.0));
    gl_Position = projection * view * model * vec4(pos, 1.0);
}
