#version 330 core

out vec4 FragColor;

in vec3 TexCoords;

uniform samplerCube texture_cubemap_depth_1;

void main()
{
    float depthValue = texture(texture_cubemap_depth_1, TexCoords).r;
    FragColor = vec4(vec3(depthValue), 1.0f);
}
