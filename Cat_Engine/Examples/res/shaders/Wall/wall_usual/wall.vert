#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitangent;

//Camera

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

uniform mat4 model;

uniform mat4 lightSpaceMatrix;

//Outputs

out vec4 FragPos;
out vec3 Normal;
out vec2 TexCoords;
out vec4 FragPosLightSpace;

out mat3 normTransform;
out mat3 TBN;

//Normal transform
uniform mat4 normalTransform;

void main()
{
    TexCoords = aTexCoords;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    
    FragPos = model * vec4(aPos.xyz, 1.0);
    
    Normal = mat3(normalTransform) * aNormal;

    FragPosLightSpace = lightSpaceMatrix * FragPos;

	normTransform = mat3(normalTransform);
    
    vec3 T = normalize(mat3(normalTransform) * aTangent);
    vec3 B = normalize(mat3(normalTransform) * aBitangent);
    vec3 N = normalize(Normal);
    
    TBN = mat3(T, B, N);
}
