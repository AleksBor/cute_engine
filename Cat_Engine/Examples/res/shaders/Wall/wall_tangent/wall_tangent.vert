#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitangent;

//Camera

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

uniform mat4 model;

uniform mat4 lightSpaceMatrix;

uniform vec3 lightPosition;
uniform vec3 viewPos;

//Outputs

out vec2 TexCoords;
out vec3 tangentFragPos;
out vec3 tangentFragPosLightSpace;

out vec3 tangentLightPosition;
out vec3 tangentViewPos;

//Normal transform
uniform mat4 normalTransform;

void main()
{
    TexCoords = aTexCoords;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    
    vec4 fragPos = model * vec4(aPos.xyz, 1.0);
    
    vec3 Normal = mat3(normalTransform) * aNormal;

    vec4 fragPosLight = lightSpaceMatrix * fragPos;
    
    tangentFragPosLightSpace = vec3(fragPosLight.xyz / fragPosLight.w);
    
    vec3 T = normalize(mat3(normalTransform) * aTangent);
    vec3 B = normalize(mat3(normalTransform) * aBitangent);
    vec3 N = normalize(Normal);
    
    mat3 TBN = inverse(mat3(T, B, N));
    tangentLightPosition = TBN * lightPosition;
    tangentFragPosLightSpace = tangentFragPosLightSpace;
    tangentViewPos = TBN * viewPos;
    tangentFragPos = TBN * vec3(fragPos);
}
