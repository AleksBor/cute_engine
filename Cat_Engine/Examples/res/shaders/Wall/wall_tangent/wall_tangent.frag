#version 330 core

//Outputs
out vec4 FragColor;

//Inputs
in vec2 TexCoords;
in vec3 tangentFragPos;
in vec3 tangentFragPosLightSpace;

in vec3 tangentLightPosition;
in vec3 tangentViewPos;

uniform bool enableNormals;

//Material

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

//Light

struct DirLight {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
uniform DirLight dirLight;

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec3 diffuseColor);
/////////////////////////////////////////////////////////////

uniform sampler2D texture_depth1;
uniform sampler2D texture_normal_map_1;

float ShadowCalculation(vec3 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
    vec3 projCoords = fragPosLightSpace;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(texture_depth1, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    float bias = max(0.0015 * (1.0 - dot(normal, lightDir)), 0.0015);
    
    float shadow = 0.0;
    
    vec2 texelSize = 1.0 / textureSize(texture_depth1, 0);
    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            float pcfDepth = texture(texture_depth1, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    
    shadow /= 9.0;
    
    if (currentDepth > 1.0) {
        shadow = 0.0;
    }
    
    return shadow;
}

void main()
{
    vec3 diffuseColor = vec3(texture(material.texture_diffuse1, TexCoords));
    
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = vec3(texture(texture_normal_map_1, TexCoords));
    norm = normalize(norm * 2.0 - 1.0);
    
    vec3 viewDir = normalize(tangentViewPos - tangentFragPos);
    
    outputColor += CalcDirLight(dirLight, norm, viewDir, diffuseColor);
    
    FragColor = vec4(outputColor, 1.f);
}

//Light realization

vec3 CalcDirLight(DirLight light, vec3 norm, vec3 viewDir, vec3 diffuseColor)
{
    vec3 lightPos = normalize(tangentLightPosition);
    
    float diffKoef = max(dot(norm, lightPos), 0.0);
    
    vec3 ambient = light.ambient * diffuseColor;
    vec3 diffuse = light.diffuse * diffKoef * diffuseColor;
    
    float shadow = 0.0f;//ShadowCalculation(tangentFragPosLightSpace, norm, lightPos);
    
    return (ambient + ((1.0 - shadow) * diffuse));
}

/////////////////////////////////////////////////////////////
