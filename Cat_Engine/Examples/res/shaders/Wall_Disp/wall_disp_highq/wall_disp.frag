#version 330 core

#include "DirLight.shader"

//Outputs
out vec4 FragColor;

//Inputs
in vec2 TexCoords;
in vec4 FragPos;
in vec3 Normal;
in vec4 FragPosLightSpace;

in mat3 normTransform;
in mat3 TBN;

uniform bool enableNormals;

//Material

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

//Light
uniform DirLight dirLight;

uniform vec3 viewPos;

uniform sampler2D texture_depth1;
uniform sampler2D texture_normal_map_1;
uniform sampler2D texture_displacement_map_1;

uniform vec2 size;

const float depthScale = 0.1;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(texture_depth1, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    float bias = max(0.0015 * (1.0 - dot(normal, lightDir)), 0.0015);
    
    float shadow = 0.0;
    
    vec2 texelSize = 1.0 / textureSize(texture_depth1, 0);
    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            float pcfDepth = texture(texture_depth1, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    
    shadow /= 9.0;
    
    if (currentDepth > 1.0) {
        shadow = 0.0;
    }
    
    return shadow;
}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir, out float lastDepthValue);
float getParallaxSelfShadow(vec2 inTexCoords, vec3 inLightDir, float inLastDepth);

void main()
{
    vec3 viewDir = normalize(viewPos - FragPos.xyz);
    
    float depthValue;
    
    vec2 texCoor = ParallaxMapping(TexCoords, normalize(inverse(TBN) * viewDir), depthValue);
    float pmShadowMultiplier = getParallaxSelfShadow(TexCoords, normalize(inverse(TBN) * normalize(dirLight.position)), depthValue);
    
    if (texCoor.x > size.x || texCoor.y > size.y || texCoor.x < 0.0 || texCoor.y < 0.0) {
        discard;
    }
    
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = normalize(Normal);
    if (enableNormals) {
        norm = vec3(texture(texture_normal_map_1, texCoor));
        norm = normalize(norm * 2.0 - 1.0);
        norm = normalize(TBN * norm);
    }
    
    vec3 diffuseColor = vec3(texture(material.texture_diffuse1, texCoor));
    float shadow = ShadowCalculation(FragPosLightSpace, norm, dirLight.position);
    outputColor += CalcDirLight(dirLight, norm, viewDir, diffuseColor, shadow);
    outputColor *= pmShadowMultiplier;
    
    FragColor = vec4(outputColor, 1.f);
}

//Light realization

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir, out float lastDepthValue) {
    const float minLayers = 2.0;
    const float maxLayers = 32.0;
    
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    
    float layerDepth = 1.0f / numLayers;
    
    float currentLayerDepth = 0.0;
    
    vec2 P = viewDir.xy * depthScale;
    vec2 deltaTexCoords = P / numLayers;
    
    vec2 currentTexCoords = texCoords;
    float currentDepthMapValue = texture(texture_displacement_map_1, currentTexCoords).r;
    
    while(currentLayerDepth < currentDepthMapValue)
    {
        currentTexCoords -= deltaTexCoords;
        
        currentDepthMapValue = texture(texture_displacement_map_1, currentTexCoords).r;
        
        currentLayerDepth += layerDepth;
    }
    
    deltaTexCoords *= 0.5;
    layerDepth *= 0.5;
    
    currentTexCoords += deltaTexCoords;
    currentLayerDepth -= layerDepth;
    
    const int _reliefSteps = 5;
    int currentStep = _reliefSteps;
    
    while (currentStep > 0)
    {
        currentDepthMapValue = texture(texture_displacement_map_1, currentTexCoords).r;
        deltaTexCoords *= 0.5;
        layerDepth *= 0.5;
        
        if (currentDepthMapValue > currentLayerDepth)
        {
            currentTexCoords -= deltaTexCoords;
            currentLayerDepth += layerDepth;
        } else {
            currentTexCoords += deltaTexCoords;
            currentLayerDepth -= layerDepth;
        }
        
        currentStep--;
    }
    
    lastDepthValue = currentDepthMapValue;
    
    return currentTexCoords;
}

float getParallaxSelfShadow(vec2 inTexCoords, vec3 inLightDir, float inLastDepth)
{
    float shadowMultiplier = 0.f;
    
    float alignFactor = dot(vec3(0.f, 0.f, 1.f), inLightDir);
    
    if (alignFactor > 0.f) {
        const float _minLayers = 16.f;
        const float _maxLayers = 32.f;
        float _numLayers = mix(_maxLayers, _minLayers, abs(alignFactor));
        float _dDepth = inLastDepth/_numLayers;
        vec2 _dtex = depthScale * inLightDir.xy/(inLightDir.z * _numLayers);
        
        int numSamplesUnderSurface = 0;
        
        float currentLayerDepth = inLastDepth - _dDepth;
        vec2 currentTexCoords = inTexCoords + _dtex;
        
        float currentDepthValue = texture(texture_displacement_map_1, currentTexCoords).r;
        
        float stepIndex = 1.f;
        
        while (currentLayerDepth > 0.f) {
            if (currentDepthValue < currentLayerDepth)
            {
                numSamplesUnderSurface++;
                float currentShadowMultiplier = (currentLayerDepth - currentDepthValue) * (1.f - stepIndex/_numLayers);
                
                shadowMultiplier = max(shadowMultiplier, currentShadowMultiplier);
            }
            stepIndex++;
            currentLayerDepth -= _dDepth;
            currentTexCoords += _dtex;
            currentDepthValue = texture(texture_displacement_map_1, currentTexCoords).r;
        }
        
        if (numSamplesUnderSurface < 1)
            shadowMultiplier = 1.0;
        else
            shadowMultiplier = 1.0 - shadowMultiplier;
    }
    
    return shadowMultiplier;
}

/////////////////////////////////////////////////////////////

