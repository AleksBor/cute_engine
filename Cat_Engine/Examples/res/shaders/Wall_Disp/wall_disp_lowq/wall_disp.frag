#version 330 core

#include "DirLight.shader"

//Outputs
out vec4 FragColor;

//Inputs
in vec2 TexCoords;
in vec4 FragPos;
in vec3 Normal;
in vec4 FragPosLightSpace;

in mat3 normTransform;
in mat3 TBN;

uniform bool enableNormals;

//Material

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

//Light
uniform DirLight dirLight;

uniform vec3 viewPos;

uniform sampler2D texture_depth1;
uniform sampler2D texture_normal_map_1;
uniform sampler2D texture_displacement_map_1;

uniform vec2 size;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(texture_depth1, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    float bias = max(0.0015 * (1.0 - dot(normal, lightDir)), 0.0015);
    
    float shadow = 0.0;
    
    vec2 texelSize = 1.0 / textureSize(texture_depth1, 0);
    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            float pcfDepth = texture(texture_depth1, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    
    shadow /= 9.0;
    
    if (currentDepth > 1.0) {
        shadow = 0.0;
    }
    
    return shadow;
}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir);

void main()
{
    vec3 viewDir = normalize(viewPos - FragPos.xyz);
    
    vec2 texCoor = ParallaxMapping(TexCoords, normalize(inverse(TBN) * viewDir));
    
    if (texCoor.x > size.x || texCoor.y > size.y || texCoor.x < 0.0 || texCoor.y < 0.0) {
        discard;
    }
    
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = normalize(Normal);
    if (enableNormals) {
        norm = vec3(texture(texture_normal_map_1, texCoor));
        norm = normalize(norm * 2.0 - 1.0);
        norm = normalize(TBN * norm);
        //norm = normalize(normTransform * norm);
    }
    
    vec3 diffuseColor = vec3(texture(material.texture_diffuse1, texCoor));
    float shadow = ShadowCalculation(FragPosLightSpace, norm, dirLight.position);
    outputColor += CalcDirLight(dirLight, norm, viewDir, diffuseColor, shadow);
    
    FragColor = vec4(outputColor, 1.f);
}

//Light realization

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir) {
    const float minLayers = 4.0;
    const float maxLayers = 32.0;
    
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    
    float layerDepth = 1.0f / numLayers;
    
    float currentLayerDepth = 0.0;
    
    vec2 P = viewDir.xy * 0.1f;
    vec2 deltaTexCoords = P / numLayers;
    
    vec2 currentTexCoords = texCoords;
    float currentDepthMapValue = texture(texture_displacement_map_1, currentTexCoords).r;
    
    while(currentLayerDepth < currentDepthMapValue)
    {
        currentTexCoords -= deltaTexCoords;
        
        currentDepthMapValue = texture(texture_displacement_map_1, currentTexCoords).r;
        
        currentLayerDepth += layerDepth;
    }
    
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;
    
    float afterDepth = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(texture_displacement_map_1, prevTexCoords).r - currentLayerDepth + layerDepth;
    
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);
    
    return finalTexCoords;
}

/////////////////////////////////////////////////////////////
