#version 330 core

#include "DirLight.shader"
#include "PointLight.shader"

//Outputs
out vec4 FragColor;

//Inputs
in vec2 TexCoords;
in vec4 FragPos;
in vec3 Normal;
in vec4 FragPosLightSpace;

//Material

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

//Light

uniform vec3 viewPos;

uniform PointLight pointLight;

uniform DirLight dirLight;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 viewDir);
/////////////////////////////////////////////////////////////

uniform sampler2D texture_depth1;
uniform samplerCube texture_cubemap_depth_1;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(texture_depth1, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    float bias = max(0.0015 * (1.0 - dot(normal, lightDir)), 0.0015);
    
    float shadow = 0.0;
    
    vec2 texelSize = 1.0 / textureSize(texture_depth1, 0);
    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            float pcfDepth = texture(texture_depth1, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    
    shadow /= 9.0;
    
    if (currentDepth > 1.0) {
        shadow = 0.0;
    }
    
    return shadow;
}

void main()
{
    vec3 outputColor = vec3(0.0);
    
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos.xyz);
    
    vec3 diffuseColor = vec3(texture(material.texture_diffuse1, TexCoords));
    float shadow = ShadowCalculation(FragPosLightSpace, norm, dirLight.position);
    outputColor += CalcDirLight(dirLight, norm, viewDir, diffuseColor, shadow);
	outputColor += CalcPointLight(pointLight, norm, viewDir);
    
    FragColor = vec4(outputColor, 1.f);
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - FragPos.xyz);
    
    float diffKoef = max(dot(normal, lightDir) , 0.0);
    
    vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse1, TexCoords));
    vec3 diffuse = light.diffuse * diffKoef * vec3(texture(material.texture_diffuse1, TexCoords));
    
    float distance = length(light.position - FragPos.xyz);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    
    float shadow = PointLightShadowCalculation(light, FragPos, normal, texture_cubemap_depth_1);
    
    return attenuation * (ambient + (1 - shadow) * diffuse);
}

/////////////////////////////////////////////////////////////
