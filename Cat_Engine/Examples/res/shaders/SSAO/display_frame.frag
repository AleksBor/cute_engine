#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

struct Material {
    sampler2D texture_diffuse1;
};
uniform Material material;

void main()
{
	vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
    float AmbientOcclusion = texture(material.texture_diffuse1, TexCoords).r;
    //FragColor = vec4(vec3(AmbientOcclusion), 1.0);
	vec3 ambient = vec3(0.8 * Diffuse * AmbientOcclusion);
	FragColor = vec4(ambient, 1.0);
}
