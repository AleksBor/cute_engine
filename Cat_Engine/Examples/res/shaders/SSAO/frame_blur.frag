#version 330 core
out float FragColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
};
uniform Material material;

void main()
{
	vec2 texelSize = 1.0 / vec2(textureSize(material.texture_diffuse1, 0));
	float result = 0.0;
	for (int x = -2; x < 2; ++x) {
		for (int y = -2; y < 2; ++y) {
			vec2 offset = vec2(float(x), float(y)) * texelSize;
			result += texture(material.texture_diffuse1, TexCoords + offset).r;
		}
    }
	FragColor = result / (4.0 * 4.0);
}
