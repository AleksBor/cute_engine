#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 FragPos;
out vec3 Normal;

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

uniform mat4 model;

uniform mat4 normalTransform;

void main()
{
    /*
    Normal = mat3(normalTransform) * aNormal;
    TexCoords = aTexCoords;
    FragPos = vec3(view * model * vec4(aPos, 1.0));
    gl_Position = projection * view * model * vec4(aPos, 1.0);*/
    
    vec4 viewPos = view * model * vec4(aPos, 1.0);
    FragPos = viewPos.xyz;
    TexCoords = aTexCoords;
    
    mat3 normalMatrix = transpose(inverse(mat3(view * model)));
    Normal = normalMatrix * aNormal;
    
    gl_Position = projection * viewPos;
}
