#shader vertex
#version 330 core

layout(location = 0) in vec3 position;

uniform mat4 model;

void main()
{
    gl_Position = model * vec4(position.xyz, 1.0);
}

#shader fragment
#version 330 core

out vec4 resultColor;

void main()
{
	resultColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
