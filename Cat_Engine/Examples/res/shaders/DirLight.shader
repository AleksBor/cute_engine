struct DirLight {
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec3 diffuseColor, float shadow)
{
    float diffKoef = max(dot(normal, light.position), 0.0);
    
    vec3 ambient = light.ambient * diffuseColor;
    vec3 diffuse = light.diffuse * diffKoef * diffuseColor;
    
    return (ambient + ((1.0 - shadow) * diffuse));
}
