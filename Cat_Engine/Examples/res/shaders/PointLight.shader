struct PointLight {
    vec3 position;
    
    float far_plane;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};

float PointLightShadowCalculation(PointLight light, vec4 fragPos, vec3 normal, samplerCube cubemap_depth)
{
    vec3 fragToLight = fragPos.xyz - light.position;
    float closestDepth = texture(cubemap_depth, fragToLight).r;
    closestDepth *= light.far_plane;
    float currentDepth = length(fragToLight);
    float bias = max(0.05 * (1.0 - dot(normal, normalize(-fragToLight))), 0.002);
    float shadow = (currentDepth - bias) > closestDepth ? 1.0 : 0.0;
    return shadow;
}
