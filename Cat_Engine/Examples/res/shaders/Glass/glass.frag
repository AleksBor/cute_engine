#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

void main()
{
    FragColor = vec4(0.0f, 1.0f, 0.0f, 0.6f);
}
