#shader vertex
#version 330 core

layout(location = 0) in vec3 position;

uniform mat4 model;

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

void main()
{
    gl_Position = projection * view * model * vec4(position.xyz, 1.0);
}

#shader fragment
#version 330 core

out vec4 resultColor;

void main()
{
   //resultColor = vec4(1.0, 0.98, 0.1, 1.0f);
	resultColor = vec4(1.0, 0.0, 0.0, 1.0f);
}
