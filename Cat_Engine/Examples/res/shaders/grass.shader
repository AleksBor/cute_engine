#shader vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
layout (location = 3) in vec2 aOffset;

out vec2 TexCoords;

uniform mat4 model;

layout (std140) uniform FPSCamera
{
    uniform mat4 projection;
    uniform mat4 view;
};

void main()
{
    TexCoords = aTexCoords;
    gl_Position = projection * view * model * vec4(aPos.x + aOffset.x, aPos.y, aPos.z + aOffset.y, 1.0);
}

#shader fragment
#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
};

uniform Material material;

void main()
{
    vec4 texColor = texture(material.texture_diffuse1, TexCoords);
    if (texColor.a < 0.1f)
    {
        discard;
    }
    FragColor = vec4(vec3(texColor), 1.0f);
}
