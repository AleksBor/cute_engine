#pragma once
class MainGUI
{
public:
	MainGUI() {}
	MainGUI(const MainGUI&);
	MainGUI& operator = (MainGUI&);
	int selectedRadioButton = 0;
	void Display();
	static MainGUI& Instance() {
		static MainGUI instance;
		return instance;
	}
};

