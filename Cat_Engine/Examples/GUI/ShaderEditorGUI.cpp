#include "ShaderEditorGUI.h"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

char ShaderEditorGUI::textBuffer[c_MaxShaderTextSize] =
"void main()\n"
"{\n"
"resultColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);\n"
"}";

void ShaderEditorGUI::Display() {
    ImGui::Begin("Shader");

    ImGui::InputTextMultiline("", ShaderEditorGUI::textBuffer, sizeof(char) * c_MaxShaderTextSize, ImVec2(500, 150));

    if (ImGui::Button("Run"))
    {
        run_callback();
    }

    ImGui::End();
}
