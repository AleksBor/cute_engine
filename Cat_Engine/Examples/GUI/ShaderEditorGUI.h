#pragma once

const int c_MaxShaderTextSize = 1024;

class ShaderEditorGUI
{
private:
	ShaderEditorGUI() {}
public:
	
	ShaderEditorGUI(const ShaderEditorGUI&) = delete;
	ShaderEditorGUI& operator = (ShaderEditorGUI&) = delete;
	void Display();
	static ShaderEditorGUI& Instance() {
		static ShaderEditorGUI instance;
		return instance;
	}

	void (*run_callback)();

	static char textBuffer[c_MaxShaderTextSize];
};

