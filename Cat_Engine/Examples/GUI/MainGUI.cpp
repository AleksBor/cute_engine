#include "MainGUI.h"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

void MainGUI::Display() {
    ImGui::Begin("Examples");

    ImGui::RadioButton("Village", &selectedRadioButton, 0);
    ImGui::RadioButton("Post processing", &selectedRadioButton, 1);
    ImGui::RadioButton("SSAO", &selectedRadioButton, 2);
    ImGui::RadioButton("Shader editor", &selectedRadioButton, 3);

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    ImGui::End();
}
