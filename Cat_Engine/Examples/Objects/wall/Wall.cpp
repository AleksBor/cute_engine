//
//  Wall.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 05/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Wall.hpp"
#include "Defines.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include "ShaderProgram.h"

Wall::Wall(Camera* camera, shader_program_ptr dirLightShader, DirLight sun, OrthoCamera* orthoCam, bool* enableNormalMaping, bool* enableTangent, glm::vec2 size, std::vector<texture_ptr> textures) :
Quad( QuadOptions{ { size.x / size.y, 1.f }, static_cast<int>(VERTEX_PARAMS::NORMALS) | static_cast<int>(VERTEX_PARAMS::TEX_COORDS) | static_cast<int>(VERTEX_PARAMS::TBN) }, s_QuadVerticalCoords), cam(camera), m_dirLightShader(dirLightShader), m_sun(sun), orthoCamera(orthoCam), m_enableNormalMaping(enableNormalMaping), m_enableTangent(enableTangent)
{
    shader = ShaderProgram::CreateShaderFromPath("Wall/wall_usual/wall.vert", "Wall/wall_usual/wall.frag");
    tan_shader = ShaderProgram::CreateShaderFromPath("Wall/wall_tangent/wall_tangent.vert", "Wall/wall_tangent/wall_tangent.frag");
    
    model = glm::translate(model, glm::vec3(4.f, -0.35f, 0.0f));
    model = glm::rotate(model, (GLfloat)(-M_PI_2), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(size.x, size.y, size.y));
    shader->AttachCamera(cam, 0);
    AddTexture(TEXTURE_ROOT_PATH + "Wall/brickwall.jpg", TEXTURE_DIFFUSE);
    AddTexture(TEXTURE_ROOT_PATH + "Wall/brickwall_normal.jpg", TEXTURE_NORMAL_MAP);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void Wall::Process(int tag)
{
    if (tag == 0) {
        if (!*m_enableTangent)
        {
            shader->Use();
            bindTextures(shader);

            shader->setUniform("enableNormals", *m_enableNormalMaping);
            
            glm::mat4 normalTransform = glm::transpose(glm::inverse(model));

            shader->setUniform("normalTransform", normalTransform);
            
            shader->setUniform("material.shininess", 128.f * 0.6f);

            shader->setUniform("viewPos", cam->pos);
            
            shader->setUniform("dirLight", m_sun);
            
            shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
            shader->setUniform("model", model);
            
            Draw();
        } else {
            tan_shader->Use();
            bindTextures(tan_shader);

            tan_shader->setUniform("enableNormals", *m_enableNormalMaping);
            
            glm::mat4 normalTransform = glm::transpose(glm::inverse(model));

            tan_shader->setUniform("normalTransform", normalTransform);
            
            tan_shader->setUniform("material.shininess", 128.f * 0.6f);

            tan_shader->setUniform("viewPos", cam->pos);
            
            tan_shader->setUniform("dirLight", m_sun);
            tan_shader->setUniform("lightPosition", m_sun.position);
            
            tan_shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
            tan_shader->setUniform("model", model);
            
            Draw();
        }
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", model);
        Draw();
    }
}
