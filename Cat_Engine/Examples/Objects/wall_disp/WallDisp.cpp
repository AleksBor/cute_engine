//
//  WallDisp.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 17/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "WallDisp.hpp"
#include "Defines.h"

#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

WallDisp::WallDisp(Camera* camera, 
    shader_program_ptr dirLightShader,
    DirLight sun, 
    OrthoCamera* orthoCam, 
    bool* enableNormalMaping, 
    bool* enableTangent, 
    glm::vec2 size, 
    std::vector<texture_ptr> textures, 
    glm::mat4 modelMat,
    bool hightQuality) :
Quad( QuadOptions{ { size.x / size.y, 1.f }, static_cast<int>(VERTEX_PARAMS::NORMALS) | static_cast<int>(VERTEX_PARAMS::TEX_COORDS) | static_cast<int>(VERTEX_PARAMS::TBN) }, s_QuadVerticalCoords), cam(camera), m_dirLightShader(dirLightShader), m_sun(sun), orthoCamera(orthoCam), m_enableNormalMaping(enableNormalMaping), m_enableTangent(enableTangent), model(modelMat)
{
    scale = size.x / size.y;

    if (hightQuality) {
        shader = ShaderProgram::CreateShaderFromPath("Wall_Disp/wall_disp_highq/wall_disp.vert", "Wall_Disp/wall_disp_highq/wall_disp.frag");
    } else {
        shader = ShaderProgram::CreateShaderFromPath("Wall_Disp/wall_disp_lowq/wall_disp.vert", "Wall_Disp/wall_disp_lowq/wall_disp.frag");
    }
    
    
    shader->AttachCamera(cam, 0);
    AddTexture(TEXTURE_ROOT_PATH + "Wall2/bricks2.jpg", TEXTURE_DIFFUSE);
    AddTexture(TEXTURE_ROOT_PATH + "Wall2/bricks2_normal.jpg", TEXTURE_NORMAL_MAP);
    AddTexture(TEXTURE_ROOT_PATH + "Wall2/bricks2_disp.jpg", TEXTURE_DISPLACEMENT_MAP);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void WallDisp::Process(int tag)
{
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);

        shader->setUniform("enableNormals", *m_enableNormalMaping);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(model));

        shader->setUniform("normalTransform", normalTransform);
        
        shader->setUniform("material.shininess", 128.f * 0.6f);

        shader->setUniform("viewPos", cam->pos);
        
        shader->setUniform("dirLight", m_sun);
        
        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", model);
        
        shader->setUniform("size", scale, 1.f);
        
        Draw();
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", model);
        Draw();
    }
}
