//
//  WallDisp.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 17/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

#include "OrthoCamera.hpp"
#include "DirLight.hpp"

class WallDisp: public Quad<BasicItem> {
private:
    OrthoCamera* orthoCamera;
    Camera* cam;
    
    glm::vec3 sunPos;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    
    DirLight m_sun;

    bool* m_enableNormalMaping;
    bool* m_enableTangent;
    
    glm::mat4 model = glm::mat4(1);
    float scale;
public:
    WallDisp() = delete;
    WallDisp(Camera* camera, 
        shader_program_ptr dirLightShader,
        DirLight sun, 
        OrthoCamera* orthoCam, 
        bool* enableNormalMaping, 
        bool* enableTangent, 
        glm::vec2 size, 
        std::vector<texture_ptr> textures, 
        glm::mat4 modelMat,
        bool hightQuality);
    
    void Process(int tag) override;
};
