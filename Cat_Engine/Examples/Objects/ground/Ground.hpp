//
//  Ground.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 29/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"
#include "OrthoCamera.hpp"
#include "PointLight.hpp"
#include "DirLight.hpp"

class Ground: public Quad<BasicItem> {
private:
    OrthoCamera* orthoCamera;
    glm::vec3 sunPos;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    shader_program_ptr m_pointLightShader;
    
    PointLight m_lamp;
    DirLight m_sun;
    
    Camera* cam;
public:
    
    Ground() = delete;
    Ground(Camera* camera, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures);
    
    void Process(int tag) override;
};
