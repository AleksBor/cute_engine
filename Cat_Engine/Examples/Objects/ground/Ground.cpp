//
//  Ground.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 29/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Ground.hpp"
#include "ShaderProgram.h"
#include "Defines.h"
#include "Assertion.h"

Ground::Ground(Camera* camera, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures):
Quad( QuadOptions{ {80, 80} } ), cam(camera), m_sun(sun), orthoCamera(orthoCam), m_dirLightShader(dirLightShader), m_pointLightShader(pointLightShader), m_lamp(lamp)
{
    shader = ShaderProgram::CreateShaderFromPath("Ground/ground.vert", "Ground/ground.frag");
    setPosition(glm::vec3(0.0f, -0.5f, 0.0f));
    setScale(glm::vec3(40.0f, 1.0f, 40.0f));
    shader->AttachCamera(cam, 0);
    AddTexture(TEXTURE_ROOT_PATH + "Grass/Seamless_golf_green_grass_texture.jpg", TEXTURE_DIFFUSE);
    AddTexture(TEXTURE_ROOT_PATH + "Grass/Seamless_golf_green_grass_texture_SPECULAR.jpg", TEXTURE_SPECULAR);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
    
    STATIC_ASSERT(sizeof(int) == 4);
    //STATIC_ASSERT(sizeof(int) == 1);
}

void Ground::Process(int tag) {
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

        shader->setUniform("normalTransform", normalTransform);
        
        shader->setUniform("material.shininess", 128.f * 0.6f);

        shader->setUniform("viewPos", cam->pos);
        
        shader->setUniform("dirLight", m_sun);
        shader->setUniform("pointLight", m_lamp);
        
        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", GetModelMatrix());
        Draw();
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", GetModelMatrix());
        Draw();
    } else {
        m_pointLightShader->setUniform("model", GetModelMatrix());
        Draw();
    }
}
