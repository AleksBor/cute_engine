//
//  Nanosuit.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 12/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Model.hpp"

class Nanosuit: public Model<BasicItem> {
private:
    shader_program_ptr shader;
    shader_program_ptr g_buffer_shader;
    
    glm::vec3* m_lightPositions;
public:
    Nanosuit() = delete;
    Nanosuit(const char* path, Camera* cam, glm::vec3* lightPositions);
    
    void ProcessMesh(int tag, mesh_ptr mesh) override;
};
