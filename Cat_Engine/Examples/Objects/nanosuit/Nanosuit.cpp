//
//  Nanosuit.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 12/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Nanosuit.hpp"
#include "ShaderProgram.h"

Nanosuit::Nanosuit(const char* path, Camera* cam, glm::vec3* lightPositions) : Model(path), m_lightPositions(lightPositions)
{
    shader = ShaderProgram::CreateShaderFromPath("Nanosuit/nanosuit.vert", "Nanosuit/nanosuit.frag");
    g_buffer_shader = ShaderProgram::CreateShaderFromPath("Nanosuit/g_nanosuit.vert", "Nanosuit/g_nanosuit.frag");
    
    setScale(glm::vec3(0.05f, 0.05f, 0.05f));
    
    shader->AttachCamera(cam, 0);
    g_buffer_shader->AttachCamera(cam, 0);
    
    std::vector<glm::vec2> offset;
    
    for (int i = 0; i < 10; i++) {
        for (int c = 0; c < 10; c++) {
            offset.push_back(glm::vec2(i * 10, c * 10));
        }
    }
    
    AddInstances(offset);
}

void Nanosuit::ProcessMesh(int tag, mesh_ptr mesh)
{
    if (tag == 0) {
        shader->Use();
        mesh->bindTextures(shader);
        shader->setUniform("model", GetModelMatrix());
        
        mesh->Draw(100);
    } else if (tag == 1) {
        g_buffer_shader->Use();
        
        mesh->bindTextures(g_buffer_shader);
        g_buffer_shader->setUniform("model", GetModelMatrix());
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

        g_buffer_shader->setUniform("normalTransform", normalTransform);
           
        mesh->Draw(100);
    }
}
