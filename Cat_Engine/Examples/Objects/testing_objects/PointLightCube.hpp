//
//  PointLightCube.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Cube.hpp"

class PointLightCube : public Cube {
private:
    shader_program_ptr shader;
public:
    PointLightCube() = delete;
    PointLightCube(Camera* camera);
    
    void Process(int tag) override;
};
