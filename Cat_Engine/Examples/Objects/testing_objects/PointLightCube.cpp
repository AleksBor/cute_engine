//
//  PointLightCube.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "PointLightCube.hpp"
#include "ShaderProgram.h"

PointLightCube::PointLightCube(Camera* camera)
{
    shader = ShaderProgram::CreateShaderFromPath("DepthCubeMap/depth_cube_map.vert", "DepthCubeMap/depth_cube_map.frag");
    shader->AttachCamera(camera, 0);
    setPosition(glm::vec3(2.f, 0.1f, 0.0f));
    setScale(glm::vec3(0.5f, 0.5f, 0.5f));
}

void PointLightCube::Process(int tag) {
    shader->Use();
    bindTextures(shader);
    shader->setUniform("model", GetModelMatrix());
    Draw();
}
