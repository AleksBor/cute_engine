//
//  SSAOFrame.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "SSAOFrame.hpp"
#include "ShaderProgram.h"
#include "GeometryFrameBuffer.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

#include <random>

float lerp(float a, float b, float f)
{
    return a + f * (b - a);
}

SSAOFrame::SSAOFrame(const camera_ptr& camera, const geometry_frame_buffer_ptr& geometryFrameBuffer) : 
    Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS) }, s_QuadVerticalCoords), 
    cam(camera),
    m_GeometryFrameBuf(geometryFrameBuffer)
{
    shader = ShaderProgram::CreateShaderFromPath("SSAO/frame.vert", "SSAO/frame.frag");
    
    setPosition(glm::vec3(0.0f, 0.0f, 0.9999999f));
    setScale(glm::vec3(2.0f, 2.0f, 2.0f));
    setRotation(M_PI, glm::vec3(0.0f, 1.0f, 0.0f));
    
    std::uniform_real_distribution<float> randomFloats(0.0f, 1.0f);
    std::default_random_engine generator;
    for (unsigned int i = 0; i < 64; ++i)
    {
        glm::vec3 sample(
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator)
        );
        
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);
        float scale = (float)i / 64.0;
        //ssaoKernel.push_back(sample);
        
        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.push_back(sample);
        
        //std::cout << "x = " << sample.x << " y = " << sample.y << " z = " << sample.z << std::endl;
    }
    
    std::vector<glm::vec3> ssaoNoise;
    for (unsigned int i = 0; i < 16; i++)
    {
        glm::vec3 noise(
            randomFloats(generator) * 2.0 - 1.0,
            randomFloats(generator) * 2.0 - 1.0,
            0.0f
        );
        ssaoNoise.push_back(noise);
    }
    
    noiseTexture = Texture::CreatePositionTexture(4, 4, ssaoNoise.data());
    noiseTexture->name = "texNoise";
    
    AddTexture(m_GeometryFrameBuf->getGPositionTex());
    AddTexture(m_GeometryFrameBuf->getGNormalTex());
    AddTexture(m_GeometryFrameBuf->getGColorSpecTex());
    AddTexture(noiseTexture);
}

void SSAOFrame::Process(int tag)
{
    if (tag == 2) {
        shader->Use();
        
        bindTextures(shader);
        //bindTexture(m_GeometryFrameBuf->getGPositionTex(), "gPosition", shader, 0);
        //bindTexture(m_GeometryFrameBuf->getGNormalTex(), "gNormal", shader, 1);
        //bindTexture(m_GeometryFrameBuf->getGColorSpecTex(), "gAlbedoSpec", shader, 2);
        //bindTexture(noiseTexture, "texNoise", shader, 3);

        shader->setUniform("model", GetModelMatrix());
        
        int i = 0;
        for(auto it = ssaoKernel.begin(); it != ssaoKernel.end(); it++, ++i)    {
            shader->setUniform(("samples[" + std::to_string(i) + "]").c_str(), *it);
        }

        shader->setUniform("projection", cam->projection());
        
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        Draw();
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
    }
}
