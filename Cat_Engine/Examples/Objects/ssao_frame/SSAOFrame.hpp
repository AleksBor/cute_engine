//
//  SSAOFrame.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"
#include "types.h"

class SSAOFrame;

typedef std::shared_ptr<SSAOFrame> ssao_frame_ptr;

class SSAOFrame : public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    
    glm::vec3* m_lightPositions;//delete
    
    std::vector<glm::vec3> ssaoKernel;
    
    texture_ptr noiseTexture;

    camera_ptr cam;

    geometry_frame_buffer_ptr m_GeometryFrameBuf;
public:
    SSAOFrame() = delete;
    SSAOFrame(const camera_ptr& camera, const geometry_frame_buffer_ptr &geometryFrameBuffer);
    
    void Process(int tag) override;
};
