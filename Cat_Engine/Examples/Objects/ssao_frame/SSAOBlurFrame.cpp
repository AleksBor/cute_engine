//
//  SSAOBlurFrame.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 17.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "SSAOBlurFrame.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

SSAOBlurFrame::SSAOBlurFrame():
Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS)}, s_QuadVerticalCoords)
{
    shader = ShaderProgram::CreateShaderFromPath("SSAO/frame_blur.vert", "SSAO/frame_blur.frag");
    
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.9999999f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
}

void SSAOBlurFrame::Process(int tag) {
    if (tag == 3) {
        shader->Use();
        
        bindTextures(shader);
        
        shader->setUniform("model", model);
        
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        Draw();
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
    }
}
