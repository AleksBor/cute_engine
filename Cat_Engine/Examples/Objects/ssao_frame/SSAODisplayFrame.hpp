//
//  SSAODisplayFrame.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 15.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

class SSAODisplayFrame : public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    
    glm::mat4 model = glm::mat4(1);
public:
    SSAODisplayFrame();
    
    void Process(int tag) override;
};
