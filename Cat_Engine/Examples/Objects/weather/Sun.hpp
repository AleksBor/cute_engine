//
//  Sun.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Sphere.hpp"

class Sun: public Sphere
{
private:
    glm::vec3 m_sunPosition;
    shader_program_ptr shader;
    Camera* m_camera;
public:
    Sun() = delete;
    Sun(Camera* camera, glm::vec3 sunPosition);
    
    void Process(int tag) override;
};
