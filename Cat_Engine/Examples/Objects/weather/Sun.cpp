//
//  Sun.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Sun.hpp"
#include "ShaderProgram.h"

Sun::Sun(Camera* camera, glm::vec3 sunPosition) : Sphere(0.4f, static_cast<int>(VERTEX_PARAMS::NORMALS)), m_camera(camera), m_sunPosition(sunPosition) {
    shader = ShaderProgram::CreateShaderFromPath("Sun.shader");
    shader->AttachCamera(camera, 0);
}

void Sun::Process(int tag) {
    shader->Use();
    setPosition(m_sunPosition + m_camera->pos);
    shader->setUniform("model", GetModelMatrix());
    Draw();
}
