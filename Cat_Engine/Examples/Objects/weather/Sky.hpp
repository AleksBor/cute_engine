//
//  Sky.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Cube.hpp"

class Sky: public Cube {
private:
    shader_program_ptr shader;
public:
    Sky() = delete;
    Sky(Camera* camera);
    
    void Process(int tag) override;
};
