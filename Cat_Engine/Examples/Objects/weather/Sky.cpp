//
//  Sky.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Sky.hpp"
#include "Defines.h"
#include "ShaderProgram.h"

Sky::Sky(Camera* camera) {
    std::vector<std::string> faces =
    {
        TEXTURE_ROOT_PATH + "skybox/right.jpg",
        TEXTURE_ROOT_PATH + "skybox/left.jpg",
        TEXTURE_ROOT_PATH + "skybox/top.jpg",
        TEXTURE_ROOT_PATH + "skybox/bottom.jpg",
        TEXTURE_ROOT_PATH + "skybox/front.jpg",
        TEXTURE_ROOT_PATH + "skybox/back.jpg"
    };

    texture_ptr cubemapTexture = Texture::CreateСubeMapTexture(faces);
    
    shader = ShaderProgram::CreateShaderFromPath("CubeMap/cube_map.vert", "CubeMap/cube_map.frag");
    
    AddTexture(cubemapTexture);
    shader->AttachCamera(camera, 0);
}

void Sky::Process(int tag)
{
    glDisable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);
    if (tag == 0)
    {
        bindTextures(shader);
        shader->Use();
        Draw();
    }
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
}
