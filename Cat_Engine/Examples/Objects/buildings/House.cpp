//
//  House.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 05/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "House.hpp"
#include "ShaderProgram.h"

House::House(const char* path, Camera* cam, shader_program_ptr dirLightShader, DirLight sun, OrthoCamera* orthoCam, bool* enableNormalMaping, std::vector<texture_ptr> textures) : Model(path), m_sun(sun), camera(cam), orthoCamera(orthoCam), m_dirLightShader(dirLightShader), m_enableNormalMaping(enableNormalMaping) {
    shader = ShaderProgram::CreateShaderFromPath("House/house.vert", "House/house.frag");
    shader->AttachCamera(camera, 0);
    setPosition(glm::vec3(6.0f, -0.507f, 0.0f));
    setScale(glm::vec3(0.008f, 0.008f, 0.008f));
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void House::ProcessMesh(int tag, mesh_ptr mesh)
{
    if (tag == 0) {
        shader->Use();
        mesh->bindTextures(shader);
        
        shader->setUniform("enableNormals", *m_enableNormalMaping);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

        shader->setUniform("normalTransform", normalTransform);

        shader->setUniform("viewPos", camera->pos);
        
        shader->setUniform("dirLight", m_sun);

        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", GetModelMatrix());
        
        mesh->Draw();
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", GetModelMatrix());
        
        mesh->Draw();
    }
}
