//
//  House.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 05/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Model.hpp"
#include "OrthoCamera.hpp"
#include "DirLight.hpp"

class House: public Model<BasicItem> {
private:
    OrthoCamera* orthoCamera;
    Camera* camera = NULL;
    
    DirLight m_sun;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    
    bool* m_enableNormalMaping;
public:
    House() = delete;
    House(const char* path, Camera* cam, shader_program_ptr dirLightShader, DirLight sun, OrthoCamera* orthoCam, bool* enableNormalMaping, std::vector<texture_ptr> textures);
    
    void ProcessMesh(int tag, mesh_ptr mesh) override;
};
