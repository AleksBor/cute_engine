//
//  Cottage.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 24/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Cottage.hpp"
#include "ShaderProgram.h"

Cottage::Cottage(const char* path, Camera* cam, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures) :
    Model(path), camera(cam), m_sun(sun), orthoCamera(orthoCam), m_pointLightShader(pointLightShader), m_dirLightShader(dirLightShader), m_lamp(lamp)
{
    shader = ShaderProgram::CreateShaderFromPath("Cottage/cottage.vert", "Cottage/cottage.frag");
    shader->AttachCamera(camera, 0);
    setPosition(glm::vec3(0.5f, -0.507f, 0.0f));
    setScale(glm::vec3(0.2f, 0.2f, 0.2f));

    shader->Use();
    glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

    shader->setUniform("normalTransform", normalTransform);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void Cottage::ProcessMesh(int tag, mesh_ptr mesh)
{
    if (tag == 0 || tag == 3) {
        shader->Use();
        mesh->bindTextures(shader);
        //glm::mat4 normalTransform = glm::transpose(glm::inverse(modelMatrix));

        //shader->setUniform("normalTransform", normalTransform);

        shader->setUniform("viewPos", camera->pos);
        
        shader->setUniform("dirLight", m_sun);
        shader->setUniform("pointLight", m_lamp);

        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", GetModelMatrix());
        
        mesh->Draw();
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", GetModelMatrix());
        mesh->Draw();
    } else {
        m_pointLightShader->setUniform("model", GetModelMatrix());
        mesh->Draw();
    }
}
