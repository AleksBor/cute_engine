//
//  Roof.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 07/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

#include "OrthoCamera.hpp"
#include "DirLight.hpp"

class Roof: public Quad<Composite>
{
private:
    OrthoCamera* orthoCamera;
    Camera* cam;
    
    glm::vec3 sunPos;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    
    DirLight m_sun;

    bool* m_enableNormalMaping;
public:
    Roof() = delete;
    Roof(Camera* camera, shader_program_ptr dirLightShader, DirLight sun, OrthoCamera* orthoCam, bool* enableNormalMaping, std::vector<texture_ptr> textures);
    
    void ComponentProcess(int tag) override;
};
