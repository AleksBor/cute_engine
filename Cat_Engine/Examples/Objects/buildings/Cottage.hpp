//
//  Cottage.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 24/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Model.hpp"
#include "OrthoCamera.hpp"
#include "PointLight.hpp"
#include "DirLight.hpp"
#include "ComplexItem.hpp"

class Cottage: public Model<Composite> {
private:
    OrthoCamera* orthoCamera;
    Camera* camera = NULL;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    shader_program_ptr m_pointLightShader;
    
    PointLight m_lamp;
    DirLight m_sun;
public:

    Cottage() = delete;
    Cottage(const char* path, Camera* cam, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures);

    void ProcessMesh(int tag, mesh_ptr mesh) override;
    
    glm::vec3 sunPos;
};
