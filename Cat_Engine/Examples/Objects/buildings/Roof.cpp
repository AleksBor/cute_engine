//
//  Roof.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 07/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Roof.hpp"
#include "Defines.h"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

Roof::Roof(Camera* camera, shader_program_ptr dirLightShader, DirLight sun, OrthoCamera* orthoCam, bool* enableNormalMaping, std::vector<texture_ptr> textures) :
Quad( QuadOptions{ { 1.f, 2.07 / 1.2 }, static_cast<int>(VERTEX_PARAMS::NORMALS) | static_cast<int>(VERTEX_PARAMS::TEX_COORDS) | static_cast<int>(VERTEX_PARAMS::TBN) } ), cam(camera), m_dirLightShader(dirLightShader), m_sun(sun), orthoCamera(orthoCam), m_enableNormalMaping(enableNormalMaping)
{
    shader = ShaderProgram::CreateShaderFromPath("Concrete/concrete.vert", "Concrete/concrete.frag");
    setPosition(glm::vec3(0.0f, 0.807f, 0.0f));
    setScale(glm::vec3(1.2f, 1.2f, 2.07f));
    shader->AttachCamera(cam, 0);
    AddTexture(TEXTURE_ROOT_PATH + "Concrete/Seamless_Grey_Wall _Stucco Paint_Plaster Texturise.jpg", TEXTURE_DIFFUSE);
    AddTexture(TEXTURE_ROOT_PATH + "Concrete/Seamless_Patchwork_Pattern_Tiles_Texturise_NORMAL.jpg", TEXTURE_NORMAL_MAP);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void Roof::ComponentProcess(int tag)
{
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);

        shader->setUniform("enableNormals", *m_enableNormalMaping);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

        shader->setUniform("normalTransform", normalTransform);
        
        shader->setUniform("material.shininess", 128.f * 0.6f);

        shader->setUniform("viewPos", cam->pos);
        
        shader->setUniform("dirLight", m_sun);
        
        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", GetModelMatrix());
        //glDisable(GL_CULL_FACE);
        Draw();
        //glEnable(GL_CULL_FACE);
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", GetModelMatrix());
        Draw();
    }
}
