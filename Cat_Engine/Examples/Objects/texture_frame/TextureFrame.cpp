//
//  TextureFrame.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "TextureFrame.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

TextureFrame::TextureFrame(glm::vec3 translate, bool specular) : Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS) }, s_QuadVerticalCoords), m_specular(specular)
{
    shader = ShaderProgram::CreateShaderFromPath("TextureFrame/texture_frame.vert", "TextureFrame/texture_frame.frag");
    
    model = glm::translate(model, translate);
    model = glm::rotate(model, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
}

void TextureFrame::Process(int tag)
{
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);
        shader->setUniform("model", model);
        shader->setUniform("specular", m_specular);
        glDisable(GL_CULL_FACE);
        Draw();
        glEnable(GL_CULL_FACE);
    }
}
