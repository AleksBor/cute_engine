//
//  TextureFrame.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"
#include "types.h"

class TextureFrame;

typedef std::shared_ptr<TextureFrame> texture_frame_ptr;

class TextureFrame: public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    shader_program_ptr g_buffer_shader;
    
    bool m_specular;
    
    glm::mat4 model = glm::mat4(1);
public:
    TextureFrame(glm::vec3 translate, bool specular = false);
    
    void Process(int tag) override;
};
