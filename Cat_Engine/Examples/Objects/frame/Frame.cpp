//
//  Frame.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Frame.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

Frame::Frame() : Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS) }, s_QuadVerticalCoords)
{
    shader = ShaderProgram::CreateShaderFromPath("Frame/frame.vert", "Frame/frame.frag");
    pingPongShader = ShaderProgram::CreateShaderFromPath("Frame/ping_pong_frame.vert", "Frame/ping_pong_frame.frag");
    
    model = glm::translate(model, glm::vec3(0.5f, 0.5f, 0.0f));
    model = glm::rotate(model, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
}

void Frame::Process(int tag)
{
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);
        shader->setUniform("model", model);
        glDisable(GL_CULL_FACE);
        Draw();
        glEnable(GL_CULL_FACE);
    } else if (tag == 3) {
        pingPongShader->Use();
        //bindTextures(pingPongShader);
        bindTexture(pingpongTexture, "image", pingPongShader);
        auto modelMatrix = glm::scale(glm::mat4(1), glm::vec3(2.0f, 2.0f, 2.0f));
        modelMatrix = glm::rotate(modelMatrix, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
        pingPongShader->setUniform("model", modelMatrix);
        pingPongShader->setUniform("horizontal", horizontal);
        glDisable(GL_CULL_FACE);
        Draw();
        glEnable(GL_CULL_FACE);
    }
}
