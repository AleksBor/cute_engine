//
//  Frame.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

class Frame;

typedef std::shared_ptr<Frame> custom_frame_ptr;

class Frame: public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    shader_program_ptr pingPongShader;
    
    glm::mat4 model = glm::mat4(1);
public:
    texture_ptr pingpongTexture;
    bool horizontal;
    
    Frame();
    ~Frame() { std::cout << "FrameDeleted" << std::endl; }
    
    void Process(int tag) override;
};
