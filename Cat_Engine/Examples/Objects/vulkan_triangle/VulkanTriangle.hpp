//
//  VulkanTriangle.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 19.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#pragma once

#include "Triangle.h"

class VulkanTriangle: public Triangle<BasicItem> {
public:
    VulkanTriangle(int tag);
    
    void Process(int tag) override;
    
    Vertex* getVertexData() override;
private:
    int m_tag;
    
    std::vector<TriangleVertex> vertices;

    shader_program_ptr getShader() override;
};
