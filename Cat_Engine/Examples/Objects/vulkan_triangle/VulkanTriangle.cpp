//
//  VulkanTriangle.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 19.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#include "VulkanTriangle.hpp"
#include "ShaderProgram.h"
#include "Triangle.h"

VulkanTriangle::VulkanTriangle(int tag) : m_tag(tag) {
    
    switch (tag) {
        case 0:
            vertices = {
                {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
                {{0.0f, 0.5f}, {0.0f, 1.0f, 0.0f}},
                {{-1.0f, 0.5f}, {0.0f, 0.0f, 1.0f}}
            };
            break;
        case 1:
            vertices = {
                {{0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
                {{1.0f, 0.5f}, {0.0f, 1.0f, 0.0f}},
                {{0.0f, 0.5f}, {0.0f, 0.0f, 1.0f}}
            };
            break;
    }
}

shader_program_ptr VulkanTriangle::getShader() {
    if (m_tag == 0) {
        return ShaderProgram::CreateShaderFromPath("Vulkan/Vulkan.vert", "Vulkan/Vulkan.frag");
    }
    else {
        return ShaderProgram::CreateShaderFromPath("Vulkan/Vulkan.vert", "Vulkan/Vulkan.frag");
    }
}

Vertex* VulkanTriangle::getVertexData() {
    return vertices.data();
}

void VulkanTriangle::Process(int tag) {
    
}
