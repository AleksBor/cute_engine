//
//  SSAONanosuit.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Model.hpp"

class Camera;

class SSAONanosuit: public Model<BasicItem> {
private:
    shader_program_ptr g_buffer_shader;
    
    glm::mat4 model = glm::mat4(1);
    
    glm::vec3* m_lightPositions;
    
    Camera* m_camera;
public:
    SSAONanosuit() = delete;
    SSAONanosuit(const char* path, Camera* cam, glm::vec3* lightPositions);
    
    void ProcessMesh(int tag, mesh_ptr mesh) override;
};
