//
//  SSAONanosuit.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "SSAONanosuit.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

SSAONanosuit::SSAONanosuit(const char* path, Camera* cam, glm::vec3* lightPositions) : Model(path), m_lightPositions(lightPositions), m_camera(cam)
{
    g_buffer_shader = ShaderProgram::CreateShaderFromPath("SSAO/nanosuit.vert", "SSAO/nanosuit.frag");
    
    model = glm::translate(model, glm::vec3(0.0f, -0.4f, 0.0f));
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    model = glm::rotate(model, (GLfloat)(-M_PI_2), glm::vec3(1.0f, 0.0f, 0.0f));
    
    g_buffer_shader->AttachCamera(cam, 0);
}

void SSAONanosuit::ProcessMesh(int tag, mesh_ptr mesh)
{
    if (tag == 1) {
        g_buffer_shader->Use();
        
        mesh->bindTextures(g_buffer_shader);
        g_buffer_shader->setUniform("model", model);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(m_camera->view() * model));

        g_buffer_shader->setUniform("normalTransform", normalTransform);
           
        mesh->Draw(100);
    }
}
