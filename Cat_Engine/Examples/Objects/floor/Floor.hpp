//
//  Floor.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

class Floor: public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    
    Camera* cam;
public:
    
    Floor() = delete;
    Floor(Camera* camera);
    
    void Process(int tag) override;
};
