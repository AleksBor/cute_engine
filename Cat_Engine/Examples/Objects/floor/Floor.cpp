//
//  Floor.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Floor.hpp"
#include "ShaderProgram.h"
#include "Defines.h"

Floor::Floor(Camera* camera):
Quad( QuadOptions{ {80, 80} } ), cam(camera)
{
    shader = ShaderProgram::CreateShaderFromPath("Grass/grass.vert", "Grass/grass.frag");
    setPosition(glm::vec3(0.0f, -0.5f, 0.0f));
    setScale(glm::vec3(40.0f, 1.0f, 40.0f));
    shader->AttachCamera(cam, 0);
}

void Floor::Process(int tag) {
    if (tag == 1) {
        shader->Use();
        bindTextures(shader);

        shader->setUniform("viewPos", cam->pos);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(cam->view() * GetModelMatrix()));

        shader->setUniform("normalTransform", normalTransform);
        
        shader->setUniform("model", GetModelMatrix());
        Draw();
    }
}
