//
//  TerrainModel.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 19.05.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Terrain.hpp"
#include "OrthoCamera.hpp"
#include "PointLight.hpp"
#include "DirLight.hpp"

class TerrainModel: public Terrain {
private:
    Camera* m_camera;
    
    OrthoCamera* orthoCamera;
    glm::vec3 sunPos;
    
    shader_program_ptr shader;
    shader_program_ptr m_dirLightShader;
    shader_program_ptr m_pointLightShader;
    
    PointLight m_lamp;
    DirLight m_sun;
public:
    TerrainModel() = delete;
    TerrainModel(std::string filepath, Camera* camera, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures);
    
    void Process(int tag) override;
};
