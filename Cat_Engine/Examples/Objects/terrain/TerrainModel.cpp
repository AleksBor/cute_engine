//
//  TerrainModel.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 19.05.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "TerrainModel.hpp"
#include "Defines.h"
#include "ShaderProgram.h"

TerrainModel::TerrainModel(std::string filepath, Camera* camera, shader_program_ptr dirLightShader, shader_program_ptr pointLightShader, DirLight sun, OrthoCamera* orthoCam, PointLight lamp, std::vector<texture_ptr> textures) : Terrain(filepath, 40.f), m_camera(camera), m_sun(sun), orthoCamera(orthoCam), m_dirLightShader(dirLightShader), m_pointLightShader(pointLightShader), m_lamp(lamp)
{
    shader = ShaderProgram::CreateShaderFromPath("Ground/ground.vert", "Ground/ground.frag");
    //modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, -100.0f, 0.0f));
    
    setPosition(glm::vec3(0.0f, -0.5f, -0.0f));
    
    shader->AttachCamera(camera, 0);
    AddTexture(TEXTURE_ROOT_PATH + "Grass/Seamless_golf_green_grass_texture.jpg", TEXTURE_DIFFUSE);
    AddTexture(TEXTURE_ROOT_PATH + "Grass/Seamless_golf_green_grass_texture_SPECULAR.jpg", TEXTURE_SPECULAR);
    for (auto it = textures.begin(); it != textures.end(); ++it) {
        AddTexture(*it);
    }
}

void TerrainModel::Process(int tag) {
    
    if (tag == 0) {
        shader->Use();
        bindTextures(shader);
        
        glm::mat4 normalTransform = glm::transpose(glm::inverse(GetModelMatrix()));

        shader->setUniform("normalTransform", normalTransform);
        
        shader->setUniform("material.shininess", 128.f * 0.6f);

        shader->setUniform("viewPos", m_camera->pos);
        
        shader->setUniform("dirLight", m_sun);
        shader->setUniform("pointLight", m_lamp);
        
        shader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
        shader->setUniform("model", GetModelMatrix());
        Draw();
    } else if (tag == 1) {
        m_dirLightShader->setUniform("model", GetModelMatrix());
        Draw();
    } else {
        m_pointLightShader->setUniform("model", GetModelMatrix());
        Draw();
    }
}
