//
//  Glass.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"

class Glass: public Quad<Composite> {
private:
    shader_program_ptr shader;
public:
    Glass() = delete;
    Glass(Camera* camera);
    
    void ComponentProcess(int tag) override;
};
