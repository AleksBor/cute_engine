//
//  Glass.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Glass.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

Glass::Glass(Camera* camera) {
    shader = ShaderProgram::CreateShaderFromPath("Glass/glass.vert", "Glass/glass.frag");
    setPosition(glm::vec3(0.4f, 0.327f, 0.0f));
    setScale(glm::vec3(0.35f, 1.0f, 1.2f));
    setRotation(M_PI * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    addRotation(M_PI * 0.5f, glm::vec3(1.0f, 0.0f, 0.0f));
    addRotation(M_PI * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    shader->AttachCamera(camera, 0);
}

void Glass::ComponentProcess(int tag)
{
    glDisable(GL_CULL_FACE);
    if (tag == 0)
    {
        shader->Use();
        shader->setUniform("model", GetModelMatrix());
        glm::mat4 glassNormalTransform = glm::transpose(glm::inverse(GetModelMatrix()));
        shader->setUniform("normalTransform", glassNormalTransform);
        Draw();
    }
    glEnable(GL_CULL_FACE);
}
