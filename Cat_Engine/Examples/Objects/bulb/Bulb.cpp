//
//  Bulb.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 30/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Bulb.hpp"
#include "ShaderProgram.h"

Bulb::Bulb(const char* path, Camera* cam, shader_program_ptr pointLightShader) : Model(path), m_pointLightShader(pointLightShader) {
    setPosition(glm::vec3(0.f, 0.547f, 0.f));
    setScale(glm::vec3(0.001f, 0.001f, 0.001f));
    //shader = Shader::CreateShaderFromPath("1.model_loading.shader");
    shader = ShaderProgram::CreateShaderFromPath("Bulb/bulb_body.vert", "Bulb/bulb_body.frag");
    light_shader = ShaderProgram::CreateShaderFromPath("Bulb/bulb_light.vert", "Bulb/bulb_light.frag");
}

void Bulb::ProcessMesh(int tag, mesh_ptr mesh)
{
    if (tag == 0) {
        if (mesh->getOrderNumber() == 0) {
            mesh->bindTextures(shader);
            shader->Use();
            shader->setUniform("model", GetModelMatrix());
        } else {
            light_shader->Use();
            light_shader->setUniform("model", GetModelMatrix());
        }
        
        mesh->Draw();
    } else if (tag == 2) {
        if (mesh->getOrderNumber() == 0) {
            m_pointLightShader->setUniform("model", GetModelMatrix());
            mesh->Draw();
        }
        
    }
}
