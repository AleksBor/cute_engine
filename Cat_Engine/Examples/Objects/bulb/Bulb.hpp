//
//  Bulb.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 30/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Model.hpp"

class Bulb: public Model<Composite> {
private:
    shader_program_ptr shader;
    shader_program_ptr light_shader;
    
    shader_program_ptr m_pointLightShader;
public:
    Bulb() = delete;
    Bulb(const char* path, Camera* cam, shader_program_ptr pointLightShader);
    
    void ProcessMesh(int tag, mesh_ptr mesh) override;
};
