//
//  TextureFrame.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Quad.hpp"
#include "types.h"

class PostProcessingFrame;

typedef std::shared_ptr<PostProcessingFrame> post_processing_frame_ptr;

class PostProcessingFrame : public Quad<BasicItem> {
private:
    shader_program_ptr shader;
    
    glm::vec3* m_lightPositions;
    
    glm::mat4 model = glm::mat4(1);
public:
    //PostProcessingFrame() = delete;
    PostProcessingFrame(glm::vec3* lightPositions = nullptr);
    
    void Process(int tag) override;
};
