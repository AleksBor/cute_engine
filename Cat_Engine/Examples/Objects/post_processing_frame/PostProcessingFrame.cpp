//
//  TextureFrame.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "PostProcessingFrame.hpp"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

PostProcessingFrame::PostProcessingFrame(glm::vec3* lightPositions) : Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS) }, s_QuadVerticalCoords), m_lightPositions(lightPositions)
{
    //shader = Shader::CreateShaderFromPath("TextureFrame/texture_frame.vert", "TextureFrame/texture_frame.frag");
    shader = ShaderProgram::CreateShaderFromPath("GBuffer/g_buffer.vert", "GBuffer/g_buffer.frag");
    
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.9999999f));
    model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
    model = glm::rotate(model, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
}

void PostProcessingFrame::Process(int tag)
{
    if (tag == 0) {
        shader->Use();
        
        if (m_lightPositions)
        {
            int count = 0;
            for (int i = 0; i < 10; i++) {
                for (int c = 0; c < 10; c++) {
                    std::string lighKey = "lights[";
                    shader->setUniform((lighKey + std::to_string(count) + "].Position").c_str(), m_lightPositions[count]);
                    shader->setUniform((lighKey + std::to_string(count) + "].Color").c_str(), glm::vec3(1.0, 0.98, 0.1));
                    count++;
                }
            }
        }
        
        bindTextures(shader);
        shader->setUniform("model", model);
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        Draw();
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
    }
}
