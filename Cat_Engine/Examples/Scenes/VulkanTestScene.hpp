//
//  VulkanTestScene.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#pragma once

#include "Scene.hpp"
#include "types.h"

class VulkanTestScene: public Scene {
public:
    VulkanTestScene();
private:
    void Update();
};
