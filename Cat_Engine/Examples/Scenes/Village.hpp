//
//  Village.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "Scene.hpp"
#include "Cube.hpp"
#include "Quad.hpp"
#include "FPSCamera.hpp"
#include "OrthoCamera.hpp"

#include "types.h"
#include "Model.hpp"

#include "Frame.hpp"

extern const int screenWidth;
extern const int screenHeight;

class Village: public Scene
{
public:
    Village();
    ~Village();
    
    void Update() override;
private:
    OrthoCamera* orthoCamera;
    
    GLfloat yaw = 90.0f;
    GLfloat pitch = 0.0f;
    
    GLfloat lastFrame = 0.f;
    
    composite_item_ptr glass;
    object_ptr sun;
    object_ptr mosaic;
    custom_frame_ptr frame;
    object_ptr cube;
    
    glm::vec3 sunPosition;
    
    shader_program_ptr modelShader;
    shader_program_ptr dirLightShader;
    shader_program_ptr pointLightShader;
    shader_program_ptr pingPongShader;
    
    tex_frame_buffer_ptr hdrFramebuffer;
    ping_pong_frame_buffer_ptr pingFrameBuf;
    depth_frame_buffer_ptr depth_framebuffer;
    cube_map_depth_frame_buffer_ptr cubemap_framebuffer;
    
    std::vector<glm::mat4> shadowTransforms;

    double lastX = 0.f, lastY = 0.f;

    void drawBuffer(int type);
    void do_movement();

    void checkCameraPosition();
};
