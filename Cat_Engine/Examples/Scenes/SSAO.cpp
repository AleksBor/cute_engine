//
//  SSAO.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "SSAO.hpp"

#include "SSAONanosuit.hpp"
#include "Sky.hpp"
#include "SSAOFrame.hpp"
#include "SSAODisplayFrame.hpp"
#include "DebugPointLight.hpp"
#include "Floor.hpp"

#include "SceneWindow.hpp"

#include "GeometryFrameBuffer.hpp"

#include <iostream>

#include "Defines.h"

#include "SSAOFrameBuffer.hpp"

extern bool grabCursor;

SSAO::SSAO() {
    auto framebuffer = SceneWindow::Instance->getDefaultFrameBuffer();
    auto cam = framebuffer->getCamera();
    Camera* camera = cam.get();
    
    gFrameBuff = geometry_frame_buffer_ptr(new GeometryFrameBuffer(screenWidth, screenHeight, 1));
    
    ssaoFrameBuffer = ssao_frame_buffer_ptr(new SSAOFrameBuffer(screenWidth, screenHeight, 2));
    
    ssaoBlurFrameBuffer = ssao_frame_buffer_ptr(new SSAOFrameBuffer(screenWidth, screenHeight, 3));
    
    std::string nanosuitPath = ROOT_PATH + "/Examples/res/objects/nanosuit/nanosuit.obj";
    
    render_item_ptr nanosuit = render_item_ptr(new SSAONanosuit(nanosuitPath.c_str(), camera, positions));
    
    gFrameBuff->AddRenderItem(nanosuit);
    
    render_item_ptr floor = render_item_ptr(new Floor(camera));
    gFrameBuff->AddRenderItem(floor);
    
    auto ssao_frame = ssao_frame_ptr(new SSAOFrame(cam, gFrameBuff));
    
    ssaoFrameBuffer->AddRenderItem(ssao_frame);
    
    render_item_ptr blurFrame = render_item_ptr(new SSAOBlurFrame());
    blurFrame->AddTexture(ssaoFrameBuffer->getBuffer());
    ssaoBlurFrameBuffer->AddRenderItem(blurFrame);
    
    render_item_ptr displayFrame = render_item_ptr(new SSAODisplayFrame());
    displayFrame->AddTexture(ssaoBlurFrameBuffer->getBuffer());
    displayFrame->AddTexture(gFrameBuff->getGColorSpecTex());
    framebuffer->AddRenderItem(displayFrame);
    
    framebuffer->setClearFlags(GL_COLOR_BUFFER_BIT);
    ssaoFrameBuffer->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ssaoBlurFrameBuffer->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gFrameBuff->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

SSAO::~SSAO() {
    SceneWindow::Instance->getDefaultFrameBuffer()->RemoveAllRenderItem();
}

void SSAO::Update()
{
    SceneWindow::Instance->getDefaultFrameBuffer()->getCamera()->UpdateUBO();
    do_movement();
    
    gFrameBuff->bind();
    gFrameBuff->Display();
    gFrameBuff->unbind();
    
    ssaoFrameBuffer->bind();
    ssaoFrameBuffer->Display();
    ssaoFrameBuffer->unbind();
    
    ssaoBlurFrameBuffer->bind();
    ssaoBlurFrameBuffer->Display();
    ssaoBlurFrameBuffer->unbind();
    
    SceneWindow::Instance->getDefaultFrameBuffer()->bind();

    SceneWindow::Instance->getDefaultFrameBuffer()->Display();
}

void SSAO::do_movement()
{
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();
    
    const float forwardSpeed = 1.2;
    const float backwardSpeed = 1.2;
    const float leftSpeed = 1.2;
    const float rightSpeed = 1.2;
    
    double xpos, ypos = 0.f;
    glfwGetCursorPos(SceneWindow::Instance->getWindow(), &xpos, &ypos);

    GLfloat sensitivity = 0.05f;
    float xoffset = (xpos - lastX) * sensitivity;
    float yoffset = (lastY - ypos) * sensitivity;
    
    lastX = xpos;
    lastY = ypos;

    yaw -= xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
    {
        pitch = 89.0f;
    }
    else if (pitch < -89.0f) {
        pitch = -89.0f;
    }

    glm::vec3 front;
    front.x = cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * (-cos(glm::radians(pitch)));
    camera->front = glm::normalize(front);
    
    GLfloat currentFrame = glfwGetTime();
    float deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    static GLfloat cameraSpeed = 1.f;
    
    static bool g_Pessed = false;

    if (key_pressed(GLFW_KEY_W)) {
        cameraSpeed = forwardSpeed * deltaTime;
        camera->pos += cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_S)) {
        cameraSpeed = backwardSpeed * deltaTime;
        camera->pos -= cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_A)) {
        cameraSpeed = leftSpeed * deltaTime;
        camera->pos -= cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_D)) {
        cameraSpeed = rightSpeed * deltaTime;
        camera->pos += cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_G)) {
        if (!g_Pessed)
        {
            grabCursor = !grabCursor;
            if (grabCursor) {
                SceneWindow::GrabCursor();
            } else {
                SceneWindow::ReleaseCursor();
            }
            g_Pessed = true;
        }
    } else {
        g_Pessed = false;
    }
}

void SSAO::checkCameraPosition() {
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();
    if (camera->pos.y < -0.3) {
        camera->pos.y = -0.3;
    }
}
