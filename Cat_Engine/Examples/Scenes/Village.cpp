//
//  Game.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "Village.hpp"
#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

#include "Defines.h"

#include "Sphere.hpp"
#include "Cottage.hpp"
#include "Ground.hpp"
#include "Bulb.hpp"
#include "Glass.hpp"
#include "Sky.hpp"
#include "Sun.hpp"
#include "PointLightCube.hpp"
#include "House.hpp"
#include "Wall.hpp"
#include "Roof.hpp"
#include "WallDisp.hpp"
#include "Frame.hpp"

#include "SceneWindow.hpp"

#include "open_gl.h"
#include "glm.h"
#include "PointLight.hpp"
#include "DirLight.hpp"
#include "TextureFrameBuffer.hpp"

#include "TerrainModel.hpp"

#include "ShaderProgram.h"

#include "DepthFrameBuffer.hpp"

#include "PingPongFrameBuffer.hpp"
#include "CubeMapDepthFrameBuffer.hpp"

extern bool grabCursor;

glm::vec3 sunPosFromCam = glm::vec3(-2.1f, 2.4f, -1.0f);

PointLight lamp = {
    .position = glm::vec3(0.5f, 0.035f, 0.0f),
    .near_plane = 0.02,
    .far_plane = 20.f,
    .ambient = glm::vec3(0.1, 0.081, 0.0651),
    .diffuse = glm::vec3(0.8f, 0.648, 0.521),
    .specular = glm::vec3(0.3, 0.3, 0.3),
    .constant = 1.0f,
    .linear = 1.0f,
    .quadratic = 0.8f
};

bool turnOnNormalMapping = false;
bool turnOnTangent = false;

Village::Village()
{
    auto framebuffer = SceneWindow::Instance->getDefaultFrameBuffer();
    auto camera = framebuffer->getCamera().get();
    
    hdrFramebuffer = tex_frame_buffer_ptr(new TextureFrameBuffer(screenWidth, screenHeight, 0));
    pingFrameBuf = ping_pong_frame_buffer_ptr(new PingPongFrameBuffer(screenWidth, screenHeight, 3));
    
    orthoCamera = new OrthoCamera(1);
    
    sunPosition = sunPosFromCam - camera->pos;
    
    DirLight sunLight = {
        .position = sunPosition,
        .ambient = glm::vec3(0.3, 0.3, 0.3),
        .diffuse = glm::vec3(0.6, 0.5364, 0.4847),
        .specular = glm::vec3(0.2, 0.2, 0.2)
    };

    glfwGetCursorPos(SceneWindow::Instance->getWindow(), &lastX, &lastY);
    
    depth_framebuffer = depth_frame_buffer_ptr(new DepthFrameBuffer(1024, 1024, 1));//FrameBuf::CreateDepthTextureFrameBuffer(1024, 1024);
    depth_framebuffer->setClearFlags(GL_DEPTH_BUFFER_BIT);
    
    cubemap_framebuffer = cube_map_depth_frame_buffer_ptr(new CubeMapDepthFrameBuffer(1024, 1024, 2));//FrameBuf::CreateCubeMapDepthTextureFrameBuffer(1024, 1024);
    cubemap_framebuffer->setClearFlags(GL_DEPTH_BUFFER_BIT);
    
    dirLightShader = ShaderProgram::CreateShaderFromPath("DirectLight/direct_light.vert", "DirectLight/direct_light.frag");
    pointLightShader = ShaderProgram::CreateShaderFromPath("PointLight/point_light.vert", "PointLight/point_light.frag", "PointLight/point_light.geom");
    
    //object_ptr dbLamp = lamp.debugObject(camera);
    //framebuffer->AddRenderItem(dbLamp);
    
    render_item_ptr ground = render_item_ptr(new Ground(camera, dirLightShader, pointLightShader, sunLight, orthoCamera, lamp, {depth_framebuffer->getTexture(), cubemap_framebuffer->getTexture()}));
    framebuffer->AddRenderItem(ground);
    hdrFramebuffer->AddRenderItem(ground);
    depth_framebuffer->AddRenderItem(ground);
    cubemap_framebuffer->AddRenderItem(ground);
    
    render_item_ptr terrain = render_item_ptr(new TerrainModel("Terrain.raw", camera, dirLightShader, pointLightShader, sunLight, orthoCamera, lamp, {depth_framebuffer->getTexture(), cubemap_framebuffer->getTexture()}));
    framebuffer->AddRenderItem(terrain);
    hdrFramebuffer->AddRenderItem(terrain);
    depth_framebuffer->AddRenderItem(terrain);
    cubemap_framebuffer->AddRenderItem(terrain);
    
    render_item_ptr wall = render_item_ptr(new Wall(camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, &turnOnTangent, glm::vec2 { 1.05, 0.3 }, {depth_framebuffer->getTexture()}));
    framebuffer->AddRenderItem(wall);
    depth_framebuffer->AddRenderItem(wall);
    
    auto wallMat1 = glm::translate(glm::mat4(1), glm::vec3(4.1f, -0.35f, 0.0f));
    wallMat1 = glm::rotate(wallMat1, (GLfloat)(M_PI_2), glm::vec3(0.0f, 1.0f, 0.0f));
    wallMat1 = glm::scale(wallMat1, glm::vec3(1.05, 0.3, 0.3));
    render_item_ptr wallDisp1 = render_item_ptr(new WallDisp(camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, &turnOnTangent, glm::vec2 { 1.05, 0.3 }, {depth_framebuffer->getTexture()}, wallMat1, false));
    
    framebuffer->AddRenderItem(wallDisp1);
    hdrFramebuffer->AddRenderItem(wallDisp1);
    depth_framebuffer->AddRenderItem(wallDisp1);
    
    auto wallMat2 = glm::translate(glm::mat4(1), glm::vec3(4.1f, -0.35f, 4.0f));
    wallMat2 = glm::rotate(wallMat2, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
    wallMat2 = glm::scale(wallMat2, glm::vec3(1.05, 0.3, 0.3));
    render_item_ptr wallDisp2 = render_item_ptr(new WallDisp(camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, &turnOnTangent, glm::vec2 { 1.05, 0.3 }, {depth_framebuffer->getTexture()}, wallMat2, false));

    framebuffer->AddRenderItem(wallDisp2);
    hdrFramebuffer->AddRenderItem(wallDisp2);
    depth_framebuffer->AddRenderItem(wallDisp2);

    auto wallMat3 = glm::translate(glm::mat4(1), glm::vec3(4.7f, -0.35f, 3.9f));
    wallMat3 = glm::rotate(wallMat3, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
    wallMat3 = glm::scale(wallMat3, glm::vec3(1.05, 0.3, 0.3));
    render_item_ptr wallDisp3 = render_item_ptr(new WallDisp(camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, &turnOnTangent, glm::vec2{ 1.05, 0.3 }, { depth_framebuffer->getTexture() }, wallMat3, true));
    
    framebuffer->AddRenderItem(wallDisp3);
    depth_framebuffer->AddRenderItem(wallDisp3);
    
    //Cottage
    
    render_item_ptr house = render_item_ptr(new House((ROOT_PATH + "/Examples/res/objects/Building/OldHouse/Old House 2 3D Models.obj").c_str(), camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, {depth_framebuffer->getTexture()}));
    framebuffer->AddRenderItem(house);
    depth_framebuffer->AddRenderItem(house);
    hdrFramebuffer->AddRenderItem(house);
    
    sun = object_ptr(new Sun(camera, sunLight.position));
    framebuffer->AddRenderItem(sun);
    hdrFramebuffer->AddRenderItem(sun);
    /*
    cube = object_ptr(new PointLightCube(camera));
    cube->AddTexture(cubemap_framebuffer->getTexture());
    renderScene->AddObject(cube);*/
    
    auto cubeMap = object_ptr(new Sky(camera));
    framebuffer->AddRenderItem(cubeMap);
    hdrFramebuffer->AddRenderItem(cubeMap);
    
    composite_item_ptr cottage = composite_item_ptr(new Cottage((ROOT_PATH + "/Examples/res/objects/Warehouse/Warehouse.obj").c_str(), camera, dirLightShader, pointLightShader, sunLight, orthoCamera, lamp, {depth_framebuffer->getTexture(), cubemap_framebuffer->getTexture()}));
    
    composite_item_ptr roof = composite_item_ptr(new Roof(camera, dirLightShader, sunLight, orthoCamera, &turnOnNormalMapping, {depth_framebuffer->getTexture()}));
    
    composite_item_ptr bulb = composite_item_ptr(new Bulb((ROOT_PATH + "/Examples/res/objects/Lamp/lightbulbfinal.obj").c_str(), camera, pointLightShader));
    
    glass = composite_item_ptr(new Glass(camera));
    
    cottage->AddItem(roof);
    cottage->AddItem(bulb);
    cottage->AddItem(glass);
    
    framebuffer->AddRenderItem(cottage);
    depth_framebuffer->AddRenderItem(cottage);
    cubemap_framebuffer->AddRenderItem(cottage);
    hdrFramebuffer->AddRenderItem(cottage);
    
    frame = custom_frame_ptr(new Frame());
    frame->AddTexture(hdrFramebuffer->getTexture(0));
    frame->AddTexture(pingFrameBuf->getBuffer(1));
    framebuffer->AddRenderItem(frame);
    pingFrameBuf->AddRenderItem(frame);
    
    orthoCamera->pos = sunPosition;
    
    float aspect = 1.0;
    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, lamp.near_plane, lamp.far_plane);
    
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lamp.position, lamp.position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glEnable(GL_CULL_FACE);
    
    glEnable(GL_MULTISAMPLE);

    framebuffer->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    hdrFramebuffer->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    pingFrameBuf->setClearFlags(GL_COLOR_BUFFER_BIT);
}

Village::~Village() {
    delete orthoCamera;
    SceneWindow::Instance->getDefaultFrameBuffer()->RemoveAllRenderItem();
    glDisable(GL_BLEND);
}

void Village::Update()
{
    auto mainCamera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera();
    mainCamera->UpdateUBO();
    
    orthoCamera->pos = sunPosition + mainCamera->pos;
    orthoCamera->front = mainCamera->pos;
    
    do_movement();
    
    glStencilMask(~0);

    SceneWindow::Instance->getDefaultFrameBuffer()->Display();
    
    depth_framebuffer->bind();
    dirLightShader->Use();
    dirLightShader->setUniform("lightSpaceMatrix", orthoCamera->projection() * orthoCamera->view());
    glViewport(0, 0, 1024, 1024);
    depth_framebuffer->Display();
    depth_framebuffer->unbind();
    
    cubemap_framebuffer->bind();
    pointLightShader->Use();
    for (int i = 0; i < 6; ++i)
    {
        pointLightShader->setUniform(("shadowMatrices[" + std::to_string(i) + "]").c_str(), shadowTransforms[i]);
    }
    pointLightShader->setUniform("far_plane", lamp.far_plane);
    pointLightShader->setUniform("lightPos", lamp.position);
    
    glViewport(0, 0, 1024, 1024);
    cubemap_framebuffer->Display();
    cubemap_framebuffer->unbind();
    
    glViewport(0, 0, screenWidth, screenHeight);
    hdrFramebuffer->bind();
    hdrFramebuffer->Display();
    hdrFramebuffer->unbind();
    
    glViewport(0, 0, screenWidth, screenHeight);
    bool horizontal = true, first_iteration = true;
    unsigned int amount = 10;
    for (unsigned int i = 0; i < amount; i++) {
        if (first_iteration){
            pingFrameBuf->bind(0);
            pingFrameBuf->bind(1);
        }
        pingFrameBuf->bind(horizontal);
        frame->pingpongTexture = first_iteration ? hdrFramebuffer->getTexture(1) : pingFrameBuf->getBuffer(!horizontal);
        frame->horizontal = horizontal;
        //pingFrameBuf->clear();
        pingFrameBuf->Display();
        horizontal = !horizontal;
        if (first_iteration) first_iteration = false;
    }
    pingFrameBuf->unbind();
}

void Village::do_movement()
{
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();

    orthoCamera->pos = sunPosition + camera->pos;
    orthoCamera->front = camera->pos;
    
    const float forwardSpeed = 1.2;
    const float backwardSpeed = 1.2;
    const float leftSpeed = 1.2;
    const float rightSpeed = 1.2;
    
    double xpos, ypos = 0.f;
    glfwGetCursorPos(SceneWindow::Instance->getWindow(), &xpos, &ypos);

    GLfloat sensitivity = 0.05f;
    float xoffset = (xpos - lastX) * sensitivity;
    float yoffset = (lastY - ypos) * sensitivity;

    lastX = xpos;
    lastY = ypos;

    yaw -= xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
    {
        pitch = 89.0f;
    }
    else if (pitch < -89.0f) {
        pitch = -89.0f;
    }

    glm::vec3 front;
    front.x = cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * (-cos(glm::radians(pitch)));
    camera->front = glm::normalize(front);
    
    GLfloat currentFrame = glfwGetTime();
    float deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    static GLfloat cameraSpeed = 1.f;
    
    static bool g_Pessed = false;

    if (key_pressed(GLFW_KEY_W)) {
        cameraSpeed = forwardSpeed * deltaTime;
        camera->pos += cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_S)) {
        cameraSpeed = backwardSpeed * deltaTime;
        camera->pos -= cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_A)) {
        cameraSpeed = leftSpeed * deltaTime;
        camera->pos -= cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_D)) {
        cameraSpeed = rightSpeed * deltaTime;
        camera->pos += cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_N)) {
        turnOnNormalMapping = !turnOnNormalMapping;
    }
    if (key_pressed(GLFW_KEY_T)) {
        turnOnTangent = !turnOnTangent;
    }
    if (key_pressed(GLFW_KEY_G)) {
        if (!g_Pessed)
        {
            grabCursor = !grabCursor;
            if (grabCursor) {
                SceneWindow::GrabCursor();
            } else {
                SceneWindow::ReleaseCursor();
            }
            g_Pessed = true;
        }
    } else {
        g_Pessed = false;
    }
}

void Village::checkCameraPosition() {
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();
    if (camera->pos.y < -0.3) {
        camera->pos.y = -0.3;
    }
}
