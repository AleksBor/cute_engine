//
//  SSAO.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Scene.hpp"
#include "TextureFrame.hpp"
#include "types.h"
#include "SSAOBlurFrame.hpp"

extern const int screenWidth;
extern const int screenHeight;

class SSAO: public Scene {
public:
    SSAO();
    ~SSAO();
    
    void Update() override;
private:
    GLfloat yaw = 90.0f;
    GLfloat pitch = 0.0f;
    
    double lastX = 0.f, lastY = 0.f;
    
    GLfloat lastFrame = 0.f;
    
    geometry_frame_buffer_ptr gFrameBuff;
    ssao_frame_buffer_ptr ssaoFrameBuffer;
    ssao_frame_buffer_ptr ssaoBlurFrameBuffer;
    
    void do_movement();
    void checkCameraPosition();
    
    glm::vec3 positions[100];
};
