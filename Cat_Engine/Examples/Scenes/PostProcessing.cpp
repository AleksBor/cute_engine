//
//  PostProcessing.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 12/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "PostProcessing.hpp"
#include "Nanosuit.hpp"
#include "Sky.hpp"
#include "PostProcessingFrame.hpp"
#include "DebugPointLight.hpp"

#include "SceneWindow.hpp"

#include "GeometryFrameBuffer.hpp"

#include <iostream>

#include "Defines.h"

extern bool grabCursor;

PostProcessing::PostProcessing() {
    auto framebuffer = SceneWindow::Instance->getDefaultFrameBuffer();
    Camera* camera = framebuffer->getCamera().get();
    
    gFrameBuff = geometry_frame_buffer_ptr(new GeometryFrameBuffer(screenWidth, screenHeight, 1));
    
    std::string nanosuitPath = ROOT_PATH + "/Examples/res/objects/nanosuit/nanosuit.obj";
    
    
    
    render_item_ptr nanosuit = render_item_ptr(new Nanosuit(nanosuitPath.c_str(), camera, positions));
    //framebuffer->AddRenderItem(nanosuit);
    
    //auto cubeMap = object_ptr(new Sky(camera));
    //framebuffer->AddRenderItem(cubeMap);
    
    gFrameBuff->AddRenderItem(nanosuit);
    /*
    auto frame1 = texture_frame_ptr(new TextureFrame(glm::vec3(0.5f, 0.5f, 0.0f)));
    frame1->AddTexture(gFrameBuff->getGPositionTex());
    framebuffer->AddRenderItem(frame1);
    
    auto frame2 = texture_frame_ptr(new TextureFrame(glm::vec3(-0.5f, 0.5f, 0.0f)));
    frame2->AddTexture(gFrameBuff->getGNormalTex());
    framebuffer->AddRenderItem(frame2);
    
    auto frame3 = texture_frame_ptr(new TextureFrame(glm::vec3(-0.5f, -0.5f, 0.0f)));
    frame3->AddTexture(gFrameBuff->getGColorSpecTex());
    framebuffer->AddRenderItem(frame3);
    
    auto frame4 = texture_frame_ptr(new TextureFrame(glm::vec3(0.5f, -0.5f, 0.0f), true));
    frame4->AddTexture(gFrameBuff->getGColorSpecTex());
    framebuffer->AddRenderItem(frame4);
*/
    auto pp_frame = post_processing_frame_ptr(new PostProcessingFrame(positions));
    pp_frame->AddTexture(gFrameBuff->getGPositionTex());
    pp_frame->AddTexture(gFrameBuff->getGNormalTex());
    pp_frame->AddTexture(gFrameBuff->getGColorSpecTex());
    framebuffer->AddRenderItem(pp_frame);
    
    int count = 0;
    for (int i = 0; i < 10; i++) {
        for (int c = 0; c < 10; c++) {
            positions[count] = glm::vec3(i * 10, 0.0f, c * 10);
            auto modelMatrix = glm::scale(glm::mat4(1), glm::vec3(0.05f, 0.05f, 0.05f));
            positions[count] = glm::mat3(modelMatrix) * positions[count];
            auto pLight = object_ptr(new DebugPointLight(0.05, camera, &positions[count]));
            framebuffer->AddRenderItem(pLight);
            
            count++;
        }
    }
    
    framebuffer->setClearFlags(GL_COLOR_BUFFER_BIT);
    gFrameBuff->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

PostProcessing::~PostProcessing() {
    SceneWindow::Instance->getDefaultFrameBuffer()->RemoveAllRenderItem();
}

void PostProcessing::Update()
{
    SceneWindow::Instance->getDefaultFrameBuffer()->getCamera()->UpdateUBO();
    do_movement();
    
    gFrameBuff->bind();
    gFrameBuff->Display();
    gFrameBuff->unbind();
    
    gFrameBuff->bind(GL_READ_FRAMEBUFFER);
    SceneWindow::Instance->getDefaultFrameBuffer()->bind(GL_DRAW_FRAMEBUFFER);
    glBlitFramebuffer(
        0, 0, screenWidth, screenHeight, 0, 0, screenWidth, screenHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    SceneWindow::Instance->getDefaultFrameBuffer()->bind();

    SceneWindow::Instance->getDefaultFrameBuffer()->Display();
}

void PostProcessing::do_movement()
{
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();
    
    const float forwardSpeed = 1.2;
    const float backwardSpeed = 1.2;
    const float leftSpeed = 1.2;
    const float rightSpeed = 1.2;
    
    double xpos, ypos = 0.f;
    glfwGetCursorPos(SceneWindow::Instance->getWindow(), &xpos, &ypos);

    GLfloat sensitivity = 0.05f;
    float xoffset = (xpos - lastX) * sensitivity;
    float yoffset = (lastY - ypos) * sensitivity;
    
    lastX = xpos;
    lastY = ypos;

    yaw -= xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
    {
        pitch = 89.0f;
    }
    else if (pitch < -89.0f) {
        pitch = -89.0f;
    }

    glm::vec3 front;
    front.x = cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * (-cos(glm::radians(pitch)));
    camera->front = glm::normalize(front);
    
    GLfloat currentFrame = glfwGetTime();
    float deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    static GLfloat cameraSpeed = 1.f;
    
    static bool g_Pessed = false;

    if (key_pressed(GLFW_KEY_W)) {
        cameraSpeed = forwardSpeed * deltaTime;
        camera->pos += cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_S)) {
        cameraSpeed = backwardSpeed * deltaTime;
        camera->pos -= cameraSpeed * camera->front;
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_A)) {
        cameraSpeed = leftSpeed * deltaTime;
        camera->pos -= cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_D)) {
        cameraSpeed = rightSpeed * deltaTime;
        camera->pos += cameraSpeed * glm::normalize(glm::cross(camera->front, camera->up));
        checkCameraPosition();
    }
    if (key_pressed(GLFW_KEY_G)) {
        if (!g_Pessed)
        {
            grabCursor = !grabCursor;
            if (grabCursor) {
                SceneWindow::GrabCursor();
            } else {
                SceneWindow::ReleaseCursor();
            }
            g_Pessed = true;
        }
    } else {
        g_Pessed = false;
    }
}

void PostProcessing::checkCameraPosition() {
    auto camera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera().get();
    if (camera->pos.y < -0.3) {
        camera->pos.y = -0.3;
    }
}
