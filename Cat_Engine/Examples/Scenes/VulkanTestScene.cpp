//
//  VulkanTestScene.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 16.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#include "VulkanTestScene.hpp"
#include "VulkanTriangle.hpp"

#include "SceneWindow.hpp"
#include "FrameBuffer.hpp"

VulkanTestScene::VulkanTestScene() {
#ifdef VULKAN_API
    auto framebuffer = SceneWindow::Instance->getDefaultFrameBuffer();
    
    auto triangle = render_item_ptr(new VulkanTriangle(0));
    auto triangle1 = render_item_ptr(new VulkanTriangle(1));

    framebuffer->AddRenderItem(triangle);
    framebuffer->AddRenderItem(triangle1);

    SceneWindow::Instance->ApplyFramebufferChanges();
#endif
}

void VulkanTestScene::Update() {
    SceneWindow::Instance->getDefaultFrameBuffer()->Display();
}
