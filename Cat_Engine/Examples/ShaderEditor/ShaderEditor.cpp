#include "ShaderEditor.h"
#include "SceneWindow.hpp"
#include "FrameBuffer.hpp"

ShaderEditor::ShaderEditor() : shaderFrame(shader_frame_ptr(new ShaderFrame())) {
	auto framebuffer = SceneWindow::Instance->getDefaultFrameBuffer();

    framebuffer->AddRenderItem(shaderFrame);

    framebuffer->setClearFlags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

ShaderEditor::~ShaderEditor() {
    SceneWindow::Instance->getDefaultFrameBuffer()->RemoveAllRenderItem();
}

void setShaderString(std::string shaderStr) {
}

void ShaderEditor::Update()
{
    auto mainCamera = SceneWindow::Instance->getDefaultFrameBuffer()->getCamera();
    mainCamera->UpdateUBO();

    SceneWindow::Instance->getDefaultFrameBuffer()->Display();
}