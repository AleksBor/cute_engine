#include "ShaderFrame.h"
#include "ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <math.h>

ShaderFrame::ShaderFrame() : Quad(QuadOptions{ { 1.f, 1.f }, static_cast<int>(VERTEX_PARAMS::TEX_COORDS) }, s_QuadVerticalCoords)
{
    shader = ShaderProgram::CreateShaderFromPath("FrameShader.shader");

    model = glm::rotate(model, (GLfloat)(M_PI), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(2.f, 2.f, 1.f));
}

void ShaderFrame::Process(int tag)
{
    shader->Use();
    bindTextures(shader);
    shader->setUniform("model", model);
    glDisable(GL_CULL_FACE);
    Draw();
    glEnable(GL_CULL_FACE);
}