#pragma once

#include "Scene.hpp"
#include "Shader.hpp"
#include "ShaderFrame.h"

class ShaderEditor: public Scene {
public:
	ShaderEditor();
	~ShaderEditor();

	void Update() override;

private:
	shader_frame_ptr shaderFrame;
};

inline void runShaderCallback() {
	std::cout << "Success Run" << std::endl;
}