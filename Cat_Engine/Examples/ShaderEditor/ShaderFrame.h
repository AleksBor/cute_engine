#pragma once

#include "Quad.hpp"

class ShaderFrame;

typedef std::shared_ptr<ShaderFrame> shader_frame_ptr;

class ShaderFrame : public Quad<BasicItem> {
private:
    shader_program_ptr shader;

    glm::mat4 model = glm::mat4(1);
public:

    ShaderFrame();
    ~ShaderFrame() { std::cout << "FrameDeleted" << std::endl; }

    void Process(int tag) override;
};
