workspace iva_engine
	architecture "x86"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "iva_engine"
	location "Engine"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prg.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prg.name}")

	files
	{
		"Engine/**.h",
		"Engine/**.hpp",
		"Engine/**.cpp"
	}

	includedirs
	{
		"Engine\Debug",
		"Engine\Texture",
		"Engine\Light",
		"Engine\Scene",
		"Engine\Camera",
		"Engine\FrameBuffer"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "10.0.18362.0"

		defines
		{
			"GLEW_STATIC"
		}

	filter "configurations:Debug"
		defines "IVA_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "IVA_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "IVA_Dist"
		optimize "On"