//
//  Texture.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 25/11/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "Texture.hpp"
#include "SOIL2.h"
#include "stb_image.h"

texture_ptr Texture::CreateTexture(const char *path, const std::string &directory, std::string type, TextureOptions opts, bool gamma)
{
    std::string filename = std::string(path);
    filename = directory.empty() ? filename : (directory + '/' + filename);

    texture_ptr t = texture_ptr(new Texture());


    int width, height, nrComponents;
    unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, t->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, opts.WRAP_S);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, opts.WRAP_T);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    t->type = type;
    
    return t;
}

texture_ptr Texture::CreateFramebufferTexture(int width, int height)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    t->type = TEXTURE_DIFFUSE;
    
    return t;
}

texture_ptr Texture::CreateGeometryTexture(int width, int height)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    t->type = TEXTURE_CUSTOM;
    
    return t;
}

texture_ptr Texture::CreateHDRTexture(int width, int height)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    t->type = TEXTURE_DIFFUSE;
    
    return t;
}

texture_ptr Texture::CreatePositionTexture(int width, int height, void* data)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    t->type = TEXTURE_CUSTOM;
    
    return t;
}

texture_ptr Texture::CreateSSAOTexture(int width, int height)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);
    t->type = TEXTURE_DIFFUSE;
    
    return t;
}

texture_ptr Texture::CreateMultisampleFramebufferTexture(GLuint framebuffer, int width, int height, unsigned int samples)
{
    texture_ptr t = texture_ptr(new Texture());
    
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, t->getID());
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGB, width, height, GL_TRUE);
    //glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    t->type = TEXTURE_DIFFUSE;
    return t;
}

texture_ptr Texture::CreateСubeMapTexture(std::vector<std::string> texture_faces)
{
    texture_ptr t = texture_ptr(new Texture());

    glBindTexture(GL_TEXTURE_CUBE_MAP, t->getID());

    int width, height, nrChannels;

    for (GLuint i = 0; i < texture_faces.size(); i++)
    {
        unsigned char* data = stbi_load(texture_faces[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
                0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << texture_faces[i] << std::endl;
            stbi_image_free(data);
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    t->type = TEXTURE_CUBE_MAP;
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    return t;
}

texture_ptr Texture::CreateDepthTexture(const unsigned int width, const unsigned int height)
{
    texture_ptr t = texture_ptr(new Texture());

    glBindTexture(GL_TEXTURE_2D, t->getID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    t->type = TEXTURE_DEPTH;
    glBindTexture(GL_TEXTURE_2D, 0);
    return t;
}

texture_ptr Texture::CreateCubeMapDepthTexture(const unsigned int width, const unsigned int height)
{
    texture_ptr t = texture_ptr(new Texture());
    glBindTexture(GL_TEXTURE_CUBE_MAP, t->getID());
    for (unsigned int i = 0; i < 6; ++i) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    t->type = TEXTURE_CUBEMAP_DEPTH;

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    
    return t;
}
