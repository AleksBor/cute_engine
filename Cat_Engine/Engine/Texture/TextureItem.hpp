//
//  TextureItem.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "types.h"
#include <iostream>

struct TextureItem {
    texture_ptr texture;
    char* name = NULL;
    GLenum target;

    inline void AddName(const std::string &strName)
    {
        size_t size = strName.length() + 1;
        name = new char[size];
#ifdef WINDOWS
        strcpy_s(name, size, strName.c_str());
#else
        strcpy(name, strName.c_str());
#endif // WINDOWS
    }

    ~TextureItem() { if (name) delete[] name; }
};
