//
//  Texture.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 25/11/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "open_gl.h"
#include <iostream>
#include <assimp/postprocess.h>

#include <vector>

#include "types.h"

#define TEXTURE_DIFFUSE "texture_diffuse"
#define TEXTURE_SPECULAR "texture_specular"
#define TEXTURE_CUBE_MAP "texture_cubemap"
#define TEXTURE_DEPTH "texture_depth"
#define TEXTURE_CUBEMAP_DEPTH "texture_cubemap_depth_"
#define TEXTURE_NORMAL_MAP "texture_normal_map_"
#define TEXTURE_DISPLACEMENT_MAP "texture_displacement_map_"
#define TEXTURE_CUSTOM "texture_custom"

struct TextureOptions
{
    GLuint WRAP_S = GL_REPEAT;
    GLuint WRAP_T = GL_REPEAT;
};

class Texture {
private:
    GLuint id;
    std::string type = TEXTURE_DIFFUSE;
    
    Texture() { glGenTextures(1, &id); }
public:
    aiString path;
    
    std::string name;
    
    static texture_ptr CreateTexture(const char* path, const std::string& directory, std::string type, TextureOptions opts = TextureOptions(), bool gamma = false);
    static texture_ptr CreateFramebufferTexture(int width, int height);
    static texture_ptr CreateGeometryTexture(int width, int height);
    static texture_ptr CreateHDRTexture(int width, int height);
    static texture_ptr CreatePositionTexture(int width, int height, void* data = nullptr);
    static texture_ptr CreateSSAOTexture(int width, int height);
    static texture_ptr CreateMultisampleFramebufferTexture(GLuint framebuffer, int width, int height, unsigned int samples);
    static texture_ptr CreateСubeMapTexture(std::vector<std::string> texture_faces);
    static texture_ptr CreateDepthTexture(const unsigned int width, const unsigned int height);
    static texture_ptr CreateCubeMapDepthTexture(const unsigned int width, const unsigned int height);
    
    
    ~Texture() { glDeleteTextures(1, &id); }
    
    inline GLuint getID() { return id; }
    inline std::string getType() { return type; }
};
