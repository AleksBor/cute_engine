//
//  SceneWindow.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 09/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "open_gl.h"

#include "types.h"

#include <vulkan/vulkan.h>

#include <vector>

struct QueueFamilyIndices;
struct SwapChainSupportDetails;

class SceneWindow
{
private:
    int screenWidth;
    int screenHeight;
    
    SceneWindow(int scrWidth, int scrHeight);
    ~SceneWindow();
    
    GLFWwindow* window;
    
    frame_buffer_ptr defaultFrameBuffer;
    
    bool framebufferResized = false;

#ifdef VULKAN_API
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkDevice device;
    VkQueue graphicsQueue;
    VkSurfaceKHR surface;
    VkQueue presentQueue;
    VkSwapchainKHR swapChain;
    std::vector<VkImage> swapChainImages;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    std::vector<VkImageView> swapChainImageViews;
    VkRenderPass renderPass;
    std::vector<VkFramebuffer> swapChainFramebuffers;
    VkCommandPool commandPool;
    std::vector<VkCommandBuffer> commandBuffers;
    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphores;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;
    size_t currentFrame = 0;

    void createVKInstance();
    void setupDebugMessenger();
    void initVulkan();
    void DestroyDebugUtilsMessengerExt(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);
    void pickPhysicalDevice();
    std::vector<VkDeviceQueueCreateInfo> pickQueues();
    void createLogicalDevice();
    void createSurface();
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
    int rateDeviceSuitability(VkPhysicalDevice device);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
    void createSwapChain();
    void createImageViews();
    void createRenderPass();
    void createFramebuffers();
    void createCommandPool();
    void createCommandBuffers();
    void createSyncObjects();
    void recreateSwapChain();
    void cleanupSwapChain();
#endif
    
    static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
public:
    static SceneWindow* Instance;
    
    static void Init(int scrWidth, int scrHeight);
    static void Reset();
    
    SceneWindow() = delete;
    SceneWindow(const SceneWindow&) = delete;
    SceneWindow& operator=(const SceneWindow&) = delete;

    static void GrabCursor() { glfwSetInputMode(SceneWindow::Instance->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); }
    static void ReleaseCursor() { glfwSetInputMode(SceneWindow::Instance->window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); }
    
    static float ScreenRatio();
    
    inline GLFWwindow* getWindow() { return window; }
    
    inline frame_buffer_ptr getDefaultFrameBuffer() { return defaultFrameBuffer; }
    
#ifdef VULKAN_API
    VkDevice getDevice() { return device; }
    VkPhysicalDevice getPhysicalDevice() { return physicalDevice; }
    VkExtent2D getSwapchainExtent() { return swapChainExtent; }
    VkRenderPass getRenderPass() { return renderPass; }
    VkCommandPool getCommandPool() { return commandPool; }
    VkQueue getGraphicsQueue() { return graphicsQueue; }
    
    void ApplyFramebufferChanges();
    void DrawFrame();
#endif
};

inline float key_pressed(unsigned int key) {
    extern bool keys[1024];
    return keys[key];
}
