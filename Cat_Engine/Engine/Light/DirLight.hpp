//
//  DirLight.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "glm.h"

struct DirLight {
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
    
    glm::vec3 ambient = glm::vec3(0.1f, 0.1f, 0.1f);
    glm::vec3 diffuse = glm::vec3(0.5f, 0.5f, 0.5f);
    glm::vec3 specular = glm::vec3(0.2f, 0.2f, 0.2f);
};
