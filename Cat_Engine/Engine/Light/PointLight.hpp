//
//  PointLight.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "glm.h"

#include "DebugPointLight.hpp"

struct PointLight
{
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
    
    float near_plane = 0.02f;
    float far_plane = 20.f;
    
    glm::vec3 ambient = glm::vec3(0.1f, 0.1f, 0.1f);
    glm::vec3 diffuse = glm::vec3(0.5f, 0.5f, 0.5f);
    glm::vec3 specular = glm::vec3(0.2f, 0.2f, 0.2f);
    
    float constant = 1.0f;
    float linear = 0.09f;
    float quadratic = 0.032f;
    
    inline object_ptr debugObject(Camera* camera) { return object_ptr(new DebugPointLight(near_plane, camera, &position)); }
};
