//
//  SSAOFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"

class SSAOBlurFrameBuffer : public FrameBuffer {
public:
    SSAOBlurFrameBuffer() = delete;
    SSAOBlurFrameBuffer(int width, int height, int tag);
};
