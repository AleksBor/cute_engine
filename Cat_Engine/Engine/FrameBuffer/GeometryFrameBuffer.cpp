//
//  GeometryFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 10/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "GeometryFrameBuffer.hpp"
#include "Texture.hpp"

GeometryFrameBuffer::GeometryFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    glGenFramebuffers(1, &id);
    
    bind();
    //position buffer
    gPosition = Texture::CreatePositionTexture(width, height);
    glBindTexture(GL_TEXTURE_2D, gPosition->getID());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition->getID(), 0);
    gPosition->name = "gPosition";
    
    gNormal = Texture::CreatePositionTexture(width, height);
    glBindTexture(GL_TEXTURE_2D, gNormal->getID());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal->getID(), 0);
    gNormal->name = "gNormal";
    
    gColorSpec = Texture::CreateGeometryTexture(width, height);
    glBindTexture(GL_TEXTURE_2D, gColorSpec->getID());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gColorSpec->getID(), 0);
    gColorSpec->name = "gAlbedoSpec";
    
    unsigned int rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
    
    unsigned int attachments[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachments);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "ERROR::FRAMEBUFFER::Framebuffer is not complete!" << std::endl;
    }
    
    unbind();
}
