//
//  GeometryFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 10/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"
#include "types.h"

class GeometryFrameBuffer: public FrameBuffer
{
private:
    texture_ptr gPosition, gNormal, gColorSpec;
public:
    GeometryFrameBuffer() = delete;
    GeometryFrameBuffer(int width, int height, int tag);
    ~GeometryFrameBuffer() { glDeleteFramebuffers(1, &id); }
    
    inline texture_ptr getGPositionTex() { return gPosition; }
    inline texture_ptr getGNormalTex() { return gNormal; }
    inline texture_ptr getGColorSpecTex() { return gColorSpec; }
};
