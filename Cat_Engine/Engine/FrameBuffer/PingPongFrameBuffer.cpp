//
//  PingPongFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 04/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "PingPongFrameBuffer.hpp"
#include "Texture.hpp"

PingPongFrameBuffer::PingPongFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    for (unsigned int i = 0; i < 2; i++) {
        glGenFramebuffers(1, &fbos[i]);
        glBindFramebuffer(GL_FRAMEBUFFER, fbos[i]);
        pingpongFBOs.push_back(fbos[i]);
        auto texture = Texture::CreateHDRTexture(width, height);
        pingpongBuffers.push_back(texture);
        
        glFramebufferTexture2D(
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingpongBuffers[i]->getID(), 0
        );
        
        unbind();
    }
}
