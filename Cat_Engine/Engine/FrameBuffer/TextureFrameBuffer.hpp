//
//  TextureFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"
#include "types.h"

class TextureFrameBuffer: public FrameBuffer {
private:
    std::vector<texture_ptr> textures;
public:
    TextureFrameBuffer() = delete;
    TextureFrameBuffer(int width, int height, int tag);
    ~TextureFrameBuffer() { glDeleteFramebuffers(1, &id); }
    
    inline texture_ptr getTexture(unsigned int index) { return textures[index]; }
};
