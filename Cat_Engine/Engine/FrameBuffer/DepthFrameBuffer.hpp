//
//  DepthFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"
#include "types.h"

class DepthFrameBuffer : public FrameBuffer {
private:
    texture_ptr texture;
public:
    DepthFrameBuffer() = delete;
    DepthFrameBuffer(int width, int height, int tag);
    ~DepthFrameBuffer() { glDeleteFramebuffers(1, &id); }
    
    inline texture_ptr getTexture() { return texture; }
};
