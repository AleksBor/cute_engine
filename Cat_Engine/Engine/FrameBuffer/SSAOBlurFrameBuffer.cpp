//
//  SSAOFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "SSAOBlurFrameBuffer.hpp"

#include "Texture.hpp"

SSAOBlurFrameBuffer::SSAOBlurFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    glGenFramebuffers(1, &id);
    
    bind();
    
    texture_ptr ssaoColorBufferBlur = Texture::CreateSSAOTexture(width, height);
    
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur->getID(), 0);
}
