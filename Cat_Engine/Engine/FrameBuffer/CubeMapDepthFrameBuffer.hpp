//
//  CubeMapDepthFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"
#include "types.h"

class CubeMapDepthFrameBuffer : public FrameBuffer {
private:
    texture_ptr texture;
public:
    CubeMapDepthFrameBuffer() = delete;
    CubeMapDepthFrameBuffer(int width, int height, int tag);
    ~CubeMapDepthFrameBuffer() { glDeleteFramebuffers(1, &id); }
    
    inline texture_ptr getTexture() { return texture; }
};
