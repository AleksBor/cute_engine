//
//  SSAOFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28.03.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"

class SSAOFrameBuffer: public FrameBuffer {
private:
    texture_ptr ssaoColorBuffer;
public:
    SSAOFrameBuffer() = delete;
    SSAOFrameBuffer(int width, int height, int tag);
    
    inline texture_ptr getBuffer() { return ssaoColorBuffer; }
};
