//
//  CubeMapDepthFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "CubeMapDepthFrameBuffer.hpp"
#include "Texture.hpp"

CubeMapDepthFrameBuffer::CubeMapDepthFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    glGenFramebuffers(1, &id);

    texture = Texture::CreateCubeMapDepthTexture(width, height);

    bind();
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture->getID(), 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    unbind();
}
