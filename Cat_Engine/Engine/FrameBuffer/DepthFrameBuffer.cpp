//
//  TextureFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "DepthFrameBuffer.hpp"
#include "Texture.hpp"

DepthFrameBuffer::DepthFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    glGenFramebuffers(1, &id);

	texture = Texture::CreateDepthTexture(width, height);

	bind();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture->getID(), 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	unbind();
}
