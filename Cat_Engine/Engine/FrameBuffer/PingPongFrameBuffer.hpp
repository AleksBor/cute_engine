//
//  PingPongFrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 04/03/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "FrameBuffer.hpp"
#include "types.h"

class PingPongFrameBuffer: public FrameBuffer
{
private:
    std::vector<unsigned int> pingpongFBOs;
    std::vector<texture_ptr> pingpongBuffers;
    
    unsigned int fbos[2];
public:
    PingPongFrameBuffer() = delete;
    PingPongFrameBuffer(int width, int height, int tag);
    ~PingPongFrameBuffer() { glDeleteFramebuffers(2, fbos); }
    
    inline unsigned int getFBO(unsigned int index) { return pingpongFBOs[index]; }
    inline texture_ptr getBuffer(unsigned int index) { return pingpongBuffers[index]; }
    
    inline void bind(unsigned int index) { glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBOs[index]); }
};
