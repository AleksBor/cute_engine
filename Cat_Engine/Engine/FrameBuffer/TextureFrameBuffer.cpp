//
//  TextureFrameBuffer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 28/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "TextureFrameBuffer.hpp"
#include "Texture.hpp"

TextureFrameBuffer::TextureFrameBuffer(int width, int height, int tag) : FrameBuffer(tag)
{
    glGenFramebuffers(1, &id);
    
    bind();
    for (unsigned int i = 0; i < 2; i++)
    {
        auto texture = Texture::CreateHDRTexture(width, height);
        glBindTexture(GL_TEXTURE_2D, texture->getID());
        textures.push_back(texture);
        
        //textures.
        glFramebufferTexture2D(
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, texture->getID(), 0
        );
    }
    
    unsigned int rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, attachments);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "ERROR::FRAMEBUFFER::Framebuffer is not complete!" << std::endl;
    }
    
    unbind();
}
