//
//  FrameBuffer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include <iostream>

#include "FPSCamera.hpp"
#include <vector>
#include "RenderItem.h"

#include "types.h"

class FrameBuffer {
    friend class SceneWindow;
private:
    FrameBuffer();
    
    camera_ptr m_camera;
    
    int m_tag;
    
    GLbitfield clearFlags;
    
    void clear();
    
    std::vector<render_item_ptr> items;
#ifdef VULKAN_API
    std::vector<VkPipelineLayout> pipelineLayouts;
    
    std::vector<VkPipeline> graphicsPipelines;
    std::vector<VkBuffer> vertexBuffers;
    std::vector<VkDeviceMemory> vertexBufferMemories;
    
    void createGraphicsPipeline(render_item_ptr& renderItem, bool blendEnabled = false);
    void createVertexBuffer(render_item_ptr& renderItem);
    
    void destroyPipeline(VkDevice device);
    void destroyVertexBuffer(VkDevice device);
    
    inline void createGraphicsPipelines() {
        for (auto item : items) {
            createGraphicsPipeline(item);
            createVertexBuffer(item);
        }
    }
#endif
protected:
    unsigned int id = 0;
public:
    FrameBuffer(int tag) : m_tag(tag) {}
    
    virtual ~FrameBuffer();
    
    inline void AddRenderItem(render_item_ptr item) {
        items.push_back(item);
    }
    
    inline void RemoveAllRenderItem() {
        items.clear();
    }
    
    inline void Display() {
        clear();
        for (auto it = items.begin(); it != items.end(); ++it) {
            (*it)->Process(m_tag);
        }
    }
    
    inline camera_ptr getCamera() { return m_camera; }
    
    inline void setClearFlags(const GLbitfield& clFlags) {
        clearFlags = clFlags;
    }
    
    inline void bind(GLuint type = GL_FRAMEBUFFER) { glBindFramebuffer(type, id); }
    inline void unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
};


inline void FrameBuffer::clear() {
#ifndef VULKAN_API
    glClear(clearFlags);
#endif
}
