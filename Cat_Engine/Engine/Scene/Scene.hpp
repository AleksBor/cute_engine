//
//  Scene.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 22/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

class Scene {
public:
    virtual ~Scene() {}

    virtual void Update() = 0;
};
