//
//  Types.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 30.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include <memory>

enum class VERTEX_PARAMS
{
    NORMALS = 1 << 0,
    TEX_COORDS = 1 << 1,
    TBN = 1 << 2
};

typedef unsigned char BYTE;

class ShaderProgram;
typedef std::shared_ptr<ShaderProgram> shader_program_ptr;

class Shader;
typedef std::shared_ptr<Shader> shader_ptr;

class IRenderItem;
typedef std::shared_ptr<IRenderItem> render_item_ptr;

class Texture;
typedef std::shared_ptr<Texture> texture_ptr;

class Camera;
typedef std::shared_ptr<Camera> camera_ptr;

class DepthFrameBuffer;
typedef std::shared_ptr<DepthFrameBuffer> depth_frame_buffer_ptr;

class FrameBuffer;
typedef std::shared_ptr<FrameBuffer> frame_buffer_ptr;

class TextureFrameBuffer;
typedef std::shared_ptr<TextureFrameBuffer> tex_frame_buffer_ptr;

class PingPongFrameBuffer;
typedef std::shared_ptr<PingPongFrameBuffer> ping_pong_frame_buffer_ptr;

class GeometryFrameBuffer;
typedef std::shared_ptr<GeometryFrameBuffer> geometry_frame_buffer_ptr;

class SSAOFrameBuffer;
typedef std::shared_ptr<SSAOFrameBuffer> ssao_frame_buffer_ptr;

class CubeMapDepthFrameBuffer;
typedef std::shared_ptr<CubeMapDepthFrameBuffer> cube_map_depth_frame_buffer_ptr;

class BasicItem;
typedef std::shared_ptr<BasicItem> object_ptr;

class Composite;
typedef std::shared_ptr<Composite> composite_item_ptr;

struct TextureItem;
typedef std::shared_ptr<TextureItem> tex_item_ptr;

class Mesh;
typedef std::shared_ptr<Mesh> mesh_ptr;
