//
//  gl.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 25/11/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include <GL/glew.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
