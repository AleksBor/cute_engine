#pragma once

#include <stdexcept>

struct Exception: std::runtime_error {
	Exception(const char* description) : runtime_error(description)
	{}
};