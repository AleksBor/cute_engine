//
//  Assert.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 20.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#pragma once
#include <iostream>

inline void TextAssert(const char* text) {
    std::cerr << text << std::endl;
    assert(false);
}
