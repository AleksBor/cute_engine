//
//  Shader.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/10/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "types.h"

enum class ShaderType
{
    NONE = -1, 
    VERTEX = 0, 
    FRAGMENT = 1, 
    GEOMETRY = 2
};

class Shader {
public:
    ~Shader();

    unsigned int getID() { return id; }

    static shader_ptr CreateShader(unsigned int type, const char* source);
private:
    Shader(unsigned int type);

    unsigned int id;
};
