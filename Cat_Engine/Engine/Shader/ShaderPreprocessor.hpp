//
//  ShaderPreprocessor.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 17/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include <iostream>
#include "ShaderProgram.h"

#include <fstream>
#include <sstream>

#include "Lexer.hpp"

enum PError {INCL_NOT_FOUND, EOT};

class ShaderPreprocessor {
private:
    const char* inChar;
    int inCounter = 0;
    char currentChar = 0;
    int startIndex = 0;

    std::string inStr;

    const std::string includeStr = "#include";
public:
    ShaderPreprocessor() = delete;
    ShaderPreprocessor(std::string inString) : inStr(inString) {}

    inline std::string process() {
        inChar = inStr.c_str();

        try {
            Include();
        }
        catch(PError) {
        }

        return inStr;
    }

    inline void Include() {

        std::string path = "";

        int index = 0;

        char includeChar;

        startIndex = inCounter;
        
        auto lexer = Lexer(inChar);
        
        int error = 1;
        
        Token* token = nullptr;
        
        while ((token = lexer.NextToken())) {
            
            if (token->type == SHARP) {
                Token* nextToken = lexer.NextToken();
                if (!nextToken) {
                    std::cout << "Error: Need derective after #" << std::endl;
                    throw EOT;
                }
                if (nextToken->value.compare("include") == 0) {
                    nextToken = lexer.NextToken();
                    if (nextToken && nextToken->type == SPACE) {
                        do {
                            nextToken = lexer.NextToken();
                        } while(nextToken && nextToken->type == SPACE);
                    } else {
                        std::cout << "Error: Need space after #include" << std::endl;
                        throw INCL_NOT_FOUND;
                    }
                    
                    if (nextToken->type == DQUOTE) {
                        while (true) {
                            nextToken = lexer.NextToken();
                            if (!nextToken) {
                                std::cout << "Error: Need \" at the end of directive include" << std::endl;
                                throw EOT;
                            }
                            
                            if (nextToken->type == DQUOTE) {break;}
                            
                            path += nextToken->value;
                        }
                        
                        startIndex = token->start;
                        inCounter = (int)nextToken->end;
                        error = 0;
                    } else {
                        std::cout << "Error: Need \" after #include" << std::endl;
                        throw INCL_NOT_FOUND;
                    }
                    
                    break;
                }
            }
        }
        
        if (error == 1) {
            throw INCL_NOT_FOUND;
        }

        std::string shaderPath = ShadersRootPath + path;
        std::ifstream stream(shaderPath);

        std::stringstream ss;

        std::string line;

        while (getline(stream, line))
        {
            ss << line << '\n';
        }
        inStr.replace(startIndex, inCounter - startIndex + 1, ss.str());
        inChar = inStr.c_str();
        inCounter = 0;
        
        Include();
    }

    inline void includeFromPath(std::string filepath) {
        
    }

    inline void nextChar() {
        if (inChar[inCounter]) {
            inCounter++;
            currentChar = inChar[inCounter];
        }
    }

    inline void nextCharWithoutSpace() {

        do {
            if (inChar[inCounter]) {
                inCounter++;
                currentChar = inChar[inCounter];
            }
            else {
                break;
            }
        } while (currentChar == ' ');
    }
};
