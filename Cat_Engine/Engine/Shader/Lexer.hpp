//
//  Lexer.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03.08.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include <string>
#include <vector>

enum TokenType {UNKNOWN, INCLUDE, IDENTIF, SHARP, SPACE, DQUOTE, DOT};

struct Token {
    TokenType type;
    unsigned long start = 0;
    unsigned long end = 0;
    std::string value;
};

class Lexer {
private:
    const char* m_prog;
    
    int currTokenIndex = 0;
    
    std::vector<Token> tokens;
    
    
    
    inline Token createToken(TokenType type, int endIndex, std::string value) {
        Token t;
        t.type = type;
        t.end = endIndex;
        t.value = value;
        t.start = endIndex - (value.size() - 1);
        
        return t;
    }
public:
    Lexer() = delete;
    Lexer(const char* &prog);
    
    std::vector<Token> GetTokens();
    
    inline Token* NextToken() {
        if (currTokenIndex < tokens.size()) {
            return &tokens[currTokenIndex++];
        }
        
        return nullptr;
    }
};
