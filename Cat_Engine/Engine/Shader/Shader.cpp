//
//  Shader.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 31/10/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "Shader.hpp"

#include "ShaderInterpret.hpp"

#include "open_gl.h"

#include "Exception.h"

Shader::Shader(unsigned int type) {
    id = glCreateShader(type);
}

shader_ptr Shader::CreateShader(unsigned int type, const char* source) {
    auto shader = shader_ptr(new Shader(type));

    auto id = shader->getID();

    const char* src = source;
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = new char[length];

        glGetShaderInfoLog(id, length, &length, message);

        std::string typeName;

        switch (type) {
        case GL_VERTEX_SHADER:
            typeName = "vertex";
            break;
        case GL_FRAGMENT_SHADER:
            typeName = "fragment";
            break;
        case GL_GEOMETRY_SHADER:
            typeName = "geometry";
            break;
        }

        std::string errorStr = "Failed to compile shader ";
        errorStr += typeName;
        errorStr += " ";
        errorStr += ":";
        errorStr += message;

        delete[] message;

        throw Exception(errorStr.c_str());

        return nullptr;
    }

    return shader;
}

Shader::~Shader() {
    glDeleteShader(id);
}




