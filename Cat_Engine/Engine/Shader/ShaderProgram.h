#pragma once

#include <iostream>

#include "open_gl.h"
#include "glm.h"
#include "FPSCamera.hpp"
#include "types.h"

#include "PointLight.hpp"
#include "DirLight.hpp"

#include "Defines.h"

static std::string ShadersRootPath = SHADERS_ROOT_PATH;

class ShaderProgram {
private:
    GLuint program;

    ShaderProgram();

    void createProgram(const char* vertexShader, const char* fragmentShader, const char* geomShader);

    std::string vShaderPath = "";
    std::string fShaderPath = "";
    std::string gShaderPath = "";
    
#ifdef VULKAN_API
    VkShaderModule vertShaderModule;
    VkShaderModule fragShaderModule;
#endif
public:
    static shader_program_ptr CreateShaderFromPath(const std::string& filepath);
    static shader_program_ptr CreateShaderFromPath(const std::string& vertShaderPath, const std::string& fragShaderPath, const std::string& geomShaderPath = "");
    static shader_program_ptr CreateShaderFromSource(const char* vertShader, const char* fragShader, const char* geomShader = nullptr);

    ~ShaderProgram();
    
#ifdef VULKAN_API
    VkShaderModule getVertShaderModule() { return vertShaderModule; };
    VkShaderModule getFragShaderModule() { return fragShaderModule; };
#endif

    inline void Use()
    {
        glUseProgram(program);
    }

    inline GLuint getProgram() { return program; }

    inline void setUniform(const char* key, float f)
    {
        GLint loc = glGetUniformLocation(program, key);
        glUniform1f(loc, f);
    }

    inline void setUniform(const char* key, int i)
    {
        GLint loc = glGetUniformLocation(program, key);
        glUniform1i(loc, i);
    }

    inline void setUniform(const char* key, const glm::vec3& vector)
    {
        setUniform(key, vector.x, vector.y, vector.z);
    }

    inline void setUniform(const char* key, float x, float y, float z)
    {
        GLint loc = glGetUniformLocation(program, key);
        glUniform3f(loc, x, y, z);
    }

    inline void setUniform(const char* key, float x, float y)
    {
        GLint loc = glGetUniformLocation(program, key);
        glUniform2f(loc, x, y);
    }

    inline void setUniform(const char* key, const glm::mat4& matrix)
    {
        GLuint loc = glGetUniformLocation(program, key);
        glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(matrix));
    }

    inline void setUniform(const std::string& key, const PointLight& pointLight)
    {
        setUniform((key + ".position").c_str(), pointLight.position);
        setUniform((key + ".ambient").c_str(), pointLight.ambient);
        setUniform((key + ".diffuse").c_str(), pointLight.diffuse);
        setUniform((key + ".specular").c_str(), pointLight.specular);
        setUniform((key + ".pointLightPos").c_str(), pointLight.position);
        setUniform((key + ".far_plane").c_str(), pointLight.far_plane);
        setUniform((key + ".constant").c_str(), pointLight.constant);
        setUniform((key + ".linear").c_str(), pointLight.linear);
        setUniform((key + ".quadratic").c_str(), pointLight.quadratic);
    }

    inline void setUniform(const std::string& key, const DirLight& dirLight)
    {
        auto lp = glm::normalize(dirLight.position);
        setUniform((key + ".position").c_str(), lp);
        setUniform((key + ".ambient").c_str(), dirLight.ambient);
        setUniform((key + ".diffuse").c_str(), dirLight.diffuse);
        setUniform((key + ".specular").c_str(), dirLight.specular);
    }

    inline void AttachCamera(Camera* camera, GLuint binding)
    {
        unsigned int lights_index = glGetUniformBlockIndex(program, "FPSCamera");
        glUniformBlockBinding(program, lights_index, binding);
        glBindBufferRange(GL_UNIFORM_BUFFER, binding, camera->getUBO(), 0, 2 * sizeof(glm::mat4));
    }
};
