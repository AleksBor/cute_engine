//
//  ShaderInterpret.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03.08.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "ShaderInterpret.hpp"
#include "ShaderPreprocessor.hpp"

std::string ShaderInterpret::Process() {
    auto preprocessor = ShaderPreprocessor(strProg);
    
    return preprocessor.process();
}
