#include "ShaderProgram.h"

#include "Shader.hpp"

#include "ShaderInterpret.hpp"

#include <fstream>
#include <sstream>
#include "Exception.h"

#include "SceneWindow.hpp"

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>

void InitResources(TBuiltInResource &Resources) {
    Resources.maxLights = 32;
    Resources.maxClipPlanes = 6;
    Resources.maxTextureUnits = 32;
    Resources.maxTextureCoords = 32;
    Resources.maxVertexAttribs = 64;
    Resources.maxVertexUniformComponents = 4096;
    Resources.maxVaryingFloats = 64;
    Resources.maxVertexTextureImageUnits = 32;
    Resources.maxCombinedTextureImageUnits = 80;
    Resources.maxTextureImageUnits = 32;
    Resources.maxFragmentUniformComponents = 4096;
    Resources.maxDrawBuffers = 32;
    Resources.maxVertexUniformVectors = 128;
    Resources.maxVaryingVectors = 8;
    Resources.maxFragmentUniformVectors = 16;
    Resources.maxVertexOutputVectors = 16;
    Resources.maxFragmentInputVectors = 15;
    Resources.minProgramTexelOffset = -8;
    Resources.maxProgramTexelOffset = 7;
    Resources.maxClipDistances = 8;
    Resources.maxComputeWorkGroupCountX = 65535;
    Resources.maxComputeWorkGroupCountY = 65535;
    Resources.maxComputeWorkGroupCountZ = 65535;
    Resources.maxComputeWorkGroupSizeX = 1024;
    Resources.maxComputeWorkGroupSizeY = 1024;
    Resources.maxComputeWorkGroupSizeZ = 64;
    Resources.maxComputeUniformComponents = 1024;
    Resources.maxComputeTextureImageUnits = 16;
    Resources.maxComputeImageUniforms = 8;
    Resources.maxComputeAtomicCounters = 8;
    Resources.maxComputeAtomicCounterBuffers = 1;
    Resources.maxVaryingComponents = 60;
    Resources.maxVertexOutputComponents = 64;
    Resources.maxGeometryInputComponents = 64;
    Resources.maxGeometryOutputComponents = 128;
    Resources.maxFragmentInputComponents = 128;
    Resources.maxImageUnits = 8;
    Resources.maxCombinedImageUnitsAndFragmentOutputs = 8;
    Resources.maxCombinedShaderOutputResources = 8;
    Resources.maxImageSamples = 0;
    Resources.maxVertexImageUniforms = 0;
    Resources.maxTessControlImageUniforms = 0;
    Resources.maxTessEvaluationImageUniforms = 0;
    Resources.maxGeometryImageUniforms = 0;
    Resources.maxFragmentImageUniforms = 8;
    Resources.maxCombinedImageUniforms = 8;
    Resources.maxGeometryTextureImageUnits = 16;
    Resources.maxGeometryOutputVertices = 256;
    Resources.maxGeometryTotalOutputComponents = 1024;
    Resources.maxGeometryUniformComponents = 1024;
    Resources.maxGeometryVaryingComponents = 64;
    Resources.maxTessControlInputComponents = 128;
    Resources.maxTessControlOutputComponents = 128;
    Resources.maxTessControlTextureImageUnits = 16;
    Resources.maxTessControlUniformComponents = 1024;
    Resources.maxTessControlTotalOutputComponents = 4096;
    Resources.maxTessEvaluationInputComponents = 128;
    Resources.maxTessEvaluationOutputComponents = 128;
    Resources.maxTessEvaluationTextureImageUnits = 16;
    Resources.maxTessEvaluationUniformComponents = 1024;
    Resources.maxTessPatchComponents = 120;
    Resources.maxPatchVertices = 32;
    Resources.maxTessGenLevel = 64;
    Resources.maxViewports = 16;
    Resources.maxVertexAtomicCounters = 0;
    Resources.maxTessControlAtomicCounters = 0;
    Resources.maxTessEvaluationAtomicCounters = 0;
    Resources.maxGeometryAtomicCounters = 0;
    Resources.maxFragmentAtomicCounters = 8;
    Resources.maxCombinedAtomicCounters = 8;
    Resources.maxAtomicCounterBindings = 1;
    Resources.maxVertexAtomicCounterBuffers = 0;
    Resources.maxTessControlAtomicCounterBuffers = 0;
    Resources.maxTessEvaluationAtomicCounterBuffers = 0;
    Resources.maxGeometryAtomicCounterBuffers = 0;
    Resources.maxFragmentAtomicCounterBuffers = 1;
    Resources.maxCombinedAtomicCounterBuffers = 1;
    Resources.maxAtomicCounterBufferSize = 16384;
    Resources.maxTransformFeedbackBuffers = 4;
    Resources.maxTransformFeedbackInterleavedComponents = 64;
    Resources.maxCullDistances = 8;
    Resources.maxCombinedClipAndCullDistances = 8;
    Resources.maxSamples = 4;
    Resources.maxMeshOutputVerticesNV = 256;
    Resources.maxMeshOutputPrimitivesNV = 512;
    Resources.maxMeshWorkGroupSizeX_NV = 32;
    Resources.maxMeshWorkGroupSizeY_NV = 1;
    Resources.maxMeshWorkGroupSizeZ_NV = 1;
    Resources.maxTaskWorkGroupSizeX_NV = 32;
    Resources.maxTaskWorkGroupSizeY_NV = 1;
    Resources.maxTaskWorkGroupSizeZ_NV = 1;
    Resources.maxMeshViewCountNV = 4;
    Resources.limits.nonInductiveForLoops = 1;
    Resources.limits.whileLoops = 1;
    Resources.limits.doWhileLoops = 1;
    Resources.limits.generalUniformIndexing = 1;
    Resources.limits.generalAttributeMatrixVectorIndexing = 1;
    Resources.limits.generalVaryingIndexing = 1;
    Resources.limits.generalSamplerIndexing = 1;
    Resources.limits.generalVariableIndexing = 1;
    Resources.limits.generalConstantMatrixVectorIndexing = 1;
}

ShaderProgram::ShaderProgram() {
#ifndef VULKAN_API
    program = glCreateProgram();
#endif
}

ShaderProgram::~ShaderProgram() {
#ifdef VULKAN_API
    vkDestroyShaderModule(SceneWindow::Instance->getDevice(), fragShaderModule, nullptr);
    vkDestroyShaderModule(SceneWindow::Instance->getDevice(), vertShaderModule, nullptr);
#else
    glDeleteProgram(program);
#endif
}

static std::vector<char> readFile(const std::string& filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    
    if (!file.is_open()) {
        std::cout << "Failed to open file: " << filename << std::endl;
        assert(false);
    }
    
    size_t fileSize = (size_t) file.tellg();
    std::vector<char> buffer(fileSize);
    
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    
    file.close();
    
    return buffer;
}

#ifdef VULKAN_API
VkShaderModule createShaderModule(const std::vector<char>& code) {
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = code.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
    
    VkShaderModule shaderModule;
    if (vkCreateShaderModule(SceneWindow::Instance->getDevice(), &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
        std::cout << "Failed to create shader module!" << std::endl;
        assert(false);
    }
    
    return shaderModule;
}
#endif

shader_program_ptr ShaderProgram::CreateShaderFromPath(const std::string& filepath)
{
    auto shader = shader_program_ptr(new ShaderProgram());

    std::string shaderPath = ShadersRootPath + filepath;
    std::ifstream stream(shaderPath);

    std::string line;
    std::stringstream ss[3];
    ShaderType type = ShaderType::NONE;
    while (getline(stream, line))
    {
        if (line.find("#shader") != std::string::npos)
        {
            if (line.find("vertex") != std::string::npos)
            {
                type = ShaderType::VERTEX;
            }
            else if (line.find("fragment") != std::string::npos) {
                type = ShaderType::FRAGMENT;
            }
        }
        else {
            ss[(int)type] << line << '\n';
        }
    }

    auto vShaderInterpret = ShaderInterpret((ss[(int)ShaderType::VERTEX]).str());
    auto fShaderInterpret = ShaderInterpret((ss[(int)ShaderType::FRAGMENT]).str());

    auto vertShader = vShaderInterpret.Process();
    auto fragShader = fShaderInterpret.Process();

    try {
        shader->createProgram(vertShader.c_str(), fragShader.c_str(), nullptr);
    }
    catch (const Exception& except) {
        std::cerr << except.what() << std::endl;
        return nullptr;
    }

    return shader;
}

shader_program_ptr ShaderProgram::CreateShaderFromPath(const std::string& vertShaderPath, const std::string& fragShaderPath, const std::string& geomShaderPath)
{
    auto shader = shader_program_ptr(new ShaderProgram());

#ifdef VULKAN_API
    static bool glslangInitialized = false;
    
    //auto vsCode = readFile(ShadersRootPath + vertShaderPath);
    //auto fsCode = readFile(ShadersRootPath + fragShaderPath);
    
    if (!glslangInitialized) {
        glslang::InitializeProcess();
        
        glslangInitialized = true;
    }
    
    
    //shader->vertShaderModule = createShaderModule(vsCode);
    //shader->fragShaderModule = createShaderModule(fsCode);
#endif
    shader->vShaderPath = ShadersRootPath + vertShaderPath;
    shader->fShaderPath = ShadersRootPath + fragShaderPath;
    shader->gShaderPath = ShadersRootPath + geomShaderPath;

    std::stringstream ss[3];

    for (int i = 0; i < 3; i++)
    {
        ShaderType type = (ShaderType)i;
        std::ifstream* stream;

        if (type == ShaderType::VERTEX) {
            stream = new std::ifstream(shader->vShaderPath);
        }
        else if (type == ShaderType::FRAGMENT) {
            stream = new std::ifstream(shader->fShaderPath);
        }
        else if (!geomShaderPath.empty()) {
            stream = new std::ifstream(shader->gShaderPath);
        }
        else {
            continue;
        }

        std::string line;

        while (getline(*stream, line))
        {
            ss[(int)type] << line << '\n';
        }

        delete stream;
    }

    std::string gShader = (ss[(int)ShaderType::GEOMETRY]).str();

    auto vShaderInterpret = ShaderInterpret((ss[(int)ShaderType::VERTEX]).str());
    auto fShaderInterpret = ShaderInterpret((ss[(int)ShaderType::FRAGMENT]).str());
    auto gShaderInterpret = ShaderInterpret((ss[(int)ShaderType::GEOMETRY]).str());

    auto vertShader = vShaderInterpret.Process();
    auto fragShader = fShaderInterpret.Process();
    auto geomShader = gShaderInterpret.Process();

#ifdef VULKAN_API
    const char* vertCString = vertShader.c_str();
    const char* fragCString = fragShader.c_str();
    
    glslang::TShader vShader(EShLangVertex);
    glslang::TShader fShader(EShLangFragment);
    
    vShader.setStrings(&vertCString, 1);
    fShader.setStrings(&fragCString, 1);
    
    int ClientInputSemanticsVersion = 100;
    glslang::EShTargetClientVersion VulkanClientVersion = glslang::EShTargetVulkan_1_2;
    glslang::EShTargetLanguageVersion TargetVersion = glslang::EShTargetSpv_1_3;
    
    vShader.setEnvInput(glslang::EShSourceGlsl, EShLangVertex, glslang::EShClientVulkan, ClientInputSemanticsVersion);
    vShader.setEnvClient(glslang::EShClientVulkan, VulkanClientVersion);
    vShader.setEnvTarget(glslang::EShTargetSpv, TargetVersion);
    
    fShader.setEnvInput(glslang::EShSourceGlsl, EShLangFragment, glslang::EShClientVulkan, ClientInputSemanticsVersion);
    fShader.setEnvClient(glslang::EShClientVulkan, VulkanClientVersion);
    fShader.setEnvTarget(glslang::EShTargetSpv, TargetVersion);
    
    glslang::TProgram vProgram;
    glslang::TProgram fProgram;
    
    TBuiltInResource Resources = {};
    InitResources(Resources);
    EShMessages messages = (EShMessages) (EShMsgSpvRules | EShMsgVulkanRules);
    
    const int DefaultVersion = 100;
    
    if (!vShader.parse(&Resources, DefaultVersion, false, messages)) {
        std::cerr << "Fail to parse vertex shader";
    }
    
    if (!fShader.parse(&Resources, DefaultVersion, false, messages)) {
        std::cerr << "Fail to parse vertex shader";
    }
    
    vProgram.addShader(&vShader);
    fProgram.addShader(&fShader);
    
    if (!vProgram.link(messages)) {
        std::cerr << "Fail to link vertex shader";
    }
    
    if (!fProgram.link(messages)) {
        std::cerr << "Fail to link fragment shader";
    }
    
    std::vector<unsigned int> vsCode;
    std::vector<unsigned int> fsCode;
    
    spv::SpvBuildLogger logger;
    glslang::SpvOptions spvOptions;
    
     
    
    glslang::GlslangToSpv(*vProgram.getIntermediate(EShLangVertex), vsCode, &logger);
    glslang::GlslangToSpv(*fProgram.getIntermediate(EShLangFragment), fsCode, &logger);
    
    std::vector<char> vertSpirV;
    vertSpirV.resize(sizeof(unsigned int)*vsCode.size());
    memcpy(vertSpirV.data(), vsCode.data(), sizeof(unsigned int)*vsCode.size());
    
    std::vector<char> fragSpirV;
    fragSpirV.resize(sizeof(unsigned int)*fsCode.size());
    memcpy(fragSpirV.data(), fsCode.data(), sizeof(unsigned int)*fsCode.size());
    
    shader->vertShaderModule = createShaderModule(vertSpirV);
    shader->fragShaderModule = createShaderModule(fragSpirV);
#else
    try {
        shader->createProgram(vertShader.c_str(), fragShader.c_str(), geomShader.empty() ? nullptr : geomShader.c_str());
    }
    catch (const Exception& except) {
        std::cerr << except.what() << std::endl;
        return nullptr;
    }
#endif

    return shader;
}

shader_program_ptr ShaderProgram::CreateShaderFromSource(const char* vertShader, const char* fragShader, const char* geomShader)
{
    auto shader = shader_program_ptr(new ShaderProgram());

    try {
        shader->createProgram(vertShader, fragShader, geomShader);
    }
    catch (const Exception& except) {
        std::cerr << except.what() << std::endl;
        return nullptr;
    }

    return shader;
}

void ShaderProgram::createProgram(const char* vertexShader, const char* fragmentShader, const char* geomShader)
{
    try {
        auto vs = Shader::CreateShader(GL_VERTEX_SHADER, vertexShader);
        auto fs = Shader::CreateShader(GL_FRAGMENT_SHADER, fragmentShader);
        if (geomShader)
        {
            auto gs = Shader::CreateShader(GL_GEOMETRY_SHADER, geomShader);
            glAttachShader(program, gs->getID());
        }

        glAttachShader(program, vs->getID());
        glAttachShader(program, fs->getID());
        glLinkProgram(program);
        glValidateProgram(program);
    }
    catch (const Exception& except) {
        std::string errorStr = except.what();
        errorStr += ("\nVertexShader" + vShaderPath);
        errorStr += ("\nFragmentShader" + fShaderPath);
        errorStr += ("\nGeometryShader" + gShaderPath);
        throw Exception(errorStr.c_str());
    }

    int result;
    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
        char* message = new char[length];

        glGetProgramInfoLog(program, length, &length, message);

        std::string errorStr = "Failed to link program :";
        errorStr += message;

        delete[] message;

        throw Exception(errorStr.c_str());
    }
}
