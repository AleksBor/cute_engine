//
//  ShaderInterpret.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03.08.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include <string>

class ShaderInterpret {
private:
    std::string strProg;
public:
    ShaderInterpret() = delete;
    ShaderInterpret(const std::string &prog) : strProg(prog) {}
    std::string Process();
};
