//
//  Lexer.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03.08.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "Lexer.hpp"
#include <iostream>
#include <unordered_map>

std::unordered_map<std::string, TokenType> keywords;

Lexer::Lexer(const char* &prog) : m_prog(prog) {
    keywords["#include"] = INCLUDE;
    
    tokens = GetTokens();
}

std::vector<Token> Lexer::GetTokens() {
    std::vector<Token> tokenArr;
    std::string buffer;
    
    for (int chI = 0; m_prog[chI]; chI++) {
        char currCh = m_prog[chI];
        
        if (isalnum(currCh)) {
            buffer += currCh;
        } else {
            if (buffer.size()) {
                /*
                if (keywords[buffer] == INCLUDE) {
                    tokenArr.push_back(createToken(INCLUDE, chI - 1, buffer));
                } else {
                    tokenArr.push_back(createToken(IDENTIF, chI - 1, buffer));
                }*/
                
                tokenArr.push_back(createToken(IDENTIF, chI - 1, buffer));
                
                buffer = "";
            }
            
            if (currCh == '#') {
                tokenArr.push_back(createToken(SHARP, chI, "#"));
            } else if (currCh == ' ') {
                tokenArr.push_back(createToken(SPACE, chI, " "));
            } else if (currCh == '"') {
                tokenArr.push_back(createToken(DQUOTE, chI, "\""));
            } else if (currCh == '.') {
                tokenArr.push_back(createToken(DOT, chI, "."));
            }
            
        }
    }
    
    return tokenArr;
}
