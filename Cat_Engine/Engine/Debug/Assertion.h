//
//  Assertion.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 26.11.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#define _ASSERT_GLUE(a, b) a ## b
#define ASSERT_GLUE(a, b) _ASSERT_GLUE(a, b)

#define STATIC_ASSERT(expr)\
    enum\
    {\
        ASSERT_GLUE(g_assert_fail_, __LINE__) = 1/(int)(!!(expr))\
    }

