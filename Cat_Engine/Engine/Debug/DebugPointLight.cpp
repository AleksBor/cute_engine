//
//  DebugPointLight.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 04/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "DebugPointLight.hpp"
#include "ShaderProgram.h"

const char* vShader =
"#version 330 core\n"
"layout(location = 0) in vec3 position;\n"
"uniform mat4 model;\n"
"layout (std140) uniform FPSCamera\n"
"{\n"
"uniform mat4 projection;\n"
"uniform mat4 view;\n"
"};\n"
"void main()\n"
"{\n"
"gl_Position = projection * view * model * vec4(position.xyz, 1.0);\n"
"}";

const char* fShader =
"#version 330 core\n"
"out vec4 resultColor;\n"
"void main()\n"
"{\n"
"resultColor = vec4(1.0, 0.98, 0.1, 1.0f);\n"
"}";

DebugPointLight::DebugPointLight(float radius, Camera* camera, glm::vec3* position) :
Sphere(radius, static_cast<int>(VERTEX_PARAMS::NORMALS)), pos(position)
{
    shader = ShaderProgram::CreateShaderFromSource(vShader, fShader);
    shader->AttachCamera(camera, 0);
}

void DebugPointLight::Process(int tag) {
    shader->Use();
    setPosition(*pos);
    shader->setUniform("model", GetModelMatrix());
    Draw();
}
