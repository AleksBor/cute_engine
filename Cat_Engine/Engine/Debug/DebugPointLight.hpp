//
//  DebugPointLight.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 04/02/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Sphere.hpp"

class DebugPointLight: public Sphere
{
private:
    shader_program_ptr shader;
    
    glm::vec3* pos;
public:
    DebugPointLight(float radius, Camera* camera, glm::vec3* position);
    DebugPointLight() = delete;
    
    void Process(int tag) override;
};
