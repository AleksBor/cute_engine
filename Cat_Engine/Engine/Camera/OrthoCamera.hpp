//
//  OrthoCamera.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 21/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "Camera.hpp"

class OrthoCamera: public Camera {
private:
    GLfloat nearPlane = 0.1;
    GLfloat farPlane = 10.f;
public:
    OrthoCamera() = delete;
    OrthoCamera(int tag) {}

    inline glm::mat4 projection() override {
        glm::mat4 proj(1.0f);
        proj = glm::ortho(-2.0f, 2.0f, -2.0f, 2.0f, nearPlane, farPlane);
        return proj;
    }

    inline glm::mat4 view() override {
        glm::mat4 view(1.0f);
        view = glm::lookAt(pos,
            front,
            glm::vec3(0.0f, 1.0f, 0.0f));
        return view;
    }
};
