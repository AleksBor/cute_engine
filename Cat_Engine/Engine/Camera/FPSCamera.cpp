//
//  FPSCamera.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 20/01/2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#include "FPSCamera.hpp"

FPSCamera::FPSCamera(GLfloat screenRatio) : scrRatio(screenRatio) {
#ifndef VULKAN_API
    glGenBuffers(1, &ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
#endif
}
