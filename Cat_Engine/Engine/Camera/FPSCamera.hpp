//
//  Camera.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/11/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "Camera.hpp"

class FPSCamera: public Camera {
private:
    GLfloat scrRatio;
    
    GLfloat nearPlane = 0.1;
    GLfloat farPlane = 50.f;
    
    GLfloat fov = 45.0f;
public:
    FPSCamera() = delete;
    FPSCamera(GLfloat screenRatio);
    
    inline void setNearPlane(GLfloat dist) { if (dist > 0.f && dist < farPlane) nearPlane = dist; }
    inline void setFarPlane(GLfloat dist) { if (dist > nearPlane) farPlane = dist; }
    
    glm::mat4 projection() override {
        glm::mat4 proj(1.0f);
        proj = glm::perspective(glm::radians(fov), scrRatio, nearPlane, farPlane);
        return proj;
    }

    glm::mat4 view() override {
        glm::mat4 view(1.0f);
        return glm::lookAt(pos, pos + front, up);
    }
};
