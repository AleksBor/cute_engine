//
//  Camera.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/11/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "open_gl.h"
#include "glm.h"

#include "Scene.hpp"

class Camera {
protected:
    unsigned int ubo;
    int m_tag;
public:
    glm::vec3 pos = glm::vec3(0.0f, 0.0f, 3.0f);
    glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    
    virtual glm::mat4 projection() = 0;
    virtual glm::mat4 view() = 0;

    inline void UpdateUBO() {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(projection()));
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view()));
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }
    
    inline unsigned int getUBO() { return ubo; }
};
