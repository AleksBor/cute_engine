//
//  Cube.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "BasicItem.hpp"

struct CubeOptions {
    FACE_ORIENTATION face_orient = FACE_ORIENTATION::BACK;
};

class Cube: public BasicItem {
public:
    Cube(CubeOptions opts = CubeOptions());
    ~Cube() { glDeleteVertexArrays(1, &VAO); }
    
    void Draw() override final;
private:
    GLuint VAO;
};
