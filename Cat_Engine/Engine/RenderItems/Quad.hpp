//
//  Plain.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "BasicItem.hpp"
#include "ComplexItem.hpp"

struct QuadOptions {
    glm::vec2 texuresNum = { 1.0f, 1.0f };
    uint8_t vertParams = static_cast<int>(VERTEX_PARAMS::NORMALS) | static_cast<int>(VERTEX_PARAMS::TEX_COORDS);
};

static const glm::vec3 s_QuadHorizontalCoords[4] = {{ 0.5f, 0.0f, 0.5f }, { 0.5f, 0.0f, -0.5f }, { -0.5f, 0.0f, -0.5f }, { -0.5f, 0.0f, 0.5f }};
static const glm::vec3 s_QuadVerticalCoords[4] = {{ 0.5f, -0.5f, 0.0f }, { 0.5f, 0.5f, 0.0f }, { -0.5f, 0.5f, 0.0f }, { -0.5f, -0.5f, 0.0f }};

template<class Base>
class Quad: public Base
{
public:
    Quad(QuadOptions opts = QuadOptions(), const glm::vec3* coords = s_QuadHorizontalCoords);
    ~Quad() { glDeleteVertexArrays(1, &VAO); }
    
    void Draw() override final;
protected:
    void Draw(unsigned int count) override;
    GLuint VAO;
};

template<class Base>
Quad<Base>::Quad(QuadOptions opts, const glm::vec3* coords)
{
    glm::vec2 texuresNum = opts.texuresNum;
    glm::vec3 normals[4];
    
    normals[1] = glm::normalize(glm::cross(glm::normalize(coords[3] - coords[1]), glm::normalize(coords[0] - coords[1])));
    normals[2] = glm::normalize(glm::cross(glm::normalize(coords[3] - coords[2]), glm::normalize(coords[1] - coords[2])));
    
    normals[0] = glm::normalize(normals[1] + normals[2]);
    normals[3] = normals[0];
    
    GLuint positionSize = sizeof(glm::vec3) * 4;
    GLuint normalSize = sizeof(normals);

    glm::vec2 uv[4];
    
    uv[0] = { 0.0f, 0.0f };
    uv[1] = { 0.0f, texuresNum.y };
    uv[2] = { texuresNum.x, texuresNum.y };
    uv[3] = { texuresNum.x, 0.0f };
    
    GLuint texSize = sizeof(uv);
    
    GLuint tangentSize = sizeof(glm::vec3) * 4;
    GLuint bitangentSize = sizeof(glm::vec3) * 4;

    GLuint indices[] = {
        0, 1, 3,
        1, 2, 3,
    };

    // VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    GLuint sizeOfParams = positionSize + ((opts.vertParams & static_cast<int>(VERTEX_PARAMS::NORMALS)) ? normalSize : 0) + ((opts.vertParams & static_cast<int>(VERTEX_PARAMS::TEX_COORDS)) ? texSize : 0) + ((opts.vertParams & static_cast<int>(VERTEX_PARAMS::TBN)) ? tangentSize + bitangentSize : 0);
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeOfParams, NULL, GL_STATIC_DRAW);

    GLuint param = 0;
    GLuint buffOffset = 0;

    glBufferSubData(GL_ARRAY_BUFFER, buffOffset, positionSize, coords);

    glEnableVertexAttribArray(param);
    glVertexAttribPointer(param, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

    buffOffset += positionSize;
    param++;
    
    if (opts.vertParams & static_cast<int>(VERTEX_PARAMS::NORMALS))
    {
        glBufferSubData(GL_ARRAY_BUFFER, buffOffset, normalSize, &normals);

        glEnableVertexAttribArray(param);
        glVertexAttribPointer(param, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, (void*)(buffOffset));

        buffOffset += normalSize;
        param++;
    }
    

    if (opts.vertParams & static_cast<int>(VERTEX_PARAMS::TEX_COORDS))
    {
        glBufferSubData(GL_ARRAY_BUFFER, buffOffset, texSize, &uv);

        glEnableVertexAttribArray(param);
        glVertexAttribPointer(param, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, (void*)(buffOffset));

        buffOffset += texSize;
        param++;
    }
    
    if (opts.vertParams & static_cast<int>(VERTEX_PARAMS::TBN))
    {
        glm::vec3 tangent;
        glm::vec3 bitangent;
        
        glm::vec3 v0 = coords[indices[0]];
        glm::vec3 v1 = coords[indices[1]];
        glm::vec3 v2 = coords[indices[2]];
        
        glm::vec2 uv0 = uv[indices[0]];
        glm::vec2 uv1 = uv[indices[1]];
        glm::vec2 uv2 = uv[indices[2]];
        
        glm::vec3 edge1 = v1-v0;
        glm::vec3 edge2 = v2-v0;
        
        glm::vec2 deltaUV1 = uv1-uv0;
        glm::vec2 deltaUV2 = uv2-uv0;
        
        GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent = glm::normalize(tangent);

        bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent = glm::normalize(bitangent);
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        glm::vec3 tangentArr[] = { tangent, tangent, tangent, tangent };
        
        glBufferSubData(GL_ARRAY_BUFFER, buffOffset, sizeof(glm::vec3) * 4, &tangentArr);

        glEnableVertexAttribArray(param);
        glVertexAttribPointer(param, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, (void*)(buffOffset));

        buffOffset += tangentSize;
        param++;
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        glm::vec3 bitangentArr[] = { bitangent, bitangent, bitangent, bitangent };
        
        glBufferSubData(GL_ARRAY_BUFFER, buffOffset, sizeof(glm::vec3) * 4, &bitangentArr);

        glEnableVertexAttribArray(param);
        glVertexAttribPointer(param, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, (void*)(buffOffset));

        buffOffset += bitangentSize;
        param++;
    }
    

    GLuint EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

template<class Base>
void Quad<Base>::Draw()
{
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

template<class Base>
void Quad<Base>::Draw(unsigned int count)
{
    glBindVertexArray(VAO);
    glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, count);
    glBindVertexArray(0);
}
