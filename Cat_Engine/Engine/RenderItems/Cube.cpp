//
//  Cube.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "Cube.hpp"
#include "Defines.h"

Cube::Cube(CubeOptions opts)
{
    GLfloat position[] = {
               -0.5f,  0.5f, -0.5f,
               -0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f,  0.5f, -0.5f,
        
                -0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f,  0.5f,  0.5f,
                
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f,  0.5f,
                0.5f,  0.5,  0.5f,
                0.5f,  0.5f, -0.5f,
        
                -0.5f, -0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
                0.5f,  0.5f,  0.5f,
                0.5f, -0.5f,  0.5f,
        
                -0.5f,  0.5f, -0.5f,
                0.5f,  0.5f, -0.5f,
                0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
        
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f,  0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f,  0.5f
           };
    
    Face faces[12];

    if (opts.face_orient == FACE_ORIENTATION::BACK) {
        faces[0] = { 0, 1, 2 };
        faces[1] = { 2, 3, 0 };
        faces[2] = { 4, 5, 6 };
        faces[3] = { 6, 7, 4 };
        faces[4] = { 8, 9, 10 };
        faces[5] = { 10, 11, 8 };
        faces[6] = { 12, 13, 14 };
        faces[7] = { 14, 15, 12 };
        faces[8] = { 16, 17, 18 };
        faces[9] = { 18, 19, 16 };
        faces[10] = { 20, 21, 22 };
        faces[11] = { 22, 21, 23 };
    } else {
        faces[0] = { 2, 1, 0 };
        faces[1] = { 0, 3, 2 };
        faces[2] = { 6, 5, 4 };
        faces[3] = { 4, 7, 6 };
        faces[4] = { 10, 9, 8 };
        faces[5] = { 8, 11, 10 };
        faces[6] = { 14, 13, 12 };
        faces[7] = { 12, 15, 14 };
        faces[8] = { 18, 17, 16 };
        faces[9] = { 16, 19, 18 };
        faces[10] = { 22, 21, 20 };
        faces[11] = { 23, 21, 22 };
    }
    
    // VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_STATIC_DRAW);
    
    GLuint EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(faces), faces, GL_STATIC_DRAW);
    
    // Vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, ((char*) NULL) + (0));
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Cube::Draw()
{
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
