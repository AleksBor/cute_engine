//
//  Vertex.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 01.02.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#pragma once

struct Vertex {
public:
    virtual size_t sizeOfStruct() = 0;
    virtual VkFormat formatOfMember(unsigned int index) = 0;
    virtual size_t offsetOfMember(unsigned int index) = 0;
    virtual size_t countOfMember() = 0;
    
    VkVertexInputBindingDescription getBindingDescription() {
        VkVertexInputBindingDescription bindingDescribtion{};
        bindingDescribtion.binding = 0;
        bindingDescribtion.stride = sizeOfStruct();
        bindingDescribtion.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        
        return bindingDescribtion;
    }
    
    std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
        
        attributeDescriptions.resize(countOfMember());
        
        int counter = 0;
        
        for (auto& attributeDescription : attributeDescriptions) {
            attributeDescription.binding = 0;
            attributeDescription.location = counter;
            attributeDescription.format = formatOfMember(counter);
            attributeDescription.offset = offsetOfMember(counter);
            counter++;
        }
        
        return attributeDescriptions;
    }
    
    virtual ~Vertex() {}
};
