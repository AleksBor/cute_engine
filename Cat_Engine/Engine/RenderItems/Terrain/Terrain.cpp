//
//  Terrain.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "Terrain.hpp"

#include <stdio.h>

#include "Defines.h"

#include "Mesh.hpp"

#include "TerrainModel.hpp"

static std::string TerrainFolderPath = TERRAIN_ROOT_PATH;

Terrain::Terrain(std::string filepath, float size)
{
    std::string terrainPath = TerrainFolderPath + filepath;
    
    FILE *pFile = NULL;
    #ifdef WINDOWS
            fopen_s(&pFile, terrainPath.c_str(), "rb");
    #else
            pFile = fopen( terrainPath.c_str(), "rb");
    #endif
    //pFile = fopen_s( terrainPath.c_str(), "rb");
    if ( pFile == NULL ) {
        std::cout << "Failed to open file " << terrainPath;
        return;
    }
    
    int prev=ftell(pFile);
    fseek(pFile, 0L, SEEK_END);
    m_fileSize = ftell(pFile);
    fseek(pFile,prev,SEEK_SET);
    
    m_heightMap = new BYTE[m_fileSize];
    
    fread(m_heightMap, 1, m_fileSize, pFile);
    
    int result = ferror( pFile );
    
    if (result)
    {
        std::cout << "Cant get data from: " << terrainPath;
    }
    
    fclose(pFile);
    
    sideSize = sqrt(m_fileSize);
    
    quadsNumber = size * size;
    
    const int vertsPerQuad = 4;
    const int indicesPerQuad = 6;
    
    std::vector<MeshVertex> meshVertices(quadsNumber * vertsPerQuad);
    std::vector<unsigned int> meshIndices(quadsNumber * indicesPerQuad);
    
    size_t i_vert = 0;
    size_t i_index = 0;
    for (int x = 0; x < size; x++) {
        for (int z = 0; z < size; z++) {
            MeshVertex* vertices = meshVertices.data() + i_vert;
            unsigned int* indices = meshIndices.data() + i_index;
            
            indices[0] = 0 + static_cast<int>(i_vert);
            indices[1] = 1 + static_cast<int>(i_vert);
            indices[2] = 3 + static_cast<int>(i_vert);
            indices[3] = indices[1];
            indices[4] = 2 + static_cast<int>(i_vert);
            indices[5] = indices[2];
            
            i_vert  += vertsPerQuad;
            i_index += indicesPerQuad;
            
            vertices[0].Position = glm::vec3(static_cast<float>(x - size/2), height(x, z), static_cast<float>(z - size/2));
            
            vertices[1].Position = glm::vec3(static_cast<float>(x - size/2), height(static_cast<float>(x), z+1), static_cast<float>(z+1 - size/2));
            
            vertices[2].Position = glm::vec3(static_cast<float>(x +1 - size/2), height(static_cast<float>(x +1), static_cast<float>(z+1)), static_cast<float>(z+1 - size/2));
            
            vertices[3].Position = glm::vec3(static_cast<float>(x+1 - size/2), height(static_cast<float>(x+1), static_cast<float>(z)), static_cast<float>(z - size/2));
            
            vertices[1].Normal = glm::normalize(glm::cross(glm::normalize(vertices[3].Position - vertices[1].Position), glm::normalize(vertices[0].Position - vertices[1].Position)));
            vertices[2].Normal = glm::normalize(glm::cross(glm::normalize(vertices[3].Position - vertices[2].Position), glm::normalize(vertices[1].Position - vertices[2].Position)));
            
            vertices[0].Normal = glm::normalize(vertices[1].Normal + vertices[2].Normal);
            vertices[3].Normal = vertices[0].Normal;
            
            vertices[0].TexCoords = { 0.0f, 0.0f };
            vertices[1].TexCoords = { 0.0f, 1.0f };
            vertices[2].TexCoords = { 1.0f, 1.0f };
            vertices[3].TexCoords = { 1.0f, 0.0f };
            
            glm::vec3 tangent;
            glm::vec3 bitangent;
                    
            glm::vec3 v0 = meshVertices[indices[0]].Position;
            glm::vec3 v1 = meshVertices[indices[1]].Position;
            glm::vec3 v2 = meshVertices[indices[2]].Position;
                    
            glm::vec2 uv0 = meshVertices[indices[0]].TexCoords;
            glm::vec2 uv1 = meshVertices[indices[1]].TexCoords;
            glm::vec2 uv2 = meshVertices[indices[2]].TexCoords;
                    
            glm::vec3 edge1 = v1-v0;
            glm::vec3 edge2 = v2-v0;
                    
            glm::vec2 deltaUV1 = uv1-uv0;
            glm::vec2 deltaUV2 = uv2-uv0;
                    
            GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

            tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
            tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
            tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
            tangent = glm::normalize(tangent);

            bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
            bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
            bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
            bitangent = glm::normalize(bitangent);
            
            vertices[0].Tangent = tangent;
            vertices[1].Tangent = tangent;
            vertices[2].Tangent = tangent;
            vertices[3].Tangent = tangent;
            
            vertices[0].Bitangent = bitangent;
            vertices[1].Bitangent = bitangent;
            vertices[2].Bitangent = bitangent;
            vertices[3].Bitangent = bitangent;
        }
    }
    
    mesh = mesh_ptr(new Mesh(meshVertices, meshIndices));
    
    //setScale(glm::vec3(size/static_cast<float>(MAP_SIZE), 1.0f, size/static_cast<float>(MAP_SIZE)));
}

Terrain::~Terrain() {
    glDeleteVertexArrays(1, &VAO);
    delete [] m_heightMap;
}

void Terrain::Draw() {
    mesh->Draw();
}

float Terrain::height(int X, int Y) {
    int x = X * (sideSize / 40);
    int y = Y * (sideSize / 40);
    
    if(!m_heightMap) return 0;
    
    return static_cast<float>(m_heightMap[x + (y * sideSize)]) * 0.02 - 2.0;
}
