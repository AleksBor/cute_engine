//
//  Terrain.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "types.h"
#include "OrthoCamera.hpp"
#include "types.h"
#include "BasicItem.hpp"

class DirLight;
class PointLight;

#define MAP_QUALITY 40

class Terrain: public BasicItem {
public:
    Terrain(std::string filepath, float size);
    ~Terrain();
    
    void Draw() override final;
private:
    GLuint VAO;
    
    mesh_ptr mesh;
    
    BYTE * m_heightMap = nullptr;
    
    int sideSize;
    
    int quadsNumber;
    
    int m_fileSize;
    
    float height(int X, int Y);
};
