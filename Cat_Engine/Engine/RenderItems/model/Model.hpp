//
//  Model.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 24/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "Mesh.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vector>

#include <type_traits>

#include "ComplexItem.hpp"

template <class ModelType>
class Model;

template <class ModelType>
class ModelBase : public ModelType
{
    friend Model<BasicItem>;
    friend Model<Composite>;
private:
    std::vector<mesh_ptr> meshes;
    std::string directory;
    
    std::vector<texture_ptr> mesh_textures_loaded;
    
    void loadModel(std::string path);
    void processNode(aiNode *node, const aiScene *scene);
    mesh_ptr processMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<texture_ptr> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
    
    unsigned int meshOrderNumber = 0;
    
    void Draw() override final {}
    
    inline void AddMesh(mesh_ptr mesh)
    {
        mesh->orderNumber = meshOrderNumber++;
        meshes.push_back(mesh);
    }
protected:
    inline void bindMeshVAO(unsigned int index) { return meshes[index]->bindVAO(); }
public:
    ModelBase(const char *path)
    {
        loadModel(path);
    }
    
    ModelBase(mesh_ptr mesh)
    {
        AddMesh(mesh);
    }
    
    ModelBase() = delete;
    
    void AddTexture(const std::string& path, std::string type, TextureOptions opts = TextureOptions()) override final;
    void AddTexture(texture_ptr texture) override final;
    
    virtual void ProcessMesh(int tag, mesh_ptr mesh) = 0;
    
    void AddInstances(std::vector<glm::vec2> params);
};

template <>
class Model<Composite> : public ModelBase<Composite> {
public:
    Model(const char *path) : ModelBase<Composite>(path) {}
    Model(mesh_ptr mesh) : ModelBase<Composite>(mesh) {}
    
    void ComponentProcess(int tag) override final {
        for (std::vector<mesh_ptr>::iterator it = meshes.begin(); it != meshes.end(); ++it) {
            ProcessMesh(tag, (*it));
        }
    }
};

template <>
class Model<BasicItem> : public ModelBase<BasicItem> {
public:
    Model(const char *path) : ModelBase<BasicItem>(path) {}
    Model(mesh_ptr mesh) : ModelBase<BasicItem>(mesh) {}
private:
    void Process(int tag) override final {
        for (std::vector<mesh_ptr>::iterator it = meshes.begin(); it != meshes.end(); ++it) {
            ProcessMesh(tag, (*it));
        }
    }
};

template <class ModelType>
void ModelBase<ModelType>::loadModel(std::string path)
{
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
    
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
        return;
    }
    directory = path.substr(0, path.find_last_of('/'));
    meshOrderNumber = 0;
    processNode(scene->mRootNode, scene);
}

template <class ModelType>
void ModelBase<ModelType>::processNode(aiNode *node, const aiScene *scene)
{
    for(unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        mesh_ptr processedMesh = processMesh(mesh, scene);
        AddMesh(processedMesh);
    }
    
    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene);
    }
}

template <class ModelType>
mesh_ptr ModelBase<ModelType>::processMesh(aiMesh *mesh, const aiScene *scene)
{
    std::vector<MeshVertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<texture_ptr> textures;
    
    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        MeshVertex vertex;
        glm::vec3 vector;
        
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;
        
        if (mesh->mTextureCoords[0])
        {
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        } else {
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }
        
        glm::vec3 tan;
        tan.x = mesh->mTangents[i].x;
        tan.y = mesh->mTangents[i].y;
        tan.z = mesh->mTangents[i].z;
        vertex.Tangent = tan;
        
        glm::vec3 bi_tan;
        bi_tan.x = mesh->mBitangents[i].x;
        bi_tan.y = mesh->mBitangents[i].y;
        bi_tan.z = mesh->mBitangents[i].z;
        vertex.Bitangent = bi_tan;
        
        vertices.push_back(vertex);
    }
    
    for(unsigned int i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for(unsigned int j = 0; j < face.mNumIndices; j++)
        {
            indices.push_back(face.mIndices[j]);
        }
    }
    
    mesh_ptr resMesh = mesh_ptr(new Mesh(vertices, indices));
    
    if (mesh->mMaterialIndex >= 0)
    {
        aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];

        std::vector<texture_ptr> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, TEXTURE_DIFFUSE);
        //textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
        for (auto it = diffuseMaps.begin(); it != diffuseMaps.end(); ++it) {
            resMesh->AddTexture(*it);
        }
        
        std::vector<texture_ptr> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, TEXTURE_SPECULAR);
        //textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
        for (auto it = specularMaps.begin(); it != specularMaps.end(); ++it) {
            resMesh->AddTexture(*it);
        }

        std::vector<texture_ptr> normalMaps = loadMaterialTextures(material, aiTextureType_NORMALS, TEXTURE_NORMAL_MAP);
        //textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
        for (auto it = normalMaps.begin(); it != normalMaps.end(); ++it) {
            resMesh->AddTexture(*it);
        }
    }
    
    return resMesh;
}

template <class ModelType>
std::vector<texture_ptr> ModelBase<ModelType>::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
    std::vector<texture_ptr> textures;
    for(unsigned int i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        bool skip = false;
        for(unsigned int j = 0; j < mesh_textures_loaded.size(); j++)
        {
            if (std::strcmp(mesh_textures_loaded[j]->path.C_Str(), str.C_Str()) == 0 && typeName == mesh_textures_loaded[j]->getType())
            {
                textures.push_back(mesh_textures_loaded[j]);
                skip = true;
                break;
            }
        }
        if (!skip)
        {
            texture_ptr texture = Texture::CreateTexture(str.C_Str(), directory, typeName);
            texture->path = str;
            textures.push_back(texture);
            mesh_textures_loaded.push_back(texture);
        }
    }
    return textures;
}

template <class ModelType>
void ModelBase<ModelType>::AddTexture(const std::string& path, std::string type, TextureOptions opts)
{
    for(unsigned int i = 0; i < meshes.size(); i++)
    {
        //meshes[i].Draw(*shader);
        bool skip = false;
        for(unsigned int j = 0; j < mesh_textures_loaded.size(); j++)
        {
            if (std::strcmp(mesh_textures_loaded[j]->path.C_Str(), path.c_str()) == 0 && type == mesh_textures_loaded[j]->getType())
            {
                meshes[i]->AddTexture(mesh_textures_loaded[j]);
                skip = true;
                break;
            }
        }
        if (!skip)
        {
            texture_ptr texture = Texture::CreateTexture(path.c_str(), "", type);
            texture->path = path;
            meshes[i]->AddTexture(texture);
            mesh_textures_loaded.push_back(texture);
        }
    }
}

template <class ModelType>
void ModelBase<ModelType>::AddTexture(texture_ptr texture)
{
    for(unsigned int i = 0; i < meshes.size(); i++) {
        meshes[i]->AddTexture(texture);
    }
}

template <class ModelType>
void ModelBase<ModelType>::AddInstances(std::vector<glm::vec2> params) {
    for(unsigned int i = 0; i < meshes.size(); i++) {
        meshes[i]->AddInstances(params);
    }
}
