//
//  Mesh.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 24/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include <iostream>
#include <vector>

#include "glm.h"
#include "types.h"
#include "BasicItem.hpp"

#include <assimp/postprocess.h>

struct MeshVertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    glm::vec3 Tangent;
    glm::vec3 Bitangent;
};

template <class ModelType>
class ModelBase;

class Mesh: public BasicItem {
    friend class ModelBase<BasicItem>;
    friend class ModelBase<Composite>;
private:
    std::vector<MeshVertex> vertices;
    std::vector<unsigned int> indices;
    unsigned int VAO;
    
    void setupMesh();
    
    unsigned int orderNumber;
public:
    Mesh() = delete;
    Mesh(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices);
    ~Mesh() { glDeleteVertexArrays(1, &VAO); }
    
    void Draw() override final;
    void Draw(unsigned int count) override;
    
    inline void bindVAO() { glBindVertexArray(VAO); }
    inline unsigned int getOrderNumber() { return orderNumber; }
    
    void Process(int tag) override {}
    
    void AddInstances(std::vector<glm::vec2> params);
};
