//
//  Object.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "BasicItem.hpp"
#include <sstream>
#include "ShaderProgram.h"

#include "TextureItem.hpp"

void BasicItem::AddTexture(const std::string& path, std::string type, TextureOptions opts)
{
    bool skip = false;
    for(unsigned int j = 0; j < textures_loaded.size(); j++)
    {
        if (std::strcmp(textures_loaded[j]->path.C_Str(), path.c_str()) == 0 && textures_loaded[j]->getType() == type)
        {
            addTextureItem(textures_loaded[j]);
            skip = true;
            break;
        }
    }
    if (!skip)
    {
        texture_ptr texture = Texture::CreateTexture(path.c_str(), "", type, opts);
        texture->path = path;
        addTextureItem(texture);
        textures_loaded.push_back(texture);
    }
}

void BasicItem::AddTexture(texture_ptr texture)
{
    addTextureItem(texture);
}

void BasicItem::addTextureItem(const texture_ptr &texture) {
    std::stringstream ss;
    std::string number;
    std::string type = texture->getType();
    
    tex_item_ptr texItem = tex_item_ptr(new TextureItem);

    texItem->texture = texture;

    if (type == TEXTURE_DIFFUSE)
    {
        ss << diffuseNr++;
        number = ss.str();
        texItem->AddName("material." + type + number);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    } else if (type == TEXTURE_SPECULAR) {
        ss << specularNr++;
        number = ss.str();
        texItem->AddName("material." + type + number);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    } else if (type == TEXTURE_CUBE_MAP) {
        texItem->AddName(TEXTURE_CUBE_MAP);
        texItem->target = GL_TEXTURE_CUBE_MAP;
        textures.push_back(texItem);
    } else if (type == TEXTURE_DEPTH) {
        ss << depthNr++;
        number = ss.str();
        texItem->AddName(type + number);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    } else if (type == TEXTURE_CUBEMAP_DEPTH) {
        ss << cubemapDepthNr++;
        number = ss.str();
        texItem->AddName(type + number);
        texItem->target = GL_TEXTURE_CUBE_MAP;
        textures.push_back(texItem);
    } else if (type == TEXTURE_NORMAL_MAP) {
        ss << normalMapNr++;
        number = ss.str();
        texItem->AddName(type + number);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    } else if (type == TEXTURE_DISPLACEMENT_MAP) {
        ss << dispMapNr++;
        number = ss.str();
        texItem->AddName(type + number);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    } else if (type == TEXTURE_CUSTOM) {
        texItem->AddName(texture->name);
        texItem->target = GL_TEXTURE_2D;
        textures.push_back(texItem);
    }
}

void BasicItem::bindTextures(const shader_program_ptr& shader)
{
    for(unsigned int i = 0; i < textures.size(); i++)
    {
        tex_item_ptr texItem = textures[i];
        texture_ptr texture = texItem->texture;
        auto name = texItem->name;
        
        glActiveTexture(GL_TEXTURE0 + i);
        
        glUniform1i(glGetUniformLocation(shader->getProgram(), name), i);
        glBindTexture(texItem->target, texture->getID());
    }
}

void BasicItem::bindTexture(texture_ptr texture, const char* name, const shader_program_ptr& shader, GLubyte order)
{
    glActiveTexture(GL_TEXTURE0 + order);
    
    GLenum target = GL_TEXTURE_2D;
    
    glUniform1i(glGetUniformLocation(shader->getProgram(), name), order);
    glBindTexture(target, texture->getID());
}

void BasicItem::setScale(const glm::vec3 scale) {
    m_scaleMatrix[0][0] = scale.x;
    m_scaleMatrix[1][1] = scale.y;
    m_scaleMatrix[2][2] = scale.z;
    
    modelMatrix = m_rotateMatrix * m_scaleMatrix;
}

void BasicItem::setRotation(float angle, glm::vec3 vector) {
    m_rotateMatrix = glm::rotate(glm::mat4(1), angle, vector);
    
    modelMatrix = m_rotateMatrix * m_scaleMatrix;
}

void BasicItem::addRotation(float angle, glm::vec3 vector) {
    glm::mat4 addRotate = glm::rotate(glm::mat4(1), angle, vector);
    m_rotateMatrix = addRotate * m_rotateMatrix;
    
    modelMatrix = m_rotateMatrix * m_scaleMatrix;
}
