//
//  BasicItem.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 27/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "open_gl.h"
#include "glm.h"
#include "Texture.hpp"
#include "types.h"

#include <vector>

#include "RenderItem.h"

struct Face { unsigned int i1, i2, i3; };

enum class FACE_ORIENTATION
{
    FRONT,
    BACK
};

class Composite;

class BasicItem: public IRenderItem
{
    friend Composite;
private:
    unsigned int diffuseNr = 1;
    unsigned int specularNr = 1;
    unsigned int depthNr = 1;
    unsigned int cubemapDepthNr = 1;
    unsigned int normalMapNr = 1;
    unsigned int dispMapNr = 1;
    
    std::vector<tex_item_ptr> textures;
    std::vector<texture_ptr> textures_loaded;
    
    void addTextureItem(const texture_ptr& texture);
    
    glm::vec3 m_pos = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::mat4 m_scaleMatrix = glm::mat4(1);
    glm::mat4 m_rotateMatrix = glm::mat4(1);
    
    glm::mat4 modelMatrix = glm::mat4(1);
    
protected:
    BasicItem() {}

    virtual void Draw(unsigned int count) {}
public:
    virtual void Draw() = 0;
    
    virtual ~BasicItem() {}
    
    virtual void AddTexture(const std::string& path, std::string type, TextureOptions opts = TextureOptions());
    virtual void AddTexture(texture_ptr texture) override;
    
    inline void setPosition(const glm::vec3 pos) { m_pos = pos; }
    inline glm::vec3 getPosition() { return m_pos; }
    
    void setScale(const glm::vec3 scale);
    void setRotation(float angle, glm::vec3 vector);
    void addRotation(float angle, glm::vec3 vector);
    
    void bindTextures(const shader_program_ptr&shader);
    void bindTexture(texture_ptr texture, const char* name, const shader_program_ptr& shader, GLubyte order = 0);
    
    inline glm::mat4 GetModelMatrix()
    {
        modelMatrix[3][0] = m_pos.x;
        modelMatrix[3][1] = m_pos.y;
        modelMatrix[3][2] = m_pos.z;
        return modelMatrix;
    }
};
