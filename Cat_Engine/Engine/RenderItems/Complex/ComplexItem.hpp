//
//  ComplexItem.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 02.12.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once
#include <list>
#include "types.h"
#include "BasicItem.hpp"

class Composite: public BasicItem {
private:
    bool isEmpty = true;
    
    Composite* parent = nullptr;
    
    inline glm::vec3 getRelatedPosition() {
        if (parent) {
            return m_pos + parent->getRelatedPosition();
        }
        return m_pos;
    }
protected:
    
    std::list<composite_item_ptr> items;
    
    void Process(int tag) override final {
        this->ComponentProcess(tag);
        for (auto &i : items) {
            i->ComponentProcess(tag);
        }
    }
    
    virtual void ComponentProcess(int tag) = 0;
public:
    void AddItem(composite_item_ptr item) {
        auto itemPtr = item.get();
        if (itemPtr->parent) {
            itemPtr->parent->items.remove(item);
        }
        
        itemPtr->parent = this;
        items.push_back(item);
    }
    
    inline glm::mat4 GetModelMatrix()
    {
        glm::vec3 relatePos = getRelatedPosition();
        modelMatrix[3][0] = relatePos.x;
        modelMatrix[3][1] = relatePos.y;
        modelMatrix[3][2] = relatePos.z;
        return modelMatrix;
    }
};
