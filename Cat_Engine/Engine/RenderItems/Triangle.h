//
//  Triangle.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 19.01.2021.
//  Copyright © 2021 Naveen. All rights reserved.
//

#pragma once

#include "BasicItem.hpp"
#include "ComplexItem.hpp"

#include "glm.h"
#include <vector>
#include "Vertex.h"

struct TriangleVertex: public Vertex {
    glm::vec2 pos;
    glm::vec3 color;
    
    TriangleVertex(const glm::vec2& p, const glm::vec3& c) : pos(p), color(c) {}
    
    size_t sizeOfStruct() override {
        return sizeof(TriangleVertex);
    }
    
    VkFormat formatOfMember(unsigned int index) override {
        switch(index) {
            case 0:
                return VK_FORMAT_R32G32_SFLOAT;
            case 1:
                return VK_FORMAT_R32G32B32_SFLOAT;
            default:
                return VK_FORMAT_R32G32B32_SFLOAT;
        }
    }
    
    size_t offsetOfMember(unsigned int index) override {
        switch(index) {
            case 0:
                return offsetof(TriangleVertex, pos);
            case 1:
                return offsetof(TriangleVertex, color);
            default:
                return 0;
        }
    }
    
    size_t countOfMember() override {
        return 2;
    }
};

template<class Base>
class Triangle: public Base
{
public:
    Triangle(std::vector<Vertex> verts);
    Triangle() {}
    ~Triangle() { }
    
    void Draw() override final;
protected:
    void Draw(unsigned int count) override;
    GLuint VAO;
private:
    std::vector<Vertex> vertices;
};

template<class Base>
Triangle<Base>::Triangle(std::vector<Vertex> verts) : vertices(verts)
{
    
}

template<class Base>
void Triangle<Base>::Draw()
{
}

template<class Base>
void Triangle<Base>::Draw(unsigned int count)
{
}
