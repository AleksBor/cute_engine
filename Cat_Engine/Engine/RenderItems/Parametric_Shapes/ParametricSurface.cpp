//
//  ParametricSurface.cpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 02/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#include "ParametricSurface.hpp"
#include "data_manip.hpp"

void ParametricSurface::SetInterval(const ParametricInterval& interval)
{
    m_upperBound = interval.UpperBound;
    m_divisions = interval.Divisions;
    m_slices = m_divisions - glm::i32vec2(1, 1);
}

int ParametricSurface::GetVertexCount() const
{
    return m_divisions.x * m_divisions.y;
}

int ParametricSurface::GetLineIndexCount() const
{
    if (lineIndexCount == 0) {
        lineIndexCount = 4 * m_slices.x * m_slices.y;
    }
    return lineIndexCount;
}

int ParametricSurface::GetTriangleIndexCount() const
{
    if (triangleIndexCount == 0) {
        triangleIndexCount = 6 * m_slices.x * m_slices.y;
    }
    return triangleIndexCount;
}

glm::vec2 ParametricSurface::ComputeDomain(float x, float y) const
{
    return glm::vec2(x * m_upperBound.x / m_slices.x, y * m_upperBound.y / m_slices.y);
}

void ParametricSurface::GenerateVertices(std::vector<float>& vertices,
                                         unsigned char flags) const
{
    int floatsPerVertex = 3;
    if (flags & static_cast<int>(VERTEX_PARAMS::NORMALS))
    {
        floatsPerVertex += 3;
    }
    
    vertices.resize(GetVertexCount() * floatsPerVertex);
    void* attribute = &vertices[0];
    
    for (int j = 0; j < m_divisions.y; j++)
    {
        for (int i = 0; i < m_divisions.x; i++)
        {
            glm::vec2 domain = ComputeDomain(i, j);
            glm::vec3 range = Evaluate(domain);
            
            Data::WriteData(&attribute, range);
            
            //Compute Normal
            if (flags & static_cast<int>(VERTEX_PARAMS::NORMALS)) {
                float s = i, t = j;
                
                //Nudge the point if the normal is indeterminate
                if (i == 0) s += 0.01;
                if (i == m_divisions.x - 1) s -= 0.01f;
                if (j == 0) t += 0.01f;
                if (j == m_divisions.y - 1) t -= 0.01f;
                
                //Compute the tangents and their cross product.
                glm::vec3 p = Evaluate(ComputeDomain(s, t));
                glm::vec3 u = Evaluate(ComputeDomain(s + 0.01f, t)) - p;
                glm::vec3 v = Evaluate(ComputeDomain(s, t + 0.01f)) - p;
                glm::vec3 normal = glm::cross(u, v);//need to normalize
                if (InvertNormal(domain))
                {
                    normal = -normal;
                }
                Data::WriteData(&attribute, normal);
            }
        }
    }
}

void ParametricSurface::GenerateLineIndices(std::vector<unsigned int>& indices) const
{
    indices.resize(GetLineIndexCount());
    std::vector<unsigned int>::iterator index = indices.begin();
    for (int j = 0, vertex = 0; j < m_slices.y; j++)
    {
        for (int i = 0; i < m_slices.x; i++)
        {
            int next = (i + 1) % m_divisions.x;
            *index++ = vertex + i;
            *index++ = vertex + next;
            *index++ = vertex + i;
            *index++ = vertex + i + m_divisions.x;
        }
        vertex += m_divisions.x;
    }
}

void ParametricSurface::GenerateTriangleIndices(std::vector<unsigned int>& indices) const
{
    indices.resize(GetTriangleIndexCount());
    std::vector<unsigned int>::iterator index = indices.begin();
    for (int j = 0, vertex = 0; j < m_slices.y; j++)
    {
        for (int i = 0; i < m_slices.x; i++)
        {
            int next = (i + 1) % m_divisions.x;
            *index++ = vertex + i;
            *index++ = vertex + next;
            *index++ = vertex + i + m_divisions.x;
            *index++ = vertex + next;
            *index++ = vertex + next + m_divisions.x;
            *index++ = vertex + i + m_divisions.x;
        }
        vertex += m_divisions.x;
    }
}

void ParametricSurface::Draw() {
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, GetTriangleIndexCount(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
