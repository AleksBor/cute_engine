//
//  ParametricSurface.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 02/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "Interfaces.h"
#include "glm.h"
#include <vector>

#include "open_gl.h"

#include "BasicItem.hpp"

struct ParametricInterval {
    glm::i32vec2 Divisions;
    glm::vec2 UpperBound;
};

class ParametricSurface : public ISurface, public BasicItem {
public:
    GLuint VAO;
    
    virtual ~ParametricSurface() {}
    
    int GetVertexCount() const override;
    int GetLineIndexCount() const override;
    int GetTriangleIndexCount() const override;
    void GenerateVertices(std::vector<float>& vertices, unsigned char flags = 0) const override;
    void GenerateLineIndices(std::vector<unsigned int>& indices) const override;
    void GenerateTriangleIndices(std::vector<unsigned int>& indices) const override;
    
    void Draw() override final;
protected:
    void SetInterval(const ParametricInterval& interval);
    virtual glm::vec3 Evaluate(const glm::vec2& domain) const = 0;
    virtual bool InvertNormal(const glm::vec2& domain) const { return false; }
private:
    glm::vec2 ComputeDomain(float x, float y) const;
    glm::vec2 m_upperBound;
    glm::i32vec2 m_slices;
    glm::i32vec2 m_divisions;
    
    mutable int lineIndexCount{0};
    mutable int triangleIndexCount{0};
};
