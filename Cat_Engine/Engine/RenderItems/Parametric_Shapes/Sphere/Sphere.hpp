//
//  Sphere.hpp
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 03/12/2019.
//  Copyright © 2019 Naveen. All rights reserved.
//

#pragma once

#include "ParametricSurface.hpp"
#define _USE_MATH_DEFINES
#include <math.h>

class Sphere : public ParametricSurface
{
public:
    Sphere(float radius, unsigned char flags = 0) : m_radius(radius), m_flags(flags)
    {
        ParametricInterval interval = { glm::i32vec2(20, 20), glm::vec2(M_PI, 2 * M_PI)};
        SetInterval(interval);

        init_gl();
    }
    ~Sphere() { glDeleteVertexArrays(1, &VAO); }

    void init_gl(){
        std::vector<GLfloat> verts;

        GenerateVertices(verts, m_flags);

        int indexCount = GetTriangleIndexCount();

        std::vector<GLuint> indexArr(indexCount);

        GenerateTriangleIndices(indexArr);

        // VAO
        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);

        GLuint VBO;
        glGenBuffers(1, &VBO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(verts[0]) * verts.size(), &verts[0], GL_STATIC_DRAW);

        GLuint EBO;
        glGenBuffers(1, &EBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexArr[0]) * indexCount, &indexArr[0], GL_STATIC_DRAW);

        // Vertices
        if (m_flags & static_cast<int>(VERTEX_PARAMS::NORMALS)) {
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 6, ((char*) NULL) + (0));

            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 6, ((char*) NULL) + (sizeof(GLfloat) * 3));
        } else {
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, ((char*) NULL) + (0));
        }

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    glm::vec3 Evaluate(const glm::vec2 &domain) const override
    {
        float u = domain.x, v = domain.y;
        float x = m_radius * sin(u) * cos(v);
        float y = m_radius * cos(u);
        float z = m_radius * -sin(u) * sin(v);
        return glm::vec3(x, y, z);
    }
private:
    float m_radius;
    unsigned char m_flags;
};
