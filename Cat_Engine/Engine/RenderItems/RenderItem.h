//
//  RenderItem.h
//  OpenGL_Tutorial
//
//  Created by Aleksandr Borodulin on 15.04.2020.
//  Copyright © 2020 Naveen. All rights reserved.
//

#pragma once

#include "types.h"

class Vertex;

class IRenderItem {
    friend class FrameBuffer;
public:
    virtual void AddTexture(texture_ptr texture) = 0;
protected:
    virtual void Process(int tag) = 0;
    virtual shader_program_ptr getShader() { return nullptr; };
    virtual Vertex* getVertexData() { return nullptr; };
};
