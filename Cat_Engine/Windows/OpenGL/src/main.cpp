#include <iostream>

#include "Shader.hpp"
#include "Camera.hpp"

#include "Village.hpp"
#include "PostProcessing.hpp"
#include "SSAO.hpp"
#include "ShaderEditor.h"

#include "SceneWindow.hpp"

#ifdef VULKAN_API
#include "VulkanTestScene.hpp"
#else
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "MainGUI.h"
#include "ShaderEditorGUI.h"
#endif

#include <thread>

const int screenWidth = 1200;
const int screenHeight = 800;

double deltaTime = 0.0f;
double lastFrame = 0.0f;

bool grabCursor = false;

#if __APPLE__
// GL 3.2 + GLSL 150
const char* glsl_version = "#version 150";
#else
// GL 3.0 + GLSL 130
const char* glsl_version = "#version 130";
#endif

bool initializeWindow() {

    SceneWindow::Init(screenWidth, screenHeight);
#ifndef VULKAN_API
    std::cout << glGetString(GL_VERSION) << std::endl;
#endif

    return true;
}

int main(void)
{
    if (!initializeWindow()) { return -1; }
#ifdef VULKAN_API
    Scene* currCanv = new VulkanTestScene();
#else
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(SceneWindow::Instance->getWindow(), true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    ShaderEditorGUI::Instance().run_callback = runShaderCallback;

    //ImFont* tahoma = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Tahoma.ttf", 27.0f);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);

    Scene* currCanv = new Village();
#endif
    double currentFrame;

    while (true)
    {
        currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;

        if (deltaTime < 0.05)
        {
            std::this_thread::yield();
            continue;
        }

        if (glfwWindowShouldClose(SceneWindow::Instance->getWindow())) break;

        lastFrame = currentFrame;
#ifndef VULKAN_API
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        {
            static int lastCanv = 0;

            MainGUI::Instance().Display();

            int radioBtn = MainGUI::Instance().selectedRadioButton;

            if (radioBtn == 3) {
                ShaderEditorGUI::Instance().Display();
            }

            if (radioBtn != lastCanv) {
                lastCanv = radioBtn;
                delete currCanv;
                switch (radioBtn) {
                case 0:
                    currCanv = new Village();
                    break;
                case 1:
                    currCanv = new PostProcessing();
                    break;
                case 2:
                    currCanv = new SSAO();
                    break;
                case 3:
                    currCanv = new ShaderEditor();
                    break;
                }
            }
        }

        // Rendering
        ImGui::Render();

        // Render here
        glViewport(0, 0, screenWidth, screenHeight);
#endif

        currCanv->Update();
#ifndef VULKAN_API
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(SceneWindow::Instance->getWindow());
#endif
        glfwPollEvents();
#ifdef VULKAN_API
        SceneWindow::Instance->DrawFrame();
#endif
    }

#ifdef VULKAN_API
    vkDeviceWaitIdle(SceneWindow::Instance->getDevice());
#else
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
#endif

    delete currCanv;

    SceneWindow::Reset();
    glfwTerminate();
    return 0;
}