#pragma once

#if _DEBUG
#define ROOT_PATH std::string(SOLUTION_DIR) + ".."
#else
#define ROOT_PATH std::string("C:/Aleksandr/Programming/OpenGL/CuteEngine/Cat_Engine")
#endif

#define TEXTURE_ROOT_PATH ROOT_PATH + "/Examples/res/texture/"
#define SHADERS_ROOT_PATH ROOT_PATH + "/Examples/res/shaders/"
#define TERRAIN_ROOT_PATH ROOT_PATH + "/Examples/res/terrain/"